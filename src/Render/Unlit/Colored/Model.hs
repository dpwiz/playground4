module Render.Unlit.Colored.Model
  ( Model

  , VertexAttrs
  , vkVertexAttrs

  , InstanceAttrs
  ) where

import Geomancy (Transform, Vec4)
import Geomancy.Vec3 qualified as Vec3
import Resource.Model qualified as Model
import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

type Model buf = Model.Indexed buf Vec3.Packed VertexAttrs

type VertexAttrs = "RGBA" ::: Vec4

type InstanceAttrs = Transform

vkVertexAttrs :: [Vk.Format]
vkVertexAttrs =
  [ Vk.FORMAT_R32G32B32A32_SFLOAT -- vColor :: vec4
  ]
