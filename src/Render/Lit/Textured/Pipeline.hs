module Render.Lit.Textured.Pipeline
  ( Pipeline
  , allocate
  , allocateBlend
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Vulkan.Core10 qualified as Vk
import Vulkan.Zero (zero)

import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (HasVulkan, HasRenderPass(..), DsBindings)
import Render.Code (compileVert, compileFrag, glsl)
import Render.DescSets.Set0 (Scene, vertexPos, instanceTransform)
import Render.DescSets.Set0.Code (set0binding0, set0binding1, set0binding2, set0binding3, set0binding4, set0binding5)
import Render.Code.Lit (litMain, shadowFuns, structLight, brdfSpecular)
import Render.Lit.Textured.Model qualified as Model

type Config = Pipeline.Config '[Scene] Model.VertexAttrs Model.InstanceAttrs
type Pipeline = Pipeline.Pipeline '[Scene] Model.VertexAttrs Model.InstanceAttrs

allocate
  :: ( HasVulkan env
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsBindings
  -> renderpass
  -> ResourceT (RIO env) Pipeline
allocate multisample tset0 rp = do
  (_, p) <- Pipeline.allocate
    Nothing
    multisample
    (config tset0)
    rp
  pure p

allocateBlend
  :: ( HasVulkan env
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsBindings
  -> renderpass
  -> ResourceT (RIO env) Pipeline
allocateBlend multisample tset0 rp = do
  (_, p) <- Pipeline.allocate
    Nothing
    multisample
    (configBlend tset0)
    rp
  pure p

config :: Tagged Scene DsBindings -> Config
config (Tagged set0) = zero
  { Pipeline.cDescLayouts  = Tagged @'[Scene] [set0]
  , Pipeline.cVertexCode   = Just vertCode
  , Pipeline.cVertexInput  = vertexInput
  , Pipeline.cFragmentCode = Just fragCode
  }
  where
    vertexInput = Pipeline.vertexInput
      [ vertexPos -- vPosition
      , (Vk.VERTEX_INPUT_RATE_VERTEX,   Model.vkVertexAttrs)
      , (Vk.VERTEX_INPUT_RATE_INSTANCE, Model.vkInstanceTexture)
      , instanceTransform
      ]

configBlend :: Tagged Scene DsBindings -> Config
configBlend tset0 = (config tset0)
  { Pipeline.cBlend      = True
  , Pipeline.cDepthWrite = False
  }

vertCode :: ByteString
vertCode =
  $(compileVert [glsl|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    ${set0binding0}

    // vertexPos
    layout(location = 0) in vec3 vPosition;
    // vertexAttrs
    layout(location = 1) in vec2 vTexCoord;
    layout(location = 2) in vec3 vNormal;
    // textureParams
    layout(location = 3) in  vec4 iTextureScaleOffset;
    layout(location = 4) in  vec4 iTextureGamma;
    layout(location = 5) in ivec2 iTextureIds;

    // transformMat
    layout(location = 6) in mat4 iModel;

    layout(location = 0)      out  vec4 fPosition;
    layout(location = 1)      out  vec3 fNormal;
    layout(location = 2)      out  vec2 fTexCoord;
    layout(location = 3) flat out  vec4 fTextureGamma;
    layout(location = 4) flat out ivec2 fTextureIds;

    void main() {
      fPosition = iModel * vec4(vPosition, 1.0);

      gl_Position
        = scene.projection
        * scene.view
        * fPosition;

      fNormal = transpose(mat3(inverse(iModel))) * vNormal; // TODO: use modelInv

      fTexCoord     = vTexCoord * iTextureScaleOffset.st + iTextureScaleOffset.pq;
      fTextureGamma = iTextureGamma;
      fTextureIds   = iTextureIds;
    }
  |])

fragCode :: ByteString
fragCode =
  $(compileFrag [glsl|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable
    #extension GL_EXT_nonuniform_qualifier : enable

    // XXX: copypasta from Lit.Colored
    // TODO: move to spec constant
    const uint MAX_LIGHTS = 255;
    const float PCF_STEP = 1.5 / 4096;

    // TODO: move to material
    const float reflectivity = 1.0/256.0;

    ${structLight}

    ${set0binding0}
    ${set0binding1}
    ${set0binding2}
    ${set0binding3}
    ${set0binding4}
    ${set0binding5}

    layout(location = 0)      in  vec4 fPosition;
    layout(location = 1)      in  vec3 fNormal;
    layout(location = 2)      in  vec2 fTexCoord;
    layout(location = 3) flat in  vec4 fTextureGamma;
    layout(location = 4) flat in ivec2 fTextureIds;

    layout(location = 0) out vec4 oColor;

    ${shadowFuns}
    ${brdfSpecular}

    void main() {
      // XXX: fall back to solid color
      vec4 baseColor = fTextureGamma;
      if ((fTextureIds.t > -1) && (fTextureIds.s > -1)) {
        vec4 texel = texture(
          sampler2D(
            textures[nonuniformEXT(fTextureIds.t)],
            samplers[nonuniformEXT(fTextureIds.s)]
          ),
          fTexCoord
        );

        if (texel.a < 0.5) discard;

        vec3 color = pow(texel.rgb, fTextureGamma.rgb);
        float combinedAlpha = texel.a * fTextureGamma.a;
        baseColor = vec4(color * combinedAlpha, combinedAlpha);
      }

      // TODO: get from textures
      float metallic = 0.0;
      float roughness = 0.6;
      float occlusion = 1.0 - 0.0;

      vec3 normal = normalize(fNormal);

      ${litMain}
    }
  |])
