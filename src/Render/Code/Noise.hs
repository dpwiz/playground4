module Render.Code.Noise
  ( hash33
  ) where

import Render.Code (Code(..), trimming)

hash33 :: Code
hash33 = Code
  [trimming|
    vec3 hash33(vec3 a) {
      vec3 p = fract(a * vec3(443.8975, 397.2973, 491.1871));
      p += dot(p.zxy, p.yxz + 19.27);
      return fract(vec3(p.x * p.y, p.z * p.x, p.y * p.z));
    }
  |]
