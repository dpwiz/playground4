{-# LANGUAGE OverloadedLists #-}

module Render.Skybox.Pipeline
  ( Pipeline
  , allocate
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Vulkan.Core10 qualified as Vk
import Vulkan.Zero (Zero(..))

import Engine.Vulkan.Pipeline (Config(..))
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (HasVulkan, HasRenderPass(..), DsBindings)
import Render.Code (compileVert, compileFrag, glsl)
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0.Code (set0binding0, set0binding1, set0binding3)

type Pipeline = Pipeline.Pipeline '[Scene] () ()

allocate
  :: ( HasVulkan env
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsBindings
  -> renderpass
  -> ResourceT (RIO env) Pipeline
allocate multisample (Tagged set0) = do
  fmap snd . Pipeline.allocate
    Nothing
    multisample
    zero
      { cVertexCode         = Just vertCode
      , cFragmentCode       = Just fragCode
      , cDescLayouts        = Tagged @'[Scene] [set0]
      , cCull               = Vk.CULL_MODE_NONE
      }

vertCode :: ByteString
vertCode =
  $(compileVert [glsl|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    ${set0binding0}

    layout(location = 0) out vec3 fragUVW;

    float farZ = 0.9999; // 1 - 1e-7;

    void main() {
      vec4 pos = vec4(0.0);
      switch(gl_VertexIndex) {
          case 0: pos = vec4(-1.0,  3.0, farZ, 1.0); break;
          case 1: pos = vec4(-1.0, -1.0, farZ, 1.0); break; // XXX: swapped with 2. culling?
          case 2: pos = vec4( 3.0, -1.0, farZ, 1.0); break;
      }

      vec3 unProjected = (scene.invProjection * pos).xyz;
      unProjected *= -1;
      fragUVW = mat3(scene.invView) * unProjected;

      gl_Position = pos;
    }
  |])

fragCode :: ByteString
fragCode =
  $(compileFrag [glsl|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable
    #extension GL_EXT_nonuniform_qualifier : enable

    ${set0binding0}

    ${set0binding1}
    ${set0binding3}

    layout(location = 0) in vec3 fragUVW;

    layout(location = 0) out vec4 outColor;

    void main() {
      if (scene.envCubeId > -1) {
        outColor = texture(
          samplerCube(
            cubes[nonuniformEXT(scene.envCubeId)],
            samplers[2] // XXX: linear/mip0/repeat
          ),
          fragUVW
        );
      }
    }
  |])
