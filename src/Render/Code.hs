{-# OPTIONS_GHC -fno-prof-auto #-}

module Render.Code
  ( compileVert
  , compileFrag
  , glsl
  , trimming
  , Code(..)
  ) where

import RIO

import Language.Haskell.TH (Exp, Q)
import NeatInterpolation (trimming)
import RIO.Text qualified as Text
import Vulkan.Utils.ShaderQQ.GLSL.Glslang (compileShaderQ, glsl)

compileVert :: String -> Q Exp
compileVert = compileShaderQ Nothing "vert" Nothing

compileFrag :: String -> Q Exp
compileFrag = compileShaderQ Nothing "frag" Nothing

-- | A wrapper to `show` code into `compileShaderQ` vars.
newtype Code = Code { unCode :: Text }
  deriving (Eq, Ord)

instance Show Code where
  show = Text.unpack . unCode
