module Engine.Stage.Bootstrap.Types
  ( Stage

  , NoRendering(..)
  , NoPipelines(..)
  , NoResources(..)
  , RunState(..)
  ) where

import RIO

import Engine.Vulkan.Types (RenderPass(..))
import Engine.Types qualified as Engine
import Engine.StageSwitch (StageSwitchVar)

data NoRendering = NoRendering

instance RenderPass NoRendering where
  allocateRenderpass_ _context = pure NoRendering
  updateRenderpass _context = pure
  refcountRenderpass _rp = pure ()

data NoPipelines = NoPipelines

type Stage = Engine.Stage NoRendering NoPipelines NoResources RunState

data NoResources = NoResources

data RunState = RunState
  { rsNextStage :: StageSwitchVar
  }
