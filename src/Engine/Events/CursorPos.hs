module Engine.Events.CursorPos where

import RIO

import Geomancy (Vec2, vec2)
import GHC.Float (double2Float)
import UnliftIO.Resource (ReleaseKey)

import Engine.Types (StageRIO)
import Engine.Window.CursorPos qualified as CursorPos
import Engine.Worker qualified as Worker

import Engine.Events (Sink)

callback
  :: ( Worker.HasInput cursor
     , Worker.GetInput cursor ~ Vec2
     )
  => cursor
  -------------------------
  -> Sink e rs
  -> StageRIO rs ReleaseKey
callback cursorVar = CursorPos.callback . handler cursorVar

handler
  :: ( Worker.HasInput cursor
     , Worker.GetInput cursor ~ Vec2
     )
  => cursor
  -> Sink e rs
  -> CursorPos.Callback rs
handler cursorVar _sink windowX windowY = do
  -- logDebug $ "CursorPos event: " <> displayShow (windowX, windowY)
  Worker.pushInput cursorVar \_old ->
    vec2 (double2Float windowX) (double2Float windowY)
