module Engine.StageSwitch
  ( StageSwitchVar
  , newStageSwitchVar

  , StageSwitch(..)
  , trySwitchStage
  , getNextStage
  ) where

import RIO

import RIO.State (gets)

import Engine.Types (NextStage, StageRIO)

-- TODO: extract more of a stage stack here

type StageSwitchVar = TMVar StageSwitch

newStageSwitchVar :: MonadIO m => m StageSwitchVar
newStageSwitchVar = newEmptyTMVarIO

data StageSwitch
  = StageSwitchHandled
  | StageSwitchPending NextStage

trySwitchStage :: TMVar StageSwitch -> NextStage -> STM Bool
trySwitchStage switchVar = tryPutTMVar switchVar . StageSwitchPending

getNextStage :: (rs -> StageSwitchVar) -> StageRIO rs (Maybe NextStage)
getNextStage varGetter = do
  var <- gets varGetter
  atomically do
    noSwitch <- isEmptyTMVar var
    if noSwitch then
      pure Nothing
    else
      takeTMVar var >>= \case
        StageSwitchPending nextStage -> do
          putTMVar var StageSwitchHandled
          pure $ Just nextStage
        StageSwitchHandled ->
          pure Nothing
