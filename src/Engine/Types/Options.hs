module Engine.Types.Options
  ( Options(..)
  , getOptions
  , optionsP
  ) where

import RIO

import Options.Applicative.Simple qualified as Opt

import Paths_playground4 qualified

-- | Command line arguments
data Options = Options
  { optionsVerbose :: Bool
  , optionsFullscreen :: Bool
  }
  deriving (Show)

getOptions :: IO Options
getOptions = do
  (options, ()) <- Opt.simpleOptions
    $(Opt.simpleVersion Paths_playground4.version)
    header
    description
    optionsP
    Opt.empty
  pure options
  where
    header =
      "Another playground"

    description =
      mempty

optionsP :: Opt.Parser Options
optionsP = do
  optionsVerbose <- Opt.switch $ mconcat
    [ Opt.long "verbose"
    , Opt.short 'v'
    , Opt.help "Show more and more detailed messages"
    ]

  optionsFullscreen <- Opt.switch $ mconcat
    [ Opt.long "fullscreen"
    , Opt.short 'f'
    , Opt.help "Run in fullscreen mode"
    ]

  pure Options{..}
