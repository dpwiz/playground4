module Resource.Collection
  ( enumerate
  , size
  , toVector
  , toVectorStorable
  ) where

import RIO

import RIO.Vector qualified as Vector
import RIO.Vector.Storable qualified as VectorStorable
import Data.List qualified as List
import Data.Traversable (mapAccumL)

{-# INLINE size #-}
size :: (Foldable t, Num size) => t a -> size
size = List.genericLength . toList

enumerate :: (Traversable t, Num ix) => t a -> t (ix, a)
enumerate = snd . mapAccumL f 0
  where
    f a b = (a + 1, (a, b))

{-# INLINE toVector #-}
toVector :: Foldable collection => collection a -> Vector a
toVector = Vector.fromList . toList

{-# INLINE toVectorStorable #-}
toVectorStorable
  :: (Foldable collection, Storable a)
  => collection a
  -> VectorStorable.Vector a
toVectorStorable = VectorStorable.fromList . toList
