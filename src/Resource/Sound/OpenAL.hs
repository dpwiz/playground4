{-# LANGUAGE CPP #-}

{-
  TODO: Decouple format and output
-}

#ifndef SOUND_OPENAL

module Resource.Sound.OpenAL () where

#else

module Resource.Sound.OpenAL
  ( OpenAL.Device
  , allocateDevice
  , createDevice
  , destroyDevice

  , Config(..)
  , Sound(..)
  , allocateCollection
  , allocateSound
  , createSound
  , destroySound

  , setListener
  , setPosition
  , toggle
  , trigger
  ) where

import RIO

import Data.ByteString qualified as ByteString
import Data.Coerce (coerce)
import Data.StateVar (($=!))
import Foreign qualified
import Foreign.C.Types (CFloat(..))
import Geomancy (Vec3, withVec3)
import Sound.OpenAL qualified as OpenAL
import Sound.OpusFile qualified as OpusFile
import UnliftIO.Resource (MonadResource)
import UnliftIO.Resource qualified as Resource

-- * Output device

allocateDevice
  :: (MonadUnliftIO m, MonadReader env m, HasLogFunc env, MonadResource m)
  => m (Resource.ReleaseKey, OpenAL.Device)
allocateDevice = do
  device <- createDevice
  key <- toIO (destroyDevice device) >>=
    Resource.register
  pure (key, device)

createDevice
  :: (MonadIO m, MonadReader env m, HasLogFunc env)
  => m OpenAL.Device
createDevice =
  OpenAL.openDevice Nothing >>= \case
    Nothing -> do
      logError "OpenAL: no devices"
      exitFailure
    Just device -> do
      OpenAL.createContext device [] >>= \case
        Nothing -> do
          logError "OpenAL.createContext failed"
          exitFailure
        Just ctx -> do
          OpenAL.currentContext $=! Just ctx
          pure device

destroyDevice
  :: (MonadIO m, MonadReader env m, HasLogFunc env)
  => OpenAL.Device
  -> m ()
destroyDevice device =
  OpenAL.closeDevice device >>= \case
    True ->
      pure ()
    False ->
      logWarn "OpenAL: closeDevice error"

-- * Samples

data Config = Config
  { source  :: FilePath
  , gain    :: Float
  , looping :: Bool
  , rollOff :: Float
  }
  deriving (Eq, Ord, Show, Generic)

data Sound = Sound
  { soundSource :: OpenAL.Source
  }

allocateCollection
  :: ( MonadReader env m, HasLogFunc env, MonadResource m
     , Traversable collection
     )
  => collection Config
  -> m (Resource.ReleaseKey, collection Sound)
allocateCollection collection = do
  collected <- for collection allocateSound
  key <- Resource.register $
    traverse_ Resource.release (fmap fst collected)
  pure (key, fmap snd collected)

allocateSound
  :: (MonadReader env m, HasLogFunc env, MonadResource m)
  => Config
  -> m (Resource.ReleaseKey, Sound)
allocateSound config = do
  sound <- createSound config
  key <- Resource.register $ destroySound sound
  pure (key, sound)

createSound :: (MonadIO m, MonadReader env m, HasLogFunc env) => Config -> m Sound
createSound Config{..} = do
  logDebug $ "Loading " <> fromString source

  -- Format

  pcm <- liftIO do
    Right file <- ByteString.readFile source >>= OpusFile.openMemoryBS
    pcm <- OpusFile.decodeInt16 file
    OpusFile.free file
    pure pcm

  -- Output

  alFormat <-
    case OpusFile.pcmChannels pcm of
      Right OpusFile.Stereo ->
        pure OpenAL.Stereo16
      Right OpusFile.Mono ->
        pure OpenAL.Mono16
      Left n -> do
        logError $ mconcat
          [ "unexpected channels in "
          , fromString source
          , ": "
          , display n
          ]
        exitFailure

  alBuffer <- liftIO OpenAL.genObjectName
  liftIO $
    Foreign.withForeignPtr (OpusFile.pcmData pcm) \ptr -> do
      let
        mreg =
          OpenAL.MemoryRegion ptr (fromIntegral $ OpusFile.pcmSize pcm)
      OpenAL.bufferData alBuffer $=! OpenAL.BufferData mreg alFormat 48000

  alSource <- liftIO $ OpenAL.genObjectName
  liftIO do
    let
      alLooping =
        if looping then
          OpenAL.Looping
        else
          OpenAL.OneShot

    OpenAL.buffer        alSource $=! Just alBuffer
    OpenAL.loopingMode   alSource $=! alLooping
    OpenAL.sourceGain    alSource $=! CFloat gain
    OpenAL.rolloffFactor alSource $=! CFloat rollOff

  pure Sound
    { soundSource = alSource
    -- , soundBuffer = alBuffer
    }

destroySound :: MonadIO m => Sound -> m ()
destroySound _sound =
  -- TODO
  pure ()

toggle :: MonadIO m => Sound -> Bool -> m ()
toggle Sound{..} active = do
  if active then
    OpenAL.play [soundSource]
  else
    OpenAL.stop [soundSource]

trigger :: MonadIO m => Sound -> Maybe Vec3 -> m ()
trigger Sound{soundSource} mpos = do
  traverse_ (setPosition soundSource) mpos
  OpenAL.play [soundSource] -- XXX: playing the active source restarts

setListener :: MonadIO m => Vec3 -> Vec3 -> Vec3 -> m ()
setListener pos forward up = do
  OpenAL.listenerPosition $=! alVertex pos
  OpenAL.orientation $=! (alVector forward, alVector up)

setPosition :: MonadIO m => OpenAL.Source -> Vec3 -> m ()
setPosition source pos =
  OpenAL.sourcePosition source $=! alVertex pos

alVertex :: Vec3 -> OpenAL.Vertex3 CFloat
alVertex vert = coerce $ withVec3 vert OpenAL.Vertex3

alVector :: Vec3 -> OpenAL.Vector3 CFloat
alVector vec = coerce $ withVec3 vec OpenAL.Vector3

#endif
