module Resource.Gltf.Weld
  ( Origin(..)
  , originV
  , adjustPositions

  , ModelPipeline(..)
  , Encodable
  , MkAttrs
  , MkNode
  , encode
  , validate

  , ignoreMaterial
  , texturedNode
  , materialNode

  , textureIndex

  , modelColoredLit
  , modelColoredUnlit
  , modelTexturedLit
  , modelTexturedUnlit
  , modelMaterial

  , sceneMeta
  , processSceneNodes
  , processMeshNode
  , partitionNodes

  , measureTree
  ) where

import RIO

import Codec.GlTF.Material qualified as GlTF (Material(..))
import Codec.GlTF.Material qualified as GltfMaterial
import Codec.GlTF.PbrMetallicRoughness qualified as Pbr
import Codec.GlTF.TextureInfo qualified as TextureInfo
import Data.List qualified as List
import Data.Sequence qualified as Seq
import Data.Tree (Tree)
import Data.Vector.Generic qualified as Generic
import Data.Vector.Storable qualified as Storable
import Geomancy (Vec3, vec2, vec4)
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import Vulkan.Zero (Zero(..))

import Render.Lit.Colored.Model qualified as ColoredLit
import Render.Lit.Material qualified as Lit (Material(..))
import Render.Lit.Material.Model qualified as MaterialLit
import Render.Lit.Textured.Model qualified as TexturedLit
import Render.Unlit.Colored.Model qualified as ColoredUnlit
import Render.Unlit.Textured.Model qualified as TexturedUnlit
import Resource.Gltf.Model qualified as Gltf
import Resource.Gltf.Scene qualified as Gltf
import Resource.Mesh.Codec qualified as MeshCodec
import Resource.Mesh.Types qualified as Mesh
import Resource.Mesh.Utils qualified as Utils
import Resource.Model (IndexRange(..))

data ModelPipeline
  = ModelColoredLit
  | ModelColoredUnlit
  | ModelMaterialLit
  | ModelTexturedLit
  | ModelTexturedUnlit
  deriving (Eq, Ord, Show, Enum, Bounded)

data Origin
  = OriginXLeft
  | OriginXMiddle
  | OriginXMean
  | OriginXRight

  | OriginYTop
  | OriginYMiddle
  | OriginYMean
  | OriginYBottom

  | OriginZNear
  | OriginZMiddle
  | OriginZMean
  | OriginZFar
  deriving (Eq, Ord, Show, Enum, Bounded)

type Encodable a = (Eq a, Show a, Typeable a, Storable a)
type MkAttrs attrs = Maybe (Int, GlTF.Material) -> Gltf.VertexAttrs -> attrs
type MkNode node = Maybe (Int, GlTF.Material) -> Mesh.Node -> node

originV :: Mesh.AxisAligned Mesh.Measurements -> [Origin] -> Vec3
originV measures = negate . Vec3.fromTuple . foldl' setOrigin (0, 0, 0)
  where
    Mesh.AxisAligned{..} = measures

    setOrigin (x, y, z) = \case
      OriginXLeft   -> (Mesh.mMin   aaX, y, z)
      OriginXMiddle -> (Mesh.middle aaX, y, z)
      OriginXMean   -> (Mesh.mMean  aaX, y, z)
      OriginXRight  -> (Mesh.mMax   aaX, y, z)

      OriginYTop    -> (x, Mesh.mMin   aaY, z)
      OriginYMiddle -> (x, Mesh.middle aaY, z)
      OriginYMean   -> (x, Mesh.mMean  aaY, z)
      OriginYBottom -> (x, Mesh.mMax   aaY, z)

      OriginZNear   -> (x, y, Mesh.mMin   aaZ)
      OriginZMiddle -> (x, y, Mesh.middle aaZ)
      OriginZMean   -> (x, y, Mesh.mMean  aaZ)
      OriginZFar    -> (x, y, Mesh.mMax   aaZ)

adjustPositions :: Vec3 -> Gltf.SceneNode -> Gltf.SceneNode
adjustPositions newOrigin sn =
  case Gltf.snPrimitives sn of
    Nothing ->
      sn
    Just primitives ->
      sn { Gltf.snPrimitives = Just (Generic.map (fmap process) primitives) }
      where
        process stuff = stuff
          { Gltf.sPositions =
              fmap
                (+ Vec3.Packed newOrigin)
                (Gltf.sPositions stuff)
          }

ignoreMaterial :: MkNode Mesh.Node
ignoreMaterial _mat node = node

materialNode :: MkNode Mesh.MaterialNode
materialNode mmaterial mnNode = Mesh.MaterialNode{..}
  where
    mnMaterial = Lit.Material{..}

    (mnMaterialIx, GlTF.Material{..}) =
      case mmaterial of
        Nothing ->
          error "oops, no material for material pipeline"
        Just found ->
          found

    pbr = fromMaybe (error "oops, no PBR in material") pbrMetallicRoughness

    mBaseColor = Vec4.fromTuple (Pbr.baseColorFactor pbr)
    mBaseColorTex = textureIndex (Pbr.baseColorTexture pbr)

    mNormalScale = maybe 1.0 (GltfMaterial.scale . TextureInfo.subtype) normalTexture
    mNormalTex = textureIndex normalTexture

    mEmissive = Vec4.fromVec3 @Vec3 (Vec3.fromTuple emissiveFactor) 1.0
    mEmissiveTex = textureIndex emissiveTexture

    mAlphaCutoff = alphaCutoff

    mMetallicRoughness = vec2 (Pbr.metallicFactor pbr) (Pbr.roughnessFactor pbr)
    mMetallicRoughnessTex = textureIndex (Pbr.metallicRoughnessTexture pbr)

    mAmbientOcclusionTex = textureIndex occlusionTexture

    -- tnOcclusion = zero
    --   { Mesh.tpTextureId =

    --   , Mesh.tpGamma =
    --       vec4 1 1 1 $ maybe 0 (Material.strength . TextureInfo.subtype) occlusionTexture
    --   }

texturedNode :: MkNode Mesh.TexturedNode
texturedNode mmaterial tnNode = Mesh.TexturedNode{..}
  where
    GlTF.Material{..} = case mmaterial of
      Nothing ->
        error "oops, no material for textured pipeline"
      Just (_ix, material) ->
        material

    pbr = fromMaybe (error "oops, no PBR in material") pbrMetallicRoughness

    tnBase = zero
      { Mesh.tpTextureId =
          textureIndex (Pbr.baseColorTexture pbr)
      , Mesh.tpGamma =
          Vec4.fromTuple (Pbr.baseColorFactor pbr)
      }

    tnNormal = zero
      { Mesh.tpTextureId =
          textureIndex normalTexture
      , Mesh.tpGamma =
          vec4 1 1 1 $ maybe 1 (GltfMaterial.scale . TextureInfo.subtype) normalTexture
      }

    tnEmissive = zero
      { Mesh.tpTextureId =
          textureIndex emissiveTexture
      , Mesh.tpGamma =
          Vec4.fromVec3 (Vec3.Packed $ Vec3.fromTuple emissiveFactor) alphaCutoff
      }

    tnMetallicRoughness = zero
      { Mesh.tpTextureId =
          textureIndex (Pbr.metallicRoughnessTexture pbr)
      , Mesh.tpGamma =
          vec4 0 (Pbr.metallicFactor pbr) (Pbr.roughnessFactor pbr) 0
      }

    tnOcclusion = zero
      { Mesh.tpTextureId =
          textureIndex occlusionTexture
      , Mesh.tpGamma =
          vec4 1 1 1 $ maybe 0 (GltfMaterial.strength . TextureInfo.subtype) occlusionTexture
      }

-- XXX: Almost guaranteed to freeze if unchecked in shader code.
textureIndex :: Maybe (TextureInfo.TextureInfo a) -> Int32
textureIndex = \case
  Nothing ->
    -1
  Just ti ->
    fromIntegral $ TextureInfo.index ti

modelColoredLit :: MkAttrs ColoredLit.VertexAttrs
modelColoredLit mmaterial Gltf.VertexAttrs{..} =
  case pbrMetallicRoughness of
    Nothing ->
      ColoredLit.VertexAttrs
        { vaBaseColor         = 1.0
        , vaEmissiveColor     = vec4 0 0 0 alphaCutoff
        , vaMetallicRoughness = 0.5
        , vaNormal
        }
    Just pbr ->
      ColoredLit.VertexAttrs
        { vaBaseColor         = Vec4.fromTuple (Pbr.baseColorFactor pbr)
        , vaEmissiveColor     = Vec4.fromVec3 @Vec3.Packed (Vec3.fromTuple emissiveFactor) alphaCutoff
        , vaMetallicRoughness = vec2 (Pbr.metallicFactor pbr) (Pbr.roughnessFactor pbr)
        , vaNormal
        }
  where
    GlTF.Material{..} = case mmaterial of
      Nothing ->
        error "oops, no material for Lit.Colored pipeline"
      Just (_ix, material) ->
        material

modelColoredUnlit :: MkAttrs ColoredUnlit.VertexAttrs
modelColoredUnlit mmaterial _va =
  case pbrMetallicRoughness of
    Nothing ->
      1.0
    Just Pbr.PbrMetallicRoughness{baseColorFactor} ->
      Vec4.fromTuple baseColorFactor
  where
    GlTF.Material{..} = case mmaterial of
      Nothing ->
        error "oops, no material for Unlit.Colored pipeline"
      Just (_ix, material) ->
        material

modelTexturedLit :: MkAttrs TexturedLit.VertexAttrs
modelTexturedLit _mat Gltf.VertexAttrs{..} = TexturedLit.VertexAttrs{..}

modelTexturedUnlit :: MkAttrs TexturedUnlit.VertexAttrs
modelTexturedUnlit _mat = Gltf.vaTexCoord

modelMaterial :: MkAttrs MaterialLit.VertexAttrs
modelMaterial mat Gltf.VertexAttrs{..} =
  MaterialLit.VertexAttrs
    { vaTexCoord0 = vaTexCoord
    , vaTexCoord1 = vaTexCoord
    , vaNormal    = vaNormal
    , vaTangent   = vaTangent
    , vaMaterial  = maybe 0 (fromIntegral . fst) mat
    }

processSceneNodes
  :: Foldable t
  => (Gltf.SceneNode -> Maybe (Int, GlTF.Material) -> Gltf.Stuff -> a)
  -> t Gltf.SceneNode
  -> [a]
processSceneNodes f = go . toList
  where
    go = \case
      [] ->
        []
      sn@Gltf.SceneNode{..} : next ->
        case snPrimitives of
          Nothing ->
            go next
          Just primitives ->
            map (uncurry (f sn)) (toList primitives) <> go next
          -- Just (material, stuff) ->
          --   f sn material stuff : go next

processMeshNode
  :: MkAttrs attrs
  -> MkNode node
  -> Gltf.SceneNode
  -> Maybe (Int, GlTF.Material)
  -> Gltf.Stuff
  -> ( Mesh.NodeGroup
     , ( ( Vector Vec3.Packed
         , Vector Word32
         , Vector attrs
         )
       , node
       )
     )
processMeshNode mkAttrs mkNode _sn material stuff =
  ( materialGroup material -- XXX: partition key
  , ( ( Gltf.sPositions stuff
      , Gltf.sIndices stuff
      , fmap (mkAttrs material) (Gltf.sAttrs stuff)
      )
    , mkNode material Mesh.Node{..}
    ) -- XXX: scene part and nodes
  )
  where
    nRange = IndexRange
      { irFirstIndex = 0
      , irIndexCount = fromIntegral $ Generic.length $ Gltf.sIndices stuff
      }

    nMeasurements = Mesh.measureAa positions

    nBoundingSphere = Utils.boundingSphere nMeasurements

    nTransformBB = mconcat
      [ Utils.aabbScale     nMeasurements
      , Utils.aabbTranslate nMeasurements
      ]

    positions = Gltf.sPositions stuff

encode
  :: ( HasLogFunc env
     , Encodable attrs
     , Encodable node
     , Encodable meta
     , Generic.Vector vp Vec3.Packed
     , Generic.Vector vi Word32
     , Generic.Vector va attrs
     , Generic.Vector vn node
     )
  => FilePath
  -> Bool
  -> vp Vec3.Packed
  -> vi Word32
  -> va attrs
  -> vn node
  -> meta
  -> RIO env ()
encode oFile oValidate positions indices attrs nodes meta = do
  MeshCodec.encodeFile oFile positions indices attrs nodes meta
  when oValidate $
    validate oFile positions indices attrs nodes meta

sceneMeta
  :: Mesh.AxisAligned Mesh.Measurements
  -> Mesh.NodePartitions Word32
  -> Mesh.NodePartitions [node]
  -> Mesh.Meta
sceneMeta mMeasurements stuffs nodes = Mesh.Meta
  { mOpaqueIndices  = IndexRange 0 opaqueIndices
  , mBlendedIndices = IndexRange opaqueIndices blendedIndices

  , mOpaqueNodes  = IndexRange 0 opaqueNodes
  , mBlendedNodes = IndexRange opaqueNodes blendedNodes

  , mBoundingSphere = Utils.boundingSphere mMeasurements
  , ..
  }
  where
    opaqueIndices  = Mesh.npOpaque stuffs
    blendedIndices = Mesh.npBlended stuffs

    -- XXX: boo, list lengths!
    opaqueNodes  = fromIntegral . length $ Mesh.npOpaque nodes
    blendedNodes = fromIntegral . length $ Mesh.npBlended nodes

    mTransformBB = mconcat
      [ Utils.aabbScale     mMeasurements
      , Utils.aabbTranslate mMeasurements
      ]

partitionNodes
  :: Foldable t
  => t (Mesh.NodeGroup, a)
  -> Mesh.NodePartitions (Seq a)
partitionNodes = foldl' go initial
  where
    go acc@Mesh.NodePartitions{..} (ng, stuff) =
      case ng of
        Mesh.NodeOpaque -> acc
          { Mesh.npOpaque = npOpaque Seq.|> stuff
          }
        Mesh.NodeBlended -> acc
          { Mesh.npBlended = npBlended Seq.|> stuff
          }

    initial = Mesh.NodePartitions mempty mempty

materialGroup :: Maybe (Int, GlTF.Material) -> Mesh.NodeGroup
materialGroup = \case
  Nothing ->
    Mesh.NodeOpaque
  Just (_ix, GltfMaterial.Material{alphaMode}) ->
    case alphaMode of
      GltfMaterial.OPAQUE -> Mesh.NodeOpaque
      GltfMaterial.MASK   -> Mesh.NodeBlended
      GltfMaterial.BLEND  -> Mesh.NodeBlended
      _unexpected ->
        error $ "unexpected alphaMode: " <> show alphaMode

validate
  :: ( HasLogFunc env
     , Encodable attrs
     , Encodable node
     , Encodable meta
     , Generic.Vector vp Vec3.Packed
     , Generic.Vector vi Word32
     , Generic.Vector va attrs
     , Generic.Vector vn node
     )
  => FilePath
  -> vp Vec3.Packed
  -> vi Word32
  -> va attrs
  -> vn node
  -> meta
  -> RIO env ()
validate outFile positions indices attrs nodes meta = do
  logInfo $ "Validating " <> fromString outFile
  (oMeta, oNodes, (oPositions, oIndices, oAttrs)) <- MeshCodec.loadBlobs outFile

  let posCheck = List.zip3 [0..] (Generic.toList positions) (Storable.toList oPositions)
  for_ posCheck \(n :: Int, Vec3.Packed i, Vec3.Packed o) ->
    when (i /= o) do
      logError $
        "Position mismatch at " <> display n <>
        ":\n\t" <> displayShow i <>
        "\n\t" <> displayShow o
      exitFailure

  let indCheck = List.zip3 [0..] (Generic.toList indices) (Storable.toList oIndices)
  for_ indCheck \(n :: Int, i, o) ->
    when (i /= o) do
      logError $
        "Index mismatch at " <> display n <>
        ":\n\t" <> displayShow i <>
        "\n\t" <> displayShow o
      exitFailure

  let attrCheck = List.zip3 [0..] (Generic.toList attrs) (Storable.toList oAttrs)
  for_ attrCheck \(n :: Int, i, o) ->
    when (i /= o) do
      logError $
        "Attribute mismatch at " <> display n <>
        ":\n\t" <> displayShow i <>
        "\n\t" <> displayShow o
      exitFailure

  let nodeCheck = List.zip3 [0..] (Generic.toList nodes) (Storable.toList oNodes)
  for_ nodeCheck \(n :: Int, i, o) ->
    when (i /= o) do
      logError $
        "Node mismatch at " <> display n <>
        ":\n\t" <> displayShow i <>
        "\n\t" <> displayShow o
      exitFailure

  when (meta /= oMeta) do
    logError $
      "Metadata mismatch:" <>
        "\n\t" <> displayShow meta <>
        "\n\t" <> displayShow oMeta
    exitFailure

measureTree
  :: Tree Gltf.SceneNode
  -> Mesh.AxisAligned Mesh.Measurements
measureTree =
  Mesh.measureAaWith
    (maybe mempty (foldMap $ Gltf.sPositions . snd) . Gltf.snPrimitives)
