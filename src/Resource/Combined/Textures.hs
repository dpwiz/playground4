module Resource.Combined.Textures where

import RIO

data Collection textures fonts a = Collection
  -- XXX: textures go first, as there should be a filler texture at [0]
  { textures :: textures a
  , fonts    :: fonts a
  }
  deriving (Show, Functor, Foldable, Traversable, Generic)

instance
  ( Applicative t
  , Applicative f
  )
  => Applicative (Collection t f) where
  pure x = Collection
    { textures = pure x
    , fonts    = pure x
    }

  af <*> ax = Collection
    { textures = textures af <*> textures ax
    , fonts    = fonts af <*> fonts ax
    }
