module Geometry.Flat
  ( coloredQuad
  , texturedQuad

  , Quad(..)
  , toVertices
  , toVertices2

  , quadPositions
  , quadUV
  , quadNormals
  ) where

import RIO

import Geomancy (Vec2, Vec4, vec2, vec3)
import Geomancy.Vec3 qualified as Vec3

import Resource.Model (Vertex(..))

data Quad a = Quad
  { quadLeftTop     :: a
  , quadRightTop    :: a
  , quadLeftBottom  :: a
  , quadRightBottom :: a
  }
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

instance Applicative Quad where
  {-# INLINE pure #-}
  pure x = Quad
    { quadLeftTop     = x
    , quadRightTop    = x
    , quadLeftBottom  = x
    , quadRightBottom = x
    }

  funcs <*> args = Quad
    { quadLeftTop     = quadLeftTop     funcs $ quadLeftTop     args
    , quadRightTop    = quadRightTop    funcs $ quadRightTop    args
    , quadLeftBottom  = quadLeftBottom  funcs $ quadLeftBottom  args
    , quadRightBottom = quadRightBottom funcs $ quadRightBottom args
    }

-- | 2 clockwise ordered triangles
toVertices :: Quad (Vertex pos attrs) -> [Vertex pos attrs]
toVertices Quad{..} =
  [ quadLeftTop, quadRightTop, quadLeftBottom
  , quadLeftBottom, quadRightTop, quadRightBottom
  ]

toVertices2 :: Quad (Vertex pos attrs) -> [Vertex pos attrs]
toVertices2 q = vertices <> reverse vertices
  where
    vertices = toVertices q

coloredQuad :: Vec4 -> Quad (Vertex Vec3.Packed Vec4)
coloredQuad color = Vertex <$> quadPositions <*> pure color

texturedQuad :: Quad (Vertex Vec3.Packed Vec2)
texturedQuad = Vertex <$> quadPositions <*> quadUV

quadPositions :: Quad Vec3.Packed
quadPositions = fmap Vec3.Packed Quad
  { quadLeftTop     = vec3 (-0.5) (-0.5) 0
  , quadRightTop    = vec3   0.5  (-0.5) 0
  , quadLeftBottom  = vec3 (-0.5)   0.5  0
  , quadRightBottom = vec3   0.5    0.5  0
  }

quadUV :: Quad Vec2
quadUV = Quad
  { quadLeftTop     = vec2 0 0
  , quadRightTop    = vec2 1 0
  , quadLeftBottom  = vec2 0 1
  , quadRightBottom = vec2 1 1
  }

quadNormals :: Quad Vec3.Packed
quadNormals = pure . Vec3.Packed $ vec3 0 0 (-1)
