module Geometry.Face where

import RIO

data Face a = Face
  { faceA :: a
  , faceB :: a
  , faceC :: a
  } deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

{-# INLINEABLE facesR #-}
facesR :: [a] -> Maybe [Face a]
facesR xs = go (Just []) xs
  where
    go acc = \case
      [] ->
        acc
      [_one] ->
        Nothing
      [_one, _two] ->
        Nothing
      faceA : faceB : faceC : next ->
        case acc of
          Nothing ->
            go (Just [Face{..}]) next
          Just old ->
            go (Just (Face{..} : old)) next