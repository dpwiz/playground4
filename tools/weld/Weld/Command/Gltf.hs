module Weld.Command.Gltf
  ( Options(..)
  , Weld.ModelPipeline(..)
  , Weld.Origin(..)
  , run
  ) where

import RIO

import Data.List qualified as List
import Data.Vector qualified as Vector
import Data.Vector.Generic qualified as Generic
import Data.Vector.Storable qualified as Storable
import Geomancy (Transform)
import Geomancy.Vec3 qualified as Vec3

import Resource.Gltf.Load qualified as Gltf
import Resource.Gltf.Model qualified as Gltf
import Resource.Gltf.Scene qualified as Gltf
import Resource.Gltf.Weld qualified as Weld
import Resource.Mesh.Types qualified as Mesh
import Resource.Model (IndexRange(..))

data Options = Options
  { optionsInput          :: FilePath
  , optionsOutput         :: FilePath
  , optionsModel          :: Weld.ModelPipeline
  , optionsValidate       :: Bool
  , optionsDoubleSide     :: Bool
  , optionsReverse        :: Bool
  , optionsTransforms     :: [Transform]
  , optionsOrigin         :: [Weld.Origin]
  , optionsMeasure        :: Bool
  , optionsMaterialOffset :: Int
  , optionsTextureOffset  :: Int
  }
  deriving (Show)

run :: HasLogFunc env => Options -> RIO env ()
run options@Options{..} = do
  (gltf, meshPrimitives) <- Gltf.loadMeshPrimitives optionsReverse optionsDoubleSide optionsInput
  logInfo $ mconcat
    [ "Loaded "
    , display (Vector.length meshPrimitives)
    , " meshes."
    ]

  for_ optionsTransforms \mat4 ->
    logDebug $ "Transform:\n" <> displayShow mat4
  let transform = mconcat optionsTransforms

  sceneTreeOriginal <- Gltf.unfoldSceneM optionsMaterialOffset transform gltf meshPrimitives

  let originalMeasurements = Weld.measureTree sceneTreeOriginal

  (sceneMeasurements, sceneTree) <- case optionsOrigin of
    [] ->
      pure (originalMeasurements, sceneTreeOriginal)
    _some -> do
      let
        originV = Weld.originV originalMeasurements optionsOrigin
        sceneTreeFixed = fmap (Weld.adjustPositions originV) sceneTreeOriginal

        fixedMeasurements = Weld.measureTree sceneTreeFixed

      logDebug $ "Origin options: " <> displayShow optionsOrigin
      logDebug $ "  Before: " <> displayShow (Mesh.middleAa originalMeasurements)
      logDebug $ "  Translation: " <> displayShow originV
      logDebug $ "  After: " <> displayShow (Mesh.middleAa fixedMeasurements)

      pure (fixedMeasurements, sceneTreeFixed)

  case optionsModel of
    Weld.ModelColoredLit    -> runPipeline options sceneMeasurements sceneTree Weld.modelColoredLit    Weld.ignoreMaterial
    Weld.ModelColoredUnlit  -> runPipeline options sceneMeasurements sceneTree Weld.modelColoredUnlit  Weld.ignoreMaterial
    Weld.ModelMaterialLit   -> runPipeline options sceneMeasurements sceneTree Weld.modelMaterial      Weld.materialNode
    Weld.ModelTexturedLit   -> runPipeline options sceneMeasurements sceneTree Weld.modelTexturedLit   Weld.texturedNode
    Weld.ModelTexturedUnlit -> runPipeline options sceneMeasurements sceneTree Weld.modelTexturedUnlit Weld.texturedNode

runPipeline
  :: ( Foldable t
     , Weld.Encodable attrs
     , Weld.Encodable node
     , Mesh.HasRange node
     , HasLogFunc env
     )
  => Options
  -> Mesh.AxisAligned Mesh.Measurements
  -> t Gltf.SceneNode
  -> Weld.MkAttrs attrs
  -> Weld.MkNode node
  -> RIO env ()
runPipeline Options{..} sceneMeasurements sceneTree mkAttrs mkNode = do
  let
    byBlendMode = Weld.partitionNodes $
      Weld.processSceneNodes (Weld.processMeshNode mkAttrs mkNode) sceneTree

  let
    nodePartitions = fmap (map snd . toList) byBlendMode
    nodesBadRanges = fold nodePartitions
    nodes = Storable.fromList $
      zipWith Mesh.adjustRange
        nodesBadRanges
        (List.scanl' (+) 0 $ map (irIndexCount . Mesh.getRange) nodesBadRanges)

  Storable.mapM_ (logDebug . displayShow) nodes
  logInfo $ "Total drawable nodes: " <> display (Generic.length nodes)

  let
    stuffByBlendMode = fmap (Gltf.mergeStuffLike . fmap fst) byBlendMode
    stuff@(positions, indices, attrs) = Gltf.mergeStuffLike stuffByBlendMode
  logMerged stuff

  let
    countIndices (_pos, npIndices, _attrs) =
      fromIntegral $ Generic.length npIndices

    meta =
      Weld.sceneMeta
        sceneMeasurements
        (fmap countIndices stuffByBlendMode)
        nodePartitions

  logMeta meta

  unless optionsMeasure $
    Weld.encode
      optionsOutput
      optionsValidate
      positions
      indices
      attrs
      nodes
      meta

logMerged
  :: HasLogFunc env
  => (Vector Vec3.Packed, Vector Word32, Vector attrs)
  -> RIO env ()
logMerged (positions, indices, _attrs) =
  logInfo $ mconcat
    [ "Merged "
    , displayShow (Vector.length positions)
    , " vertices and "
    , display (Vector.length indices)
    , " indices"
    ]

logMeta :: (HasLogFunc env) => Mesh.Meta -> RIO env ()
logMeta Mesh.Meta{..} = do
  logInfo $ "Scene AABB: "
  for_ (zip "XYZ" $ toList mMeasurements) \(axis, measurements) ->
    logInfo $ mconcat
      [ "\t"
      , display axis
      , " center: "
      , displayShow $ Mesh.middle measurements
      , " size: "
      , displayShow $ Mesh.mMax measurements - Mesh.mMin measurements
      ]

  logInfo $ "Scene mean: "
  for_ (zip "XYZ" $ toList mMeasurements) \(axis, measurements) ->
    logInfo $ mconcat
      [ "\t"
      , display axis
      , " "
      , displayShow $ Mesh.mMean measurements
      , " ± "
      , displayShow $ Mesh.mStd measurements
      ]

  logInfo $ "Opaque index range: " <> displayShow mOpaqueIndices
  logInfo $ "Blended index range: " <> displayShow mBlendedIndices

  logInfo $ "Opaque node range: " <> displayShow mOpaqueNodes
  logInfo $ "Blended node range: " <> displayShow mBlendedNodes
