module Weld.Options
  ( Options(..)
  , getOptions
  , optionsP

  , Command(..)
  ) where

import RIO

import Geomancy (Transform)
import Geomancy.Transform qualified as Transform
import Options.Applicative.Simple qualified as Opt

import Paths_playground4 qualified
import Weld.Command.Gltf qualified as Gltf

-- | Command line arguments
data Options = Options
  { optionsVerbose :: Bool
  , optionsCommand :: Command
  , optionsWorkdir :: Maybe FilePath
  }
  deriving (Show)

data Command
  = CommandGltf Gltf.Options
  -- TODO: CommandFont Font.Options
  -- TODO: CommandTexture Texture.Options
  deriving (Show)

getOptions :: IO Options
getOptions = do
  (options, ()) <- Opt.simpleOptions
    $(Opt.simpleVersion Paths_playground4.version)
    header
    description
    optionsP
    Opt.empty
  pure options
  where
    header =
      "Playground welding tool"

    description =
      mempty

optionsP :: Opt.Parser Options
optionsP = do
  optionsVerbose <- Opt.switch $ mconcat
    [ Opt.long "verbose"
    , Opt.short 'v'
    , Opt.help "Show more and more detailed messages"
    ]

  optionsWorkdir <- Opt.optional . Opt.strOption $ mconcat
    [ Opt.long "work-dir"
    , Opt.help "Change running directory"
    , Opt.metavar "PATH"
    ]

  optionsCommand <- Opt.hsubparser $ mconcat
    [ gltf
    -- , TODO: font
    -- , TODO: texture
    ]

  pure Options{..}

gltf :: Opt.Mod Opt.CommandFields Command
gltf = Opt.command "gltf" . Opt.info gltfP $ Opt.progDesc "Compress glTF scene"

gltfP :: Opt.Parser Command
gltfP = fmap CommandGltf do
  optionsInput <- Opt.strOption $ mconcat
    [ Opt.long "input"
    , Opt.short 'i'
    , Opt.help "Input glTF scene file."
    , Opt.metavar "resources.in/SOMETHING/scene.gltf"
    ]

  optionsOutput <- Opt.strOption $ mconcat
    [ Opt.long "output"
    , Opt.short 'o'
    , Opt.help "Output mesh file."
    , Opt.metavar "resources/meshes/SOMETHING.mesh"
    ]

  optionsModel <- Opt.option modelPipeline $ mconcat
    [ Opt.long "model-pipeline"
    , Opt.short 'p'
    , Opt.help "Model pipeline that will be used to render a mesh."
    ]

  optionsValidate <- Opt.switch $ mconcat
    [ Opt.long "validate"
    , Opt.help "Load output data back and compare with input."
    ]

  optionsDoubleSide <- Opt.switch $ mconcat
    [ Opt.long "double-side"
    , Opt.help "Append reverse indices to meshes with double-sided material."
    ]

  optionsReverse <- Opt.switch $ mconcat
    [ Opt.long "reverse"
    , Opt.help "Reverse mesh indices (to clockwise order) without changing vertices."
    ]

  optionsTransforms <- many $ Opt.argument transform $ mconcat
    [ Opt.help "Apply transformation at scene root."
    , Opt.metavar "TRANSFORM"
    ]

  optionsOrigin <- many $ Opt.option origin $ mconcat
    [ Opt.long "origin"
    , Opt.help $ unwords
        [ "Move vertices to match desired origin."
        , "Actual values are calculated from mesh measurements after transforms."
        ]
    , Opt.metavar "AXIS:ORIGIN"
    ]

  optionsMeasure <- Opt.switch $ mconcat
    [ Opt.long "measure"
    , Opt.help "Only measure the resulting mesh, but don't write the output file."
    ]

  optionsMaterialOffset <- Opt.option Opt.auto $ mconcat
    [ Opt.long "material-offset"
    , Opt.help "Add offset for each material ID used."
    , Opt.value 0
    ]

  optionsTextureOffset <- Opt.option Opt.auto $ mconcat
    [ Opt.long "texture-offset"
    , Opt.help "Add offset for each texture ID used."
    , Opt.value 0
    ]

  pure Gltf.Options{..}

modelPipeline :: Opt.ReadM Gltf.ModelPipeline
modelPipeline = Opt.maybeReader \case
  "lit:colored"    -> Just Gltf.ModelColoredLit
  "lit:material"   -> Just Gltf.ModelMaterialLit
  "lit:textured"   -> Just Gltf.ModelTexturedLit
  "unlit:colored"  -> Just Gltf.ModelColoredUnlit
  "unlit:textured" -> Just Gltf.ModelTexturedUnlit
  _                -> Nothing

transform :: Opt.ReadM Transform
transform = Opt.maybeReader \arg ->
  case break (':' ==) arg of
    ("turn-x", ':' : turns) -> fmap (Transform.rotateX . turns2rad) $ readMaybe turns
    ("turn-y", ':' : turns) -> fmap (Transform.rotateY . turns2rad) $ readMaybe turns
    ("turn-z", ':' : turns) -> fmap (Transform.rotateZ . turns2rad) $ readMaybe turns

    ("flip", ':' : "x") -> Just $ Transform.scale3 (-1) 1 1
    ("flip", ':' : "y") -> Just $ Transform.scale3 1 (-1) 1
    ("flip", ':' : "z") -> Just $ Transform.scale3 1 1 (-1)

    ("scale",   ':' : num)  -> Transform.scale <$> readMaybe num

    -- ("translate", ':' : )
    -- ("axis-angle", ':' : axisAngle)

    _ -> Nothing

  where
    turns2rad t = t * τ

origin :: Opt.ReadM Gltf.Origin
origin = Opt.maybeReader \case
  "x:left"   -> Just Gltf.OriginXLeft
  "x:middle" -> Just Gltf.OriginXMiddle
  "x:mean"   -> Just Gltf.OriginXMean
  "x:right"  -> Just Gltf.OriginXRight

  "y:top"    -> Just Gltf.OriginYTop
  "y:middle" -> Just Gltf.OriginYMiddle
  "y:mean"   -> Just Gltf.OriginYMean
  "y:bottom" -> Just Gltf.OriginYBottom

  "z:near"   -> Just Gltf.OriginZNear
  "z:middle" -> Just Gltf.OriginZMiddle
  "z:mean"   -> Just Gltf.OriginZMean
  "z:far"    -> Just Gltf.OriginZFar

  _ -> Nothing

τ :: Float
τ = 2 * pi
