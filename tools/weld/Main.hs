module Main (main) where

import RIO
import RIO.App (appMain, appEnv)

import Weld.Options (Command(..), Options(..), getOptions, optionsVerbose)
import Weld.Command.Gltf qualified as Gltf

main :: IO ()
main =
  appMain getOptions optionsVerbose setup run
  where
    setup Options{optionsCommand} =
      pure (optionsCommand, ())

    run =
      asks appEnv >>= \case
        CommandGltf gltf ->
          Gltf.run gltf
