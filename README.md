# playground4

A game engine (-ish) thingy to go beyound drawing a static triangle.

![screenshot](https://i.imgur.com/uJGmu17.png)

## Prerequisites

* Install Vulkan SDK from LunarG: https://vulkan.lunarg.com/sdk/home

## Execute

> I'm building with stack and can't maintain nix parts myself, but I know some would like that.
> So contributions are very welcome.

* Run `stack exec -- playground4` and use your Korg nanoKONTROL2 🤪
* With `stack exec -- playground4 --verbose` more logging will be shown, with timestamps and source locations.

## What's inside

- `examples/tutorial` - A single-module executable overview for engine basics.
- `examples/2d/texts-and-sprites` - 2D demo that updates and draws tons of sprites.
- `examples/playground4` - Unfocused playground you can rip to modules.
  * `Game` - Project-specific stuff to be swapped with the actual thing.
- `tools/weld` - Asset manipulation gadget to fix stuff and produce GPU-friendly data.
  * `gltf` - Convert `.gltf`/`.glb`/`.gtf.zst` scenes into `.p4m` meshes.
- `src/` - Code that should be split into libraries and example projects 😩
  * `Engine` - Vulkan boilerplate and lifecycle services.
  * `Render` - Non-project specific rendering stuff that can be reused between projects.
  * `Resource` - Resource loaders and lifecycle services.

### ... and outside

- [p4-starter] - Single-stage ready-to-do-nothing git repo to clone and build and expand.
- [p4-snap] - Experimental 2.71D tile editor with modest usage of DearImGUI bindings.

[p4-snap]: https://gitlab.com/dpwiz/p4-snap
[p4-starter]: https://gitlab.com/dpwiz/p4-starter

## Points of interest

### Stage stack (`Engine.Types`)

An application is divided into "Stages" that can be active one at a time.

A `Stage` type captures resources allocated and used during the entirety of its rendering loop.
There's no event loop and input events and timers are handled separately in their own threads.

Stage stack is **not** some windowing/UI system.
A `Stage` is rendering-centered and not a *game* state transition pattern.
Think of it as a set of mini-games, each with distinct world and interactions.

Stage starts with allocation of its resources and will render its frames until a stack command is produced.

The stack commands being (`Engine.Types.NextStage`):

- `Finish` - resources released and stack runner proceeds to a next stage on the stack.
    (*"Dialog" closes.*)
- `Replace` - a new stage is pushed on top and current resources are released.
    (*One "level" stage gives way to a next "level".*)
- `PushRestart` - two new stages pushed: the current and a a next one.
    Current resources are released and a fresh set would be procured when the runner gets back.
    (*"Main menu" releases its resources while a "level" is running.*)
- `PushFreeze` - resources used by current stage are "frozen" instead of released and a special stage appears under a next one.
    Only frame-recycled resources would be created anew.
    This way one stage can share resources with younger stages e.g.
    (*"Loader" stage hold permanent resources like font textures.*)

### Observable processes (`Engine.Worker`, `Game.Sim` et al.)

This playground is an antithesis of "immediate mode" engines like Gloss.

* CPU calculations aren't cheap enough.
  Data that was calculated once shouldn't be calculated again until its input have changed.
* GPU transfers aren't cheap at all.
  Buffers that were filled once should be recycled and reused if possible.
  E.g. a game is paused and there's no user input - nothing changes and GPU happily works with the same data again and again.

However, GPU resources in use need to be left alone until frame is rendered (and a framebuffer is presented).
This prompts to have a pool of similar-shaped data that should be synced with the parts of a world they represent.

The solution explored here consists of versioned variables in STM (`Engine.Worker.Var`) and IORef (`Engine.Worker.ObserverIO`).

* STM-based variables can serve both as an input or as an output of some process.
  Changes in input trigger updates downstream.
* IORef-based variables are used to derive data from upstream STM vars and updated on-demand.

The versions used in variables are actually [vector clocks] and contain versions of all the upstream involved.

[vector clocks]: https://en.wikipedia.org/wiki/Vector_clock

* Processes are waiting in STM for concatenated versions of their inputs to become lexicographically greater than before.
* Observers are comparing their last-known version to their input and trigger update actions.

> This works for constant number of inputs and some new scheme should be devised for inputs joining and parting on the go.

The example "game" demonstrates a data flow tying together multiple levels:
- Global
  * Camera projection: getting input from window resize events and calculating transforms.
- Stage
  * Camera projection: sending events to the global projection input.
  * Camera view: sending events from a worker bound to a stage.
  * Shader scene: merging together camera projection and view.
  * World timer: input and a process that publishes timer events.
  * World simulation: reacts to timer events with updates to its world.
- Frame
  * Scene observer: each frame syncing its uniform buffer with the scene var.

![svgbob:dataflow.txt](dataflow.svg)

Scene flow is implements with the basic `Var` wrappers:

- `Cell` - spawned with a pure function and an input var to do a potentially costly computation when its input changes.
- `Merge` - spawned with a pure function and a number of inputs (one procedure per different number of arguments).

Observed output of final `Merge` is checked by an `ObserverIO` when the frame is preparing its rendering resources.
`Scene` data is `Foreign`-stored into a uniform buffer that is allocated on GPU and attached to a slot in Set0 descriptor.

Time/Sim flow is a bit different:
- `Var` - passive updatable input.
- `Time` - a process that wakes up at regular intervals, checks input `Var` and *publishes* a "time delta" into a channel.
- `World` - a process that is waiting on its *subscribed* channel for a "time delta" to process and calculates a new `World` value in STM.

Frames are *observing* the output of the `World` process and update recycled vertex attribute buffer

The descriptors and buffers then used in their Stage rendering code.

When it is time to release Stage resources, the process threads are stopped and recyclable are finally released.

### P4M: compressed format for precomputed assets

Continuing the "you can (not) compute" trend, a codec is made to serialize the positions-attrs-indices trios augmented with metadata for the whole mesh and individual subranges inside.

No more parsing 10M of JSON, then 20M of binary data, then correcting the loaded mesh, then computing bounding boxes...
Okay, it's all only 2M with Zstd compression, but the loading times are way too high.

It takes 7M for P4M to hold the data from that scene, processed and sorted for your game needs that you simply uncompress and shovel into GPU.

### Static checks for bindings

Wrappers from `Render.Draw` are used together with `DescSets` and `Pipeline` binding functions to prevent accidental garbage or a freeze due to incompatible data in pipeline/descset and pipeline/vertex.

Using Loader rendering code:

```haskell
-- XXX: no draws and binds without descriptor sets
let dsl = Pipeline.pLayout $ Render.pUnlitTextured fPipelines
withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS dsl rSceneDescs do
  -- XXX: only set0-compatbile pipelines beyond this point

  Pipeline.bind cb (Render.pUnlitTextured fPipelines) $
    -- XXX: only unlit/textured draws inside
    Draw.indexed cb texturedQuad quadInstances

  Pipeline.bind cb (Render.pEvanwSdf fPipelines) $
    -- XXX: only no attrs / font-instanced draws inside
    Draw.quads cb message

  -- XXX: no draws here, bind the pipeline first

-- XXX: no draws again; and no pipeline can be bound due to lack of active descriptor set
```

### Ready-to-go rendering pipeline

The library contains all you need to start drawing scenes.

- Forward RenderPass.
- Shadowmap RenderPass.
- glTF-compatible PBR material pipeline and import tools.

Fear not the Vulkan Boilerplate!

## Previously...

The project ~~cannibalizes~~ builds upon some previous endeavors:

- `hlsl` example of [vulkan](https://github.com/expipiplus1/vulkan) library:
  * Frame loop and data recycler.
  * RefCounted allocations.
  * Handles and setup for Vulkan device, instance, etc.
  * Swapchain reinitialization.
- [vulkan-setup](https://gitlab.com/dpwiz/vulkan-setup) framework:
  * Pipeline setup.
  * Common descriptor set 0.
  * Staged and coherent buffer wrappers.
  * Refined texture and cubemap loading.
- [playground 3](https://gitlab.com/dpwiz/playground3):
  * Multisampled render pass.
  * Oneshot command buffer and image/views setup.

## Assets used

- «Viking Room» by [nigelgoh] (CC BY 4.0)
- «Flying world - Battle of the Trash god» by [burunduk] (CC BY 4.0)
- «stylised sky player home dioroma» by [Sander Vander Meiren] (CC BY 4.0)

[nigelgoh]: https://sketchfab.com/3d-models/viking-room-a49f1b8e4f5c4ecf9e1fe7d81915ad38
[burunduk]: https://sketchfab.com/3d-models/flying-world-battle-of-the-trash-god-350a9b2fac4c4430b883898e7d3c431f
[Sander Vander Meiren]: https://sketchfab.com/3d-models/stylised-sky-player-home-dioroma-6597e6c9a5184f07a638ac33c08c2ad5
