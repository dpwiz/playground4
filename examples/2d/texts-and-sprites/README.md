# Playground 4 - 2D texts and sprites

CPU particle system demo that can be used as a benchmark.

It draws 3 kinds of animated stuff on multiple layers:
- Flares: CPU particle system.
  Every particle translated to a GPU instance.
  Each emitter is a `Timed` worker with config, state and output buffers.
- Stars: Alternative way to use the same pipeline.
  A stack of backround layers with static position, but changing texture parameters.
  One `Timed` worker holds all the instances.
- Messages: Simple 2d layout demonstration.
  One group hits all the origins, another uses dynamic split ratio.
  Each has a small input and a relatively small output.
  Input text characters translated to SDF pipeline instances.

![2021-02-27](https://i.imgur.com/V2VsLuW.png)

## Features

- Stages: 1
- Pipelines:
  * Unlit/Textured
  * Font/EvanwSdf
- Resources:
  * fonts/large
  * textures/base
- World:
  * Flare :: Worker.Timed
  * Stars :: Worker.Timed
  * Message :: Worker.Merge
