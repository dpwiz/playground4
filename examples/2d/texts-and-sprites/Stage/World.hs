module Stage.World where

import RIO

import Geomancy (vec4)
import Geomancy.Vec4 qualified as Vec4
import RIO.State (gets)
import RIO.Time (getZonedTime)
import RIO.Vector qualified as Vector

import Engine.Types qualified as Engine
import Engine.UI.Message qualified as Message
import Engine.Worker qualified as Worker

import Stage.Types (RunState(..))
import Stage.World.Stars qualified as Stars

starLayers :: Vector Stars.Layer
starLayers = Vector.fromList
  [ Stars.Layer
    { direction = 1/12
    , offset    = 0
    , scale     = 1
    }
  , Stars.Layer
      { direction = 1/4 - 1/12
      , offset    = 0
      , scale     = 2
      }
  , Stars.Layer
      { direction = -1/4
      , offset    = 0
      , scale     = 0.5
      }
  ]

animateMessages :: Engine.StageRIO RunState ()
animateMessages = do
  messages <- gets rsMessageOrigins
  let size = fromIntegral $ length messages
  forever do
    now <- getZonedTime
    t <- fmap realToFrac getMonotonicTime
    for_ (zip [0..] messages) \(ix, (inputVar, _process)) -> do
      let phase = ix / size

      Worker.pushInput inputVar \input ->
        let
          color = vec4
            (1 - phase)
            (0.5 + 0.5 * cos (phase * log size * τ + t))
            (0.5 + 0.5 * sin (phase * log size * τ + t))
            1

          outline = vec4
            phase
            (0.5 + 0.5 * cos (phase * log size * τ + t))
            (0.5 + 0.5 * sin (phase * log size * τ + t))
            1
        in
          input
            { Message.inputText  = fromString $ takeWhile (/= '.') (show now)
            , Message.inputSize  = (phase + 0.5) * 64 * (1 + 0.25 * cos t)
            , Message.inputColor = if phase == 0 then 1 else color Vec4.^* phase
            , Message.inputOutline = outline Vec4.^* phase
            }
    threadDelay 10_000

τ :: Float
τ = 2 * pi
