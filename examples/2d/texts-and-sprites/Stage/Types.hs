module Stage.Types
  ( Stage
  , ScreenPasses(..)
  , Pipelines(..)
  , FrameResources(..)
  , RunState(..)
  ) where

import RIO

import Data.Tagged (Tagged)
import Vulkan.Core10 qualified as Vk

import Engine.StageSwitch (StageSwitchVar)
import Engine.Types qualified as Engine
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (RenderPass(..))
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Scene
import Render.Font.EvanwSdf.Pipeline qualified as EvanwSdf
import Render.ForwardMsaa (ForwardMsaa)
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.Unlit.Textured.Model qualified as UnlitTextured
import Render.Unlit.Textured.Pipeline qualified as UnlitTextured
import Resource.Buffer qualified as Buffer

import Stage.Resource.Font (FontCollection)
import Stage.Resource.Texture.Combined qualified as Combined
import Stage.World.Flare qualified as Flare
import Stage.World.Stars qualified as Stars

type Stage = Engine.Stage ScreenPasses Pipelines FrameResources RunState

data ScreenPasses = ScreenPasses
  { spForwardMsaa :: ForwardMsaa
  }

instance RenderPass ScreenPasses where
  allocateRenderpass_ context = ScreenPasses
    <$> ForwardMsaa.allocateMsaa context

  updateRenderpass context ScreenPasses{..} = ScreenPasses
    <$> ForwardMsaa.updateMsaa context spForwardMsaa

  refcountRenderpass ScreenPasses{..} =
    refcountRenderpass spForwardMsaa

data Pipelines = Pipelines
  { pUnlitTextured :: UnlitTextured.Pipeline
  , pEvanwSdf      :: EvanwSdf.Pipeline
  }

data FrameResources = FrameResources
  { frSceneDescs :: Tagged '[Scene] (Vector Vk.DescriptorSet)
  , frSceneData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , frScene      :: Worker.ObserverIO Scene

  , frStars  :: Stars.Observer

  , frFlares  :: Flare.Observer
  , frFlares1 :: Flare.Observer
  , frFlares2 :: Flare.Observer
  , frFlares3 :: Flare.Observer

  , frMessageSplit   :: [Message.Observer]
  , frMessageOrigins :: [Message.Observer]
  }

data RunState = RunState
  { rsStageSwitch :: StageSwitchVar
  , rsSceneP      :: Scene.Process
  , rsQuadUV      :: UnlitTextured.Model 'Buffer.Staged
  , rsFonts       :: FontCollection
  , rsTextures    :: Combined.Textures

  , rsStarsP :: Stars.Process

  , rsFlaresP  :: Flare.Process
  , rsFlares1P :: Flare.Process
  , rsFlares2P :: Flare.Process
  , rsFlares3P :: Flare.Process

  , rsMessageOrigins :: [(Worker.Var Message.Input, Message.Process)]
  , rsMessageSplit   :: [Message.Process]
  }
