{-# LANGUAGE OverloadedLists #-}

module Stage.World.Flare
  ( Process
  , Config(..)
  , spawn

  , Observer
  , newObserver
  , observe
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Vector qualified as Vector
import Data.Vector.Storable qualified as Storable
import Geomancy (vec4)
import Geomancy.Transform qualified as Transform
import Vulkan.Zero (zero)
import System.Random (newStdGen, random)
import System.Random.Stateful (applySTGen, newSTGenM)

import Engine.Types (StageRIO)
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Render.Samplers qualified as Samplers
import Render.Unlit.Textured.Model qualified as UnlitTextured
import Resource.Buffer qualified as Buffer

type Process = Worker.Timed Config FlareAttrs

data Config = Config
  { textureId  :: Int32
  , originX    :: Float
  , originY    :: Float
  , turnBase   :: Float
  , turnSpread :: Float
  , spawnRate  :: Either Float Int
  , spawnSide  :: Either () ()
  }

data Flare = Flare
  { fuse      :: Float
  , energy    :: Float
  , direction :: Float
  , x         :: Float
  , y         :: Float
  }

type FlareAttrs =
  ( Storable.Vector UnlitTextured.TextureParams
  , Storable.Vector UnlitTextured.Transform
  )

spawn :: Config -> RIO env Process
spawn = Worker.spawnTimed stepF (Left dt) True mempty

dt :: Num a => a
dt = 15_625

stepF :: Vector Flare -> Config -> RIO env (FlareAttrs, Vector Flare)
stepF current Config{..} = do
  seed <- newStdGen
  nextFlares <- evaluate $ runST do
    gen <- newSTGenM seed
    Vector.mapMaybeM (advanceEx gen) current

  seedNew <- newStdGen
  new <- evaluate $ runST do
    gen <- newSTGenM seedNew
    extra <- case spawnRate of
      Left scale -> do
        uniform <- applySTGen random gen
        pure . round $ uniform * scale
      Right limit ->
        pure . max 0 $ limit - Vector.length current
    Vector.replicateM extra do
      !fuse' <- applySTGen random gen
      !τ' <- applySTGen random gen
      pure $! spawnFlare fuse' τ'

  pure
    ( nextBufs nextFlares
    , case spawnSide of
        Left () ->
          new <> nextFlares
        Right () ->
          nextFlares <> new
    )
  where
    advanceEx gen flare = do
      !driftDir <- applySTGen random gen
      pure $! advance driftDir flare

    advance driftD Flare{..} =
      if energy > 0 then
        Just Flare
          { fuse      = fuse'
          , energy    = energy'
          , direction = direction'
          , x         = x'
          , y         = y'
          }
      else
        Nothing
      where
        fuse'  = max 0 $ fuse - dt
        energy' = energy - dt + fuse'
        direction' = direction + (driftD - 0.5) / 12
        x'     = x + cos direction'
        y'     = y + sin direction'

    maxEnergy = 20_000_000

    spawnFlare fuse' τ' = Flare
      { fuse      = dt * 45 * fuse'
      , energy    = 1_000_000
      , direction = τ * (turnBase + turnSpread * (τ' - 0.5))
      , x         = originX
      , y         = originY
      }

    nextBufs nextFlares = (textures, transforms)
      where
        textures = Storable.convert do
          Flare{..} <- nextFlares
          pure $! zero
            { UnlitTextured.tpSamplerId = Samplers.linearMip Samplers.indices
            , UnlitTextured.tpTextureId = textureId
            , UnlitTextured.tpGamma = vec4 1 1 1 (min (maxEnergy / 2) energy / maxEnergy)
            }

        transforms = Storable.convert do
          Flare{..} <- nextFlares
          pure $! mconcat
            [ Transform.scale 32
            , Transform.translate x y 0
            ]

type Observer = Worker.ObserverIO (UnlitTextured.InstanceBuffers 'Buffer.Coherent 'Buffer.Coherent)

newObserver :: Int -> ResourceT (StageRIO st) Observer
newObserver initialSize = do
  context <- ask

  (_transient, initialData) <- UnlitTextured.allocateInstancesWith
    (Buffer.createCoherent context) -- dynamic texture params
    (Buffer.createCoherent context) -- dynamic transform params
    (Buffer.destroy context)
    (replicate initialSize zero) -- reserve buffer space for some sprites

  Worker.newObserverIO initialData

observe :: HasVulkan env => Process -> Observer -> RIO env ()
observe flareP observer = do
  context <- ask
  Worker.observeIO_ flareP observer $
    UnlitTextured.updateCoherentResize_ context

τ :: Float
τ = 2 * pi
