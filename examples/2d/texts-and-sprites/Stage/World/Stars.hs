{-# LANGUAGE OverloadedLists #-}

module Stage.World.Stars
  ( Process
  , Config(..)
  , Layer(..)
  , spawn

  , Observer
  , newObserver
  , observe
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Vector qualified as Vector
import Data.Vector.Storable qualified as Storable
import Geomancy (Vec2, vec2)
import Geomancy.Transform qualified as Transform
import Vulkan.Zero (zero)

import Engine.Types (StageRIO)
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Render.Samplers qualified as Samplers
import Render.Unlit.Textured.Model qualified as UnlitTextured
import Resource.Buffer qualified as Buffer

type Process = Worker.Timed Config StarAttrs

data Config = Config
  { textureId :: Int32
  }

data Layer = Layer
  { direction :: Float
  , offset    :: Vec2
  , scale     :: Vec2
  }

type StarAttrs =
  ( Storable.Vector UnlitTextured.TextureParams
  , Storable.Vector UnlitTextured.Transform
  )

spawn :: Vector Layer -> Config -> RIO env Process
spawn = Worker.spawnTimed stepF (Left dt) True

dt :: Num a => a
dt = 15_625

stepF :: Vector Layer -> Config -> RIO env (StarAttrs, Vector Layer)
stepF layers Config{textureId} = do
  let nextStars = fmap advance layers
  pure
    ( nextBufs nextStars
    , nextStars
    )
  where
    advance old@Layer{direction, offset} = old
      { offset = offset + vec2 (cos direction) (sin direction) / 4096.0
      }

    nextBufs nextStars = (textures, transforms)
      where
        textures = Storable.convert do
          Layer{..} <- nextStars
          pure $! zero
            { UnlitTextured.tpSamplerId = Samplers.linearRepeat Samplers.indices
            , UnlitTextured.tpTextureId = textureId
            , UnlitTextured.tpOffset    = offset
            , UnlitTextured.tpScale     = scale
            }

    transforms =
      Storable.replicate (Vector.length layers) (Transform.scale 4096)

type Observer = Worker.ObserverIO (UnlitTextured.InstanceBuffers 'Buffer.Coherent 'Buffer.Coherent)

newObserver :: Int -> ResourceT (StageRIO st) Observer
newObserver initialSize = do
  context <- ask

  (_transient, initialData) <- UnlitTextured.allocateInstancesWith
    (Buffer.createCoherent context) -- dynamic texture params
    (Buffer.createCoherent context) -- dynamic transform params
    (Buffer.destroy context)
    (replicate initialSize zero) -- reserve buffer space for some sprites

  Worker.newObserverIO initialData

observe :: HasVulkan env => Process -> Observer -> RIO env ()
observe starsP observer = do
  context <- ask
  Worker.observeIO_ starsP observer $
    UnlitTextured.updateCoherentResize_ context

_τ :: Float
_τ = 2 * pi
