module Stage.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import RIO.State (gets)
import RIO.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

import Engine.Types qualified as Engine
import Engine.UI.Message qualified as Message
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
import Resource.Buffer qualified as Buffer

import Stage.Types (FrameResources(..), RunState(..), ScreenPasses(..), Pipelines(..))
import Stage.World.Flare qualified as Flare
import Stage.World.Stars qualified as Stars

updateBuffers
  :: RunState
  -> FrameResources
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Worker.observeIO_ rsSceneP frScene \_old new -> do
    _same <- Buffer.updateCoherent (Vector.singleton new) frSceneData
    pure new

  Stars.observe rsStarsP frStars

  Flare.observe rsFlaresP frFlares
  Flare.observe rsFlares1P frFlares1
  Flare.observe rsFlares2P frFlares2
  Flare.observe rsFlares3P frFlares3

  for_ (zip rsMessageOrigins frMessageOrigins) \((_input, process), buf) ->
    Message.observe process buf

  for_ (zip rsMessageSplit frMessageSplit) $
    uncurry Message.observe

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> "image index" ::: Word32
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  texturedQuad <- mapRIO fst $ gets rsQuadUV

  Worker.Versioned{vData=stars} <- readIORef frStars

  Worker.Versioned{vData=flares}  <- readIORef frFlares
  Worker.Versioned{vData=flares1} <- readIORef frFlares1
  Worker.Versioned{vData=flares2} <- readIORef frFlares2
  Worker.Versioned{vData=flares3} <- readIORef frFlares3

  messageOrigins <- for frMessageOrigins $
    fmap Worker.vData . readIORef

  messageSplit <- for frMessageSplit $
    fmap Worker.vData . readIORef

  ForwardMsaa.usePass (spForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    let dsl = Pipeline.pLayout $ pUnlitTextured fPipelines
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS dsl frSceneDescs do
      Pipeline.bind cb (pUnlitTextured fPipelines) do
        Draw.indexed cb texturedQuad stars

        Draw.indexed cb texturedQuad flares
        Draw.indexed cb texturedQuad flares1
        Draw.indexed cb texturedQuad flares2
        Draw.indexed cb texturedQuad flares3

      Pipeline.bind cb (pEvanwSdf fPipelines) do
        for_ messageOrigins $
          Draw.quads cb

        for_ messageSplit $
          Draw.quads cb
