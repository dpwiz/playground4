module Stage.Setup
  ( stackStage
  , Stage
  , stage
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.List qualified as List
import Data.Tagged (Tagged(..))
import Geomancy (vec4)
import GHC.RTS.Flags (getRTSFlags)
import RIO.App (appEnv)
import RIO.FilePath (takeBaseName)
import RIO.State (gets)
import RIO.Vector qualified as Vector
import RIO.Vector.Partial qualified as Vector (headM)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk
import Vulkan.Utils.Debug qualified as Debug
-- import RIO.Process (proc, runProcess)

import Engine.Camera qualified as Camera
import Engine.StageSwitch (getNextStage, newStageSwitchVar, trySwitchStage)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (HasSwapchain(..), Queues, getDevice)
import Engine.Worker qualified as Worker
import Geometry.Flat qualified as Flat
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Scene
import Render.Font.EvanwSdf.Pipeline qualified as EvanwSdf
import Render.Samplers qualified as Samplers
import Render.Unlit.Textured.Pipeline qualified as UnlitTextured
import Resource.Collection qualified as Collection
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.CommandBuffer (allocatePools)
import Resource.Font qualified as Font (allocateCollection, container, texture)
import Resource.Image qualified as Image
import Resource.Model qualified as Model
import Resource.Texture qualified as Texture
import Resource.Texture.Ktx1 qualified as Ktx1

import Stage.Render qualified as Render
import Stage.Resource.Font qualified as Font
import Stage.Resource.Texture.Combined qualified as Combined
import Stage.Resource.Texture.Flat qualified as Flat
import Stage.Types (Pipelines(..), FrameResources(..), RunState(..), ScreenPasses(..), Stage)
import Stage.World qualified as World
import Stage.World.Flare qualified as Flare
import Stage.World.Stars qualified as Stars

stackStage :: Engine.StackStage
stackStage = Engine.StackStage stage

stage :: Stage
stage = Engine.Stage
  { sTitle = "Main"

  , sInitialRS  = initialRunState
  , sAllocateP  = allocatePipelines
  , sInitialRR  = intialRecyclableResources
  , sBeforeLoop = async World.animateMessages

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sGetNextStage = getNextStage rsStageSwitch
  , sAfterLoop    = cancel
  }

allocatePipelines
  :: HasSwapchain swapchain
  => swapchain
  -> ScreenPasses
  -> ResourceT (Engine.StageRIO RunState) Pipelines
allocatePipelines swapchain ScreenPasses{..} = do
  -- Allocate static set of common samplers.
  (_, samplers) <- Samplers.allocate swapchain

  -- Configure descriptors for statically-known set of flat textures, no cubes, and no shadows.
  let sceneBinds = Scene.set0 samplers (Left Combined.numTextures) (Left 0) 0

  let msaa = getMultisample swapchain

  -- Derive pipelines for the screen pass.
  pUnlitTextured <- UnlitTextured.allocateBlend msaa sceneBinds spForwardMsaa
  pEvanwSdf <- EvanwSdf.allocate msaa sceneBinds spForwardMsaa

  pure Pipelines{..}

initialRunState :: Engine.StageSetupRIO (Resource.ReleaseKey, RunState)
initialRunState = do
  rts <- liftIO getRTSFlags
  logDebug $ displayShow rts

  rsStageSwitch <- newStageSwitchVar

  projection <- asks $ Engine.ghScreenP . appEnv

  (sceneKey, rsSceneP) <- Worker.registered $
    Worker.spawnMerge1 mkScene projection

  context <- ask

  (bqKey, pools) <- allocatePools context

  logInfo "Loading models"
  rsQuadUV <- Model.createStagedL context pools (Flat.toVertices Flat.texturedQuad) Nothing
  quadKey <- Resource.register $ Model.destroyIndexed context rsQuadUV

  logInfo "Loading fonts"
  (fontKey, fonts) <- Font.allocateCollection pools Font.configs

  logInfo "Loading textures"
  (textureKey, textures) <- Texture.allocateCollectionWith
    (Ktx1.createTexture pools)
    Flat.paths

  let
    rsFonts = fmap Font.container fonts

    rsTextures = CombinedTextures.Collection
      { textures = textures
      , fonts    = fmap Font.texture fonts
      }
    textureIds = fmap fst $ Collection.enumerate rsTextures

  let names = List.zip3 (toList textureIds) (toList Flat.paths) (toList rsTextures)
  for_ names \(ix, path, tx) -> do
    logDebug $ displayShow (ix, path)
    let image = Image.aiImage $ Texture.tAllocatedImage tx
    Debug.nameObject (getDevice context) image . fromString $ show ix <> ":" <> takeBaseName path

  (starsKey, rsStarsP) <- Worker.registered $
    Stars.spawn World.starLayers
      ( Stars.Config
          { textureId = Flat.stars $ CombinedTextures.textures textureIds
          }
      )

  (flaresKey, rsFlaresP) <- Worker.registered $
    Flare.spawn Flare.Config
      { textureId  = Flat.cd $ CombinedTextures.textures textureIds
      , spawnRate  = Right $ 2 ^ (14 :: Int)
      , spawnSide  = Right ()
      , originX    = 0
      , originY    = 600
      , turnBase   = -1/4
      , turnSpread = 1/3
      }

  (flares1Key, rsFlares1P) <- Worker.registered $
    Flare.spawn Flare.Config
      { textureId  = Flat.david $ CombinedTextures.textures textureIds
      , spawnRate  = Left 0 -- XXX: on first kick
      , spawnSide  = Left ()
      , originX    = -750
      , originY    = 450
      , turnBase   = -1/4 + 1/8
      , turnSpread = 1/6
      }

  (flares2Key, rsFlares2P) <- Worker.registered $
    Flare.spawn Flare.Config
      { textureId  = Flat.fashion $ CombinedTextures.textures textureIds
      , spawnRate  = Left 0 -- XXX: on first kick
      , spawnSide  = Left ()
      , originX    = 750
      , originY    = 450
      , turnBase   = -1/4 - 1/8
      , turnSpread = 1/6
      }

  (flares3Key, rsFlares3P) <- Worker.registered $
    Flare.spawn Flare.Config
      { textureId  = Flat.flare $ CombinedTextures.textures textureIds
      , spawnRate  = Left 0 -- XXX: on second kick
      , spawnSide  = Left ()
      , originX    = 0
      , originY    = 750
      , turnBase   = -1/4
      , turnSpread = 1/6
      }

  void $! async do
    threadDelay 2e6
    logInfo "Pre kick"

    Worker.modifyConfig rsFlares1P \config -> config
      { Flare.spawnRate = Right 1
      }

    Worker.modifyConfig rsFlares2P \config -> config
      { Flare.spawnRate = Right 1
      }

  void $! async do
    threadDelay 6e6
    logInfo "First kick"

    Worker.modifyConfig rsFlares1P \config -> config
      { Flare.spawnRate = Left 8
      }

    Worker.modifyConfig rsFlares2P \config -> config
      { Flare.spawnRate = Left 8
      }

  void $! async do
    threadDelay 8.5e6 -- Time offset to raise inot screen
    logInfo "Second kick"
    Worker.modifyConfig rsFlares3P \config -> config
      { Flare.spawnRate = Left 100
      }

  void $! async do
    threadDelay 45e6
    logInfo "Fading out..."
    for_ [100, 99 .. 0] \(_volume :: Int) -> do
      threadDelay 100e3
      for_ [rsFlaresP, rsFlares1P, rsFlares2P, rsFlares3P] \flare ->
        Worker.modifyConfig flare \config@Flare.Config{spawnRate} -> config
          { Flare.spawnRate =
              bimap
                (\rate -> rate * 0.9)
                (const 0)
                spawnRate
          }

  void $! async do
    threadDelay 57e6
    -- _done <- proc
    --   "mplayer"
    --   [ "idol.opus"
    --   , "-quiet"
    --   -- , "-slave"
    --   -- , "-input", "file=" <> fifo
    --   -- , "-volume", "0"
    --   , "-ss"
    --   , "195" -- "90"
    --   ]
    --   runProcess

    threadDelay 15e6

    -- TODO: when benchmark
    -- threadDelay 60e6

    Worker.Versioned{vVersion} <- readTVarIO (Worker.tOutput rsFlaresP)
    logInfo $ "Flare updates: " <> displayShow vVersion

    atomically $ trySwitchStage rsStageSwitch Engine.Finish

  -- And now for something completely different....

  let
    fontLarge = Font.large rsFonts
    fontLargeId = Font.large (CombinedTextures.fonts textureIds)

  screenBoxP <- Layout.trackScreen

  paddingVar <- Worker.newVar 32
  screenPaddedP <- Layout.padAbs screenBoxP paddingVar

  let
    origins =
      [ Layout.LeftTop
      , Layout.LeftMiddle
      , Layout.LeftBottom
      , Layout.CenterTop
      , Layout.Center
      , Layout.CenterBottom
      , Layout.RightTop
      , Layout.RightMiddle
      , Layout.RightBottom
      ]
  originInputs <- for origins \inputOrigin -> do
    let
      inputText         = ""
      inputFont         = fontLarge
      inputFontId       = fontLargeId
      inputSize         = 0 -- XXX: Animated
      inputColor        = 0
      inputOutline      = 0
      inputOutlineWidth = 4/16
      inputSmoothing    = 1/16

    inputVar <- Worker.newVar Message.Input{..}
    pure (screenPaddedP, inputVar)

  (messageKey, messageGroup) <- Worker.registeredCollection (uncurry Message.spawn) originInputs
  let rsMessageOrigins = List.zip (map snd originInputs) messageGroup

  splitVar <- Worker.newVar 0.5
  async $ forever do
    threadDelay 10_000
    t <- fmap realToFrac getMonotonicTime
    Worker.pushInput splitVar $ const (sin (t * 0.125) * 0.25 + 0.5)

  (leftHalfP, rightHalfP) <- Layout.hSplitRel screenBoxP splitVar

  leftInput <- Worker.newVar Message.Input
    { inputText         = "‹ left text ‹"
    , inputFontId       = fontLargeId
    , inputFont         = fontLarge
    , inputOrigin       = Layout.RightMiddle
    , inputSize         = 64
    , inputColor        = 1
    , inputOutline      = vec4 0 0 0 1
    , inputOutlineWidth = 4/16
    , inputSmoothing    = 1/16
    }
  leftMessageP <- Message.spawn leftHalfP leftInput

  rightInput <- Worker.newVar Message.Input
    { inputText         = "› right text ›"
    , inputFontId       = fontLargeId
    , inputFont         = fontLarge
    , inputOrigin       = Layout.LeftMiddle
    , inputSize         = 64
    , inputColor        = 1
    , inputOutline      = vec4 0 0 0 1
    , inputOutlineWidth = 4/16
    , inputSmoothing    = 1/16
    }
  rightMessageP <- Message.spawn rightHalfP rightInput
  let rsMessageSplit = [leftMessageP, rightMessageP]

  release <- Resource.register $ traverse_ Resource.release
    [ sceneKey
    , quadKey
    , fontKey
    , textureKey
    , starsKey
    , flaresKey
    , flares1Key
    , flares2Key
    , flares3Key
    , messageKey
    ]

  Resource.release bqKey

  pure (release, RunState{..})
  where
    mkScene Camera.Projection{..} = Scene.emptyScene
      { Scene.sceneProjection = projectionOrthoUI
      }

intialRecyclableResources
  :: Queues Vk.CommandPool
  -> ScreenPasses
  -> Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
intialRecyclableResources _cmdPools _renderPasses pipelines = do
  combinedTextures <- gets rsTextures
  (frSceneDescs, frSceneData, frScene) <- Scene.createSet0Ds
    (getSceneLayout pipelines)
    combinedTextures
    []
    Nothing
    mempty
    Nothing

  frStars <- Stars.newObserver $ Vector.length World.starLayers

  frFlares  <- Flare.newObserver 32768
  frFlares1 <- Flare.newObserver 32768
  frFlares2 <- Flare.newObserver 32768
  frFlares3 <- Flare.newObserver 32768

  origins <- gets rsMessageOrigins
  frMessageOrigins <- for origins \_worker ->
    Message.newObserver 80

  split <- gets rsMessageSplit
  frMessageSplit <- for split \_worker ->
    Message.newObserver 80

  pure FrameResources{..}
  where
    getSceneLayout :: Pipelines -> Tagged '[Scene] Vk.DescriptorSetLayout
    getSceneLayout Pipelines{pUnlitTextured} =
      case Vector.headM (unTagged $ Pipeline.pDescLayouts pUnlitTextured) of
        Nothing ->
          error "UnlitTextured has at least set0 in layout"
        Just set0layout ->
          Tagged set0layout
