{-# LANGUAGE DeriveAnyClass #-}

{-# OPTIONS_GHC -Wno-unused-top-binds #-}
-- XXX: the example uses shared textured and only needs some of them

module Stage.Resource.Texture.Flat
  ( Collection(..)
  , TextureCollection
  , paths
  ) where

import RIO

import Data.Distributive (Distributive(..))
import Data.Distributive.Generic (genericCollect)
import Data.Functor.Rep (Co(..), Representable)
import GHC.Generics (Generic1)

import Resource.Static as Static
import Resource.Texture (Texture, Flat)

data Collection a = Collection
  { black   :: a
  , cd      :: a
  , david   :: a
  , fashion :: a
  , flare   :: a
  , stars   :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Co Collection)
  deriving anyclass (Representable)

instance Distributive Collection where
  collect = genericCollect

type TextureCollection = Collection (Int32, Texture Flat)

Static.filePatterns Static.Files "resources/textures/base"

paths :: Collection FilePath
paths = Collection
  { black   = BLACK_KTX_ZST
  , cd      = CD_KTX_ZST
  , david   = DAVID_KTX_ZST
  , fashion = FASHION_KTX_ZST
  , flare   = FLARE_KTX_ZST
  , stars   = STARS_KTX_ZST
  }
