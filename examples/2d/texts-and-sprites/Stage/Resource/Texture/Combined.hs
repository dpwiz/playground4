module Stage.Resource.Texture.Combined where

import RIO

import Resource.Collection (enumerate, size)
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.Texture (Texture, Flat)

import Stage.Resource.Font qualified as Font
import Stage.Resource.Texture.Flat qualified as Flat

type TextureCollectionF a =
  CombinedTextures.Collection
    Flat.Collection
    Font.Collection
    a

type CombinedCollection = TextureCollectionF (Int32, Texture Flat)
type Ids                = TextureCollectionF Int32
type Textures           = TextureCollectionF (Texture Flat)

paths :: TextureCollectionF FilePath
paths = CombinedTextures.Collection
  { textures = Flat.paths
  , fonts    = fmap Font.configTexture Font.configs
  }

indices :: TextureCollectionF Int32
indices = fmap fst $ enumerate paths

numTextures :: Num a => a
numTextures = size paths
