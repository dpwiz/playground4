{-# LANGUAGE DeriveAnyClass #-}

module Stage.Resource.Font
  ( Collection(..)
  , ConfigCollection
  , FontCollection
  , TextureCollection
  , Font.Config(..)
  , configs
  ) where

import RIO

import Data.Distributive (Distributive(..))
import Data.Distributive.Generic (genericCollect)
import Data.Functor.Rep (Co(..), Representable)
import GHC.Generics (Generic1)

import Resource.Font qualified as Font
import Resource.Font.EvanW qualified as EvanW
import Resource.Static as Static
import Resource.Texture (Texture, Flat)

type ConfigCollection = Collection Font.Config
type FontCollection = Collection EvanW.Container
type TextureCollection = Collection (Texture Flat)

data Collection a = Collection
  { small :: a
  , large :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Co Collection)
  deriving anyclass (Representable)

instance Distributive Collection where
  collect = genericCollect

Static.filePatterns Static.Files "resources/fonts/evanw-sdf"

configs :: Collection Font.Config
configs = Collection
  { small = Font.Config
      { Font.configContainer = UBUNTU_32_5_JSON
      , Font.configTexture   = UBUNTU_32_5_KTX_ZST
      }
  , large = Font.Config
      { Font.configContainer = UBUNTU_256_8_JSON
      , Font.configTexture   = UBUNTU_256_8_KTX_ZST
      }
  }
