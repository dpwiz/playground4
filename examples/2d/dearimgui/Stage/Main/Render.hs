module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import RIO.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui
import Resource.Buffer qualified as Buffer

import Stage.Main.Types (FrameResources(..), RunState(..), ScreenPasses(..), Pipelines(..))
import Stage.Main.Render.Dear qualified as Dear

updateBuffers
  :: RunState
  -> FrameResources
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Worker.observeIO_ rsSceneP frScene \_old new -> do
    _same <- Buffer.updateCoherent (Vector.singleton new) frSceneData
    pure new

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> "image index" ::: Word32
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  dear <- Dear.imguiDrawData

  ForwardMsaa.usePass (spForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    let dsl = Pipeline.pLayout $ pUnlitColored fPipelines
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS dsl frSceneDescs do
      -- XXX: Draw you stuff here, for DearImGui to blend over.
      pure ()

    ImGui.draw dear cb
