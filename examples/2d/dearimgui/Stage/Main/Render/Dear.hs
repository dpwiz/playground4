{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Render.Dear where

import RIO

import DearImGui qualified
import RIO.State (gets)

import Engine.Events.Sink (Sink(..))
import Engine.Types (StageFrameRIO)
import Engine.Worker qualified as Worker
import Render.ImGui qualified as ImGui

import Stage.Main.Event.Type qualified as Event
import Stage.Main.Types (RunState(..))

imguiDrawData :: StageFrameRIO rp p fr RunState DearImGui.DrawData
imguiDrawData = fmap snd $ ImGui.mkDrawData do
  mainMenu
  someWindows
  annoyingTooltip

mainMenu :: StageFrameRIO rp p fr RunState ()
mainMenu =
  DearImGui.withMainMenuBarOpen do
    DearImGui.menuItem "Item" >>= \clicked ->
      when clicked $
        logInfo "Test clicked"

    DearImGui.withMenuOpen "Menu" do
      DearImGui.menuItem "Test item" >>= \clicked ->
        when clicked $
          logInfo "Test clicked"

      DearImGui.withMenuOpen "Submenu" do
        DearImGui.menuItem "Test item" >>= \clicked ->
          when clicked $
            logInfo "Test clicked"

mainMenuPadding :: StageFrameRIO rp p fr RunState ()
mainMenuPadding = do
  DearImGui.spacing
  DearImGui.spacing
  DearImGui.spacing
  DearImGui.spacing

someWindows :: StageFrameRIO rp p fr RunState ()
someWindows = do
  fullscreenVar <- mapRIO fst $ gets rsShowFullscreen

  testWindow1
  testWindow2
  testWindow3

  unnamed

  wants fullscreenVar
  fullscreen fullscreenVar

testWindow1 :: StageFrameRIO rp p fr RunState ()
testWindow1 = do
  DearImGui.setNextWindowSize
    (pure @IO $ DearImGui.ImVec2 500 200)
    DearImGui.ImGuiCond_None
  DearImGui.withWindowOpen "Test window" do
    DearImGui.text "Warning: Avoiding success may incur some costs."

testWindow2 :: StageFrameRIO rp p fr RunState ()
testWindow2 = do
  -- XXX: the constraints are ignored since the window is already created.
  DearImGui.setNextWindowSize
    (pure @IO $ DearImGui.ImVec2 50 50)
    DearImGui.ImGuiCond_None
  DearImGui.withWindowOpen "Test window" do
    DearImGui.text "NB: using the same title adds items to the same window."

testWindow3 :: StageFrameRIO rp p fr RunState ()
testWindow3 = do
  DearImGui.setNextWindowSize
    (pure @IO $ DearImGui.ImVec2 500 200)
    DearImGui.ImGuiCond_None
  DearImGui.withWindowOpen "Test window##Not the same" do
    DearImGui.text "NB: use \"##suffixes\" to distinguish similarly-named names"

unnamed :: StageFrameRIO rp p fr RunState ()
unnamed = do
  DearImGui.setNextWindowSizeConstraints
    (pure @IO $ DearImGui.ImVec2 500 100)
    (pure @IO $ DearImGui.ImVec2 800 800)
  DearImGui.setNextWindowBgAlpha 0.5
  DearImGui.withWindowOpen "##Unnamed" do
    DearImGui.text "The window has no name."
    DearImGui.text "It's got some constraints though."

wants :: Worker.Var Bool -> StageFrameRIO rp p fr RunState ()
wants fullscreenVar = do
  DearImGui.setNextWindowSizeConstraints
    (pure @IO $ DearImGui.ImVec2 200 100)
    (pure @IO $ DearImGui.ImVec2 200 100)
  DearImGui.withWindowOpen "Wants" do
    wantMouse <- DearImGui.wantCaptureMouse
    DearImGui.text $ "Mouse: " <> show wantMouse

    wantKb <- DearImGui.wantCaptureKeyboard
    DearImGui.text $ "Keyboard: " <> show wantKb

    DearImGui.checkbox "Fullscreen window" (Worker.stateVar fullscreenVar) >>= \changed ->
      when changed $
        logInfo "The checkbox has been clicked"

fullscreen :: Worker.Var Bool -> StageFrameRIO rp p fr RunState ()
fullscreen fullscreenVar = do
  showFullscreen <- Worker.getOutputData fullscreenVar
  when showFullscreen $
    DearImGui.withFullscreen do
      mainMenuPadding

      DearImGui.text "This text is very full-screen."
      DearImGui.text "This window will always want some mouse."
      DearImGui.text "The keyboard events will go through. Press F2 to toggle."

      DearImGui.button "Begone!" >>= \clicked ->
        when clicked $
          mapRIO fst (gets rsEvents) >>= \case
            Nothing ->
              logError "RunState is not ready to receive events"
            Just (Sink signal) -> do
              -- sceneFile <- Worker.getOutputData $ UI.sceneFile ui
              mapRIO fst . signal $ Event.ToggleFullscreen

annoyingTooltip :: StageFrameRIO rp p fr RunState ()
annoyingTooltip =
  DearImGui.withTooltip do
    t <- fmap realToFrac getMonotonicTime

    DearImGui.text "Hi! I am an annoying tooltip."
    DearImGui.text "I'm here to annoy you."
    DearImGui.separator
    DearImGui.plotHistogram "" do
      x <- [ 0.0, 0.05 .. 1.0 ]
      pure . sin $ 4 * pi * (x + t / 10.0)

    DearImGui.button "Close" >>= \clicked ->
      when clicked $
        logInfo "LOL, you did it!"
