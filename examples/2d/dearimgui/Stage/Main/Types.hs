module Stage.Main.Types
  ( Stage
  , ScreenPasses(..)
  , Pipelines(..)
  , FrameResources(..)
  , RunState(..)
  ) where

import RIO

import Data.Tagged (Tagged)
import Geomancy (Vec2)
import Vulkan.Core10 qualified as Vk

import Engine.Events qualified as Events
import Engine.StageSwitch (StageSwitchVar)
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (RenderPass(..))
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Scene
import Render.ForwardMsaa (ForwardMsaa)
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.Unlit.Colored.Pipeline qualified as UnlitColored
import Resource.Buffer qualified as Buffer

import Stage.Main.Event.Type (Event)

type Stage = Engine.Stage ScreenPasses Pipelines FrameResources RunState

data ScreenPasses = ScreenPasses
  { spForwardMsaa :: ForwardMsaa
  }

instance RenderPass ScreenPasses where
  allocateRenderpass_ context = ScreenPasses
    <$> ForwardMsaa.allocateMsaa context

  updateRenderpass context ScreenPasses{..} = ScreenPasses
    <$> ForwardMsaa.updateMsaa context spForwardMsaa

  refcountRenderpass ScreenPasses{..} =
    refcountRenderpass spForwardMsaa

data Pipelines = Pipelines
  { pUnlitColored :: UnlitColored.Pipeline
  , pWireframe    :: UnlitColored.Pipeline
  }

data FrameResources = FrameResources
  { frSceneDescs :: Tagged '[Scene] (Vector Vk.DescriptorSet)
  , frSceneData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , frScene      :: Worker.ObserverIO Scene
  }

data RunState = RunState
  { rsStageSwitch :: StageSwitchVar

  , rsSceneP      :: Scene.Process

  , rsEvents      :: Maybe (Events.Sink Event RunState)

  , rsCursorPos   :: Worker.Var Vec2
  , rsCursorP     :: Worker.Merge Vec2

  , rsShowFullscreen :: Worker.Var Bool
  }
