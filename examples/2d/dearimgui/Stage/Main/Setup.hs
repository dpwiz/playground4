module Stage.Main.Setup
  ( stackStage
  , Stage
  , stage
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Geomancy (vec2, pattern WithVec2)
import RIO.App (appEnv)
import RIO.State (gets, modify')
import RIO.Vector.Partial qualified as Vector (headM)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Events.CursorPos qualified as CursorPos
import Engine.Events.MouseButton qualified as MouseButton
import Engine.StageSwitch (getNextStage, newStageSwitchVar)
import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (HasSwapchain(..), Queues)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Scene
import Render.ImGui qualified as ImGui
import Render.Unlit.Colored.Pipeline qualified as UnlitColored

import Stage.Main.Render qualified as Render
import Stage.Main.Types (Pipelines(..), FrameResources(..), RunState(..), ScreenPasses(..), Stage)
import Stage.Main.Event.Key qualified as Key
import Stage.Main.Event.MouseButton qualified as MouseButton
import Stage.Main.Event.Sink (handleEvent)

stackStage :: Engine.StackStage
stackStage = Engine.StackStage stage

stage :: Stage
stage = Engine.Stage
  { sTitle = "Main"

  , sInitialRS  = initialRunState
  , sAllocateP  = allocatePipelines
  , sInitialRR  = intialRecyclableResources
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sGetNextStage = getNextStage rsStageSwitch
  , sAfterLoop    = afterLoop
  }
  where
    beforeLoop = do
      cursorWindow <- gets rsCursorPos
      cursorCentered <- gets rsCursorP

      (key, sink) <- Events.spawn
        handleEvent
        [ CursorPos.callback cursorWindow
        , MouseButton.callback cursorCentered MouseButton.clickHandler
        , Key.callback
        ]

      modify' \rs -> rs
        { rsEvents = Just sink
        }

      ImGui.beforeLoop True

      pure key

    afterLoop key = do
      ImGui.afterLoop
      Resource.release key

allocatePipelines
  :: HasSwapchain swapchain
  => swapchain
  -> ScreenPasses
  -> ResourceT (Engine.StageRIO RunState) Pipelines
allocatePipelines swapchain ScreenPasses{..} = do
  let sceneBinds = Scene.set0_
  let msaa = getMultisample swapchain

  -- Derive pipelines for the screen pass.
  pUnlitColored <- UnlitColored.allocate True msaa sceneBinds spForwardMsaa
  pWireframe    <- UnlitColored.allocateWireframe True msaa sceneBinds spForwardMsaa

  void $! ImGui.allocate swapchain spForwardMsaa 0

  pure Pipelines{..}

initialRunState :: Engine.StageSetupRIO (Resource.ReleaseKey, RunState)
initialRunState = do
  rsStageSwitch <- newStageSwitchVar

  projection <- asks $ Engine.ghScreenP . appEnv

  rsCursorPos <- Worker.newVar 0
  (cursorKey, rsCursorP) <- Worker.registered $
    Worker.spawnMerge2
      (\Camera.ProjectionInput{projectionScreen} window ->
        let
          Vk.Extent2D{width, height} = projectionScreen
          WithVec2 windowX windowY = window
        in
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      (Worker.getInput projection)
      rsCursorPos

  (sceneKey, rsSceneP) <- Worker.registered $
    Worker.spawnMerge1 mkScene projection

  release <- Resource.register $ traverse_ Resource.release
    [ sceneKey
    , cursorKey
    ]

  let rsEvents = Nothing
  rsShowFullscreen <- Worker.newVar False

  pure (release, RunState{..})
  where
    mkScene Camera.Projection{..} = Scene.emptyScene
      { Scene.sceneProjection = projectionOrthoUI
      }

intialRecyclableResources
  :: Queues Vk.CommandPool
  -> ScreenPasses
  -> Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
intialRecyclableResources _cmdPools _renderPasses pipelines = do
  (frSceneDescs, frSceneData, frScene) <- Scene.createSet0Ds_
    (getSceneLayout pipelines)

  pure FrameResources{..}
  where
    getSceneLayout :: Pipelines -> Tagged '[Scene] Vk.DescriptorSetLayout
    getSceneLayout Pipelines{pUnlitColored} =
      case Vector.headM (unTagged $ Pipeline.pDescLayouts pUnlitColored) of
        Nothing ->
          error "UnlitColored has at least set0 in layout"
        Just set0layout ->
          Tagged set0layout
