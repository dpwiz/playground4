module Stage.Main.Event.Type
  ( Event(..)
  ) where

import RIO

data Event
  = DoNothing
  | ToggleFullscreen
  deriving (Eq, Show, Generic)
