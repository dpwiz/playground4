# Playground 4 - DearImGui integration

Widget demo and some GLFW event handling gotchas.

https://github.com/haskell-game/dear-imgui.hs

The main action is in `Stage.Main.Render.Dear`.

There are extra hoop-jumping in `Stage.Main.Setup` to allow using stage event sink with DearImGui actions.

Other than that it's a vanilla 2D setup, ready to be extended with some shape-drawing.

![2021-06-04](https://i.imgur.com/2eEI3bt.png)

## Features

- Stages: 1
- Pipelines:
  * Whatever the DearImGui uses.
- Resources:
  * None
- World:
  * None
