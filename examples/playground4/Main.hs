module Main (main) where

import RIO hiding (traceM)

import Debug.Trace (traceM)
import RIO.Directory (doesDirectoryExist, getCurrentDirectory, withCurrentDirectory)
import RIO.FilePath (takeDirectory, takeFileName, (</>))
import System.Environment (getExecutablePath)

import Engine.App (engineMain)

import Stage.Loader.Setup (stackStage)
import Paths_playground4 (getDataDir)

main :: IO ()
main = do
  currentDir <- getCurrentDirectory

  executableDir <- fmap takeDirectory getExecutablePath
  let shareDir = takeDirectory executableDir </> "share"

  usrShareName <- fmap takeFileName getDataDir
  let
    withVer = usrShareName
    sansVer = stripVersion withVer

  let
    candidates =
      [ currentDir
      , shareDir </> withVer
      , shareDir </> sansVer
      ]
  exists <- for candidates \dir ->
    doesDirectoryExist (dir </> "resources")
  let checked = (zip exists candidates)
  case filter fst checked of
    (True, found) : _rest ->
      withCurrentDirectory found $
        engineMain stackStage
    _ -> do
      traceM "Resource directory not found:"
      traverse_ traceM candidates
      exitFailure

stripVersion :: String -> String
stripVersion = reverse . drop 1 . dropWhile (/= '-') . reverse
