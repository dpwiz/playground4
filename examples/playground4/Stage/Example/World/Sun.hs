module Stage.Example.World.Sun
  ( Process
  , SunInput(..)
  , mkSun
  ) where

import RIO hiding (view)

import Geomancy (Transform, Vec3, Vec4, vec3, vec4)
import Geomancy.Transform qualified as Transform
import Geomancy.Vec4 qualified as Vec4

import Engine.Worker qualified as Worker
import Render.DescSets.Sun (Sun(..))

type Process = Worker.Cell SunInput (Transform, Sun)

data SunInput = SunInput
  { siColor      :: Vec4

  , siInclination :: Float
  , siAzimuth     :: Float
  , siRadius      :: Float

  , siTarget     :: Vec3
  , siDepthRange :: Float
  , siSize       :: Float
  , siShadowIx   :: Float
  }

mkSun :: SunInput -> (Transform, Sun)
mkSun SunInput{..} =
  ( bbTransform
  , Sun
      { sunViewProjection = mconcat vp
      , sunShadow         = vec4 0 0 siShadowIx siSize
      , sunPosition       = Vec4.fromVec3 position 0
      , sunDirection      = Vec4.fromVec3 direction 0
      , sunColor          = siColor
      }
  )
  where
    vp =
      [ Transform.rotateY (-siAzimuth)
      , Transform.rotateX (-siInclination)

      , Transform.translate 0 0 siRadius

      -- XXX: some area beyond the near plane receives light, but not shadows
      , Transform.scale3
          (1 / siSize)
          (1 / siSize)
          (1 / siDepthRange)
      ]

    position = Transform.apply (vec3 0 0 siRadius) rotation

    direction = Transform.apply (vec3 0 0 $ -1) rotation

    bbTransform = mconcat
      [ -- XXX: orient wire box "green/near -> far/red"
        Transform.rotateX (τ/4)
        -- XXX: the rest must be matched with VP flipped
      , Transform.translate 0 0 0.5                 -- XXX: shift origin to the near face
      , Transform.scale3 siSize siSize siDepthRange -- XXX: size to projection volume
      , Transform.translate 0 0 (-siRadius)         -- XXX: translate near face to radius
      , rotation                                    -- XXX: apply sphere coords
      ]

    rotation = mconcat
      [ Transform.rotateX siInclination
      , Transform.rotateY siAzimuth
      ]

τ :: Float
τ = 2 * pi
