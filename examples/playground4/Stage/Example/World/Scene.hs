module Stage.Example.World.Scene where

import RIO

import Geomancy (Vec3, vec4)
import Geomancy.Vec4 qualified as Vec4
import Geomancy.Transform qualified as Transform

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene(..))

import Global.Resource.CubeMap qualified as CubeMap

type InputVar = Worker.Var Input

data Input = Input
  { iFogColor   :: Vec3
  , iFogScatter :: Float

  , iTweak0     :: Float
  , iTweak1     :: Float
  , iTweak2     :: Float
  , iTweak3     :: Float
  }

initialInput :: Input
initialInput = Input
  { iFogColor   = 0.5
  , iFogScatter = 1 / 32768

  , iTweak0 = 0
  , iTweak1 = 0
  , iTweak2 = 0
  , iTweak3 = 0
  }

type Process = Worker.Merge Scene

mkScene :: Camera.Projection -> Camera.View -> Input -> Scene
mkScene Camera.Projection{..} Camera.View{..} Input{..} = Scene{..}
  where
    sceneProjection    = projectionPerspective
    sceneInvProjection = Transform.inverse projectionPerspective -- FIXME: move to cell output

    sceneView          = viewTransform
    sceneInvView       = viewTransformInv
    sceneViewPos       = viewPosition
    sceneViewDir       = viewDirection

    sceneFog           = Vec4.fromVec3 iFogColor iFogScatter
    sceneEnvCube       = CubeMap.milkyway CubeMap.indices -- TODO: use input var
    sceneNumLights     = 2 -- TODO: use merge3 arg
    sceneTweaks        = vec4 iTweak0 iTweak1 iTweak2 iTweak3

mkSceneUi :: Camera.Projection -> Camera.View -> Input -> Scene
mkSceneUi camera@Camera.Projection{..} cameraView input =
  (mkScene camera cameraView input)
    { sceneProjection    = projectionOrthoUI
    , sceneInvProjection = Transform.inverse projectionOrthoUI -- FIXME: move to cell output
    , sceneView          = mempty
    , sceneInvView       = mempty
    }
