{-# LANGUAGE OverloadedLists #-}

module Stage.Example.World.Env
  ( Input(..)
  , Env
  , Process
  , spawn

  , Buffer
  , Observer
  , newObserver
  , observe
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as Resource
import Foreign qualified
import Geomancy (Vec4, vec4)
import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer

type Process = Worker.Cell Input Env

data Input = Input
  { azimuth     :: Float
  , inclination :: Float
  , altitude    :: Float
  }
  deriving (Eq, Show)

data Env = Env
  { envLocation :: Vec4 -- ^ Cartesian XYZ + altitude
  , envBodies   :: Vec4 -- ^ Planet radius, atmo height. Sun radius, distance.
  , envRayleigh :: Vec4 -- ^ Beta RGB + altitude scale
  , envMie      :: Vec4 -- ^ Beta RGB + altitude scale
  , envFancy    :: Vec4 -- ^ Ground diffuse+specular, cloud density, 2*unused
  }

instance Storable Env where
  alignment ~_ = 4
  sizeOf ~_ = 16 + 16 + 16 + 16 + 16

  peek ptr = do
    envLocation <- Foreign.peekByteOff ptr  0
    envBodies   <- Foreign.peekByteOff ptr 16
    envRayleigh <- Foreign.peekByteOff ptr 32
    envMie      <- Foreign.peekByteOff ptr 48
    envFancy <- Foreign.peekByteOff ptr 64
    pure Env{..}

  poke ptr Env{..} = do
    Foreign.pokeByteOff ptr  0 envLocation
    Foreign.pokeByteOff ptr 16 envBodies
    Foreign.pokeByteOff ptr 32 envRayleigh
    Foreign.pokeByteOff ptr 48 envMie
    Foreign.pokeByteOff ptr 64 envFancy

spawn :: MonadUnliftIO m => Input -> m Process
spawn = Worker.spawnCell mkEnv

mkEnv :: Input -> Env
mkEnv Input{..} = Env
  { envLocation =
      vec4
        (sincl * cazim)
        (-cincl)
        (sincl * sazim)
        altitude
  , envBodies =
      vec4
        6360 -- earthRadius, km
        60   -- atmoHeight
        2772960 -- distance to sun
        1.496e8 -- sun radius
  , envRayleigh =
      vec4
        5.8e-4
        1.35e-3
        3.31e-3
        6.0
  , envMie =
      vec4
        4.0e-3
        4.0e-3
        4.0e-3
        1.2
  , envFancy =
      vec4
        2 -- ground diffuse/spec id
        0
        3 -- clouds density id
        5 -- clouds altitude
  }
  where
    sincl = sin inclination'
    cincl = cos inclination'
    sazim = sin azimuth'
    cazim = cos azimuth'

    azimuth'     = azimuth * τ
    inclination' = inclination * τ

type Buffer = Buffer.Allocated 'Buffer.Coherent Env

type Observer = Worker.ObserverIO Buffer

newObserver :: ResourceT (Engine.StageRIO st) Observer
newObserver = do
  context <- ask

  envData <- Buffer.createCoherent context Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT 1 mempty
  observer <- Worker.newObserverIO envData

  void $! Resource.register do
    vData <- readIORef observer
    Buffer.destroyAll context vData

  pure observer

observe
  :: ( Worker.HasOutput source
     , Worker.GetOutput source ~ Env
     )
  => source
  -> Observer
  -> RIO env ()
observe envP observer =
  Worker.observeIO_ envP observer \ol ne ->
    Buffer.updateCoherent [ne] ol

τ :: Float
τ = 2 * pi
