{-# LANGUAGE OverloadedLists #-}

module Stage.Example.Render where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as ResourceT
import Data.List qualified as List
import Data.Vector.Storable qualified as Storable
import Geomancy (vec4)
import Geomancy.Transform qualified as Transform
import GHC.Float (double2Float)
import RIO.State (gets)
import RIO.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

import Engine.Types (StageRIO, StageFrameRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (extendDS, withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 qualified as Scene
import Render.DescSets.Sun qualified as Sun
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardPass
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Buffer qualified as Buffer
import Resource.Image qualified as Image
import Resource.Mesh.Types (Meta(..))
import Resource.Mesh.Types qualified as Mesh
import Resource.Model qualified as Model
import Render.ImGui qualified as ImGui

import Global.Render qualified as Render
import Global.Render.SkySun.DescSets qualified as SkySun
import Global.Resource.Model qualified as GameModel
import Stage.Example.Types (Resources(..), RunState(..))
import Stage.Example.UI qualified as UI
import Stage.Example.World.Env qualified as Env
import Stage.Example.World.Sun (SunInput(..), mkSun)
import Stage.Example.Render.UI qualified as RenderUI

initialData
  :: Queues Vk.CommandPool
  -> Render.RenderPasses
  -> Render.Pipelines
  -> ResourceT (StageRIO RunState) Resources
initialData _pools renderPasses pipelines = do
  context <- ask

  (rSunDescs, rSunData) <- Sun.createSet0Ds (Render.getSunLayout pipelines)
  rSunOut <- Worker.newObserverIO mempty

  let sceneLayouts = Pipeline.pDescLayouts $ Render.pSkySun pipelines

  combinedTextures <- gets $ fmap snd . rsTextures
  combinedCubes <- gets $ fmap snd . rsCubeMaps
  combinedMaterials <- gets rsMaterials

  materials <- Buffer.createCoherent
    context
    Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
    0
    combinedMaterials

  void $! ResourceT.register $
    Buffer.destroy context materials

  (ds0, rSceneData, rScene) <- Scene.createSet0Ds
    (Render.getSceneLayout pipelines)
    combinedTextures
    combinedCubes
    (Just rSunData)
    [ Image.aiImageView . ShadowPass.smDepthImage $
        Render.spShadowPass renderPasses
    ]
    (Just materials)

  ds1 <- SkySun.createSet1 sceneLayouts

  rEnv <- Env.newObserver
  envP <- gets rsEnvP
  lift $ Env.observe envP rEnv
  envData <- Worker.readObservedIO rEnv
  SkySun.updateSet1 ds1 envData

  let rSceneDescs = extendDS ds0 ds1

  (ds0ui, rSceneUiData, rSceneUi) <- Scene.createSet0Ds
    (Render.getSceneLayout pipelines)
    combinedTextures
    combinedCubes
    (Just rSunData)
    [ Image.aiImageView . ShadowPass.smDepthImage $
        Render.spShadowPass renderPasses
    ]
    (Just materials)
  let rSceneUiDescs = extendDS ds0ui ds1

  worldThingsData <- Buffer.createCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1000 mempty
  rWorldThings <- Worker.newObserverIO worldThingsData

  logReleaseThings <- toIO $ logDebug "Releasing Example recycled resources"
  void $! ResourceT.register do
    logReleaseThings
    Worker.readObservedIO rWorldThings >>=
      Buffer.destroy context

  ui <- gets rsUI
  rUI <- UI.newObserver ui

  pure Resources{..}

updateBuffers
  :: RunState
  -> Resources
  -> StageFrameRIO Render.ScreenPasses Render.Pipelines Resources RunState ()
updateBuffers RunState{..} Resources{..} = do
  Worker.observeIO_ rsSceneP rScene \_old new -> do
    -- XXX: must stay the same or descsets must be updated with a new buffer
    _same <- Buffer.updateCoherent (Vector.singleton new) rSceneData
    pure new

  Worker.observeIO_ rsSceneUiP rSceneUi \_old new -> do
    -- XXX: must stay the same or descsets must be updated with a new buffer
    _same <- Buffer.updateCoherent (Vector.singleton new) rSceneUiData
    pure new

  Worker.observeIO_ rsSunP rSunOut \_oldBBs (sunBB, sun) -> do
    -- XXX: must stay the same or descsets must be updated with a new buffer
    _same <- Buffer.updateCoherent [sun, staticMoon] rSunData
    pure [sunBB, moonBB]

  Env.observe rsEnvP rEnv

  UI.observe rsUI rUI
  where
    -- TODO: actually a backlight, depends on Env location
    (moonBB, staticMoon) = mkSun SunInput
      { siColor       = vec4 0.125 0.25 1 1 -- vec4 0.0625 0.0625 0.125 1
      , siInclination = -0.25 * τ + 1/128
      , siAzimuth     = 0
      , siRadius      = 768

      , siTarget     = 0
      , siDepthRange = 768 * 2
      , siSize       = 768

      , siShadowIx   = 1
      }

recordCommands
  :: Vk.CommandBuffer
  -> Resources
  -> "imageIndex" ::: Word32
  -> StageFrameRIO Render.ScreenPasses Render.Pipelines Resources RunState ()
recordCommands cb Resources{..} imageIndex = do
  (context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  thingsModel <- mapRIO fst . gets $ GameModel.icosphere1 . rsModels
  bbWire <- mapRIO fst . gets $ GameModel.bbWire . rsModels
  thingsInstances <- Worker.readObservedIO rWorldThings

  (battleMeta, bNodes, battleModel) <- mapRIO fst . gets $ GameModel.battle . rsModels
  (homeMeta, hNodes, homeModel) <- mapRIO fst . gets $ GameModel.skyHome . rsModels
  (roomMeta, _rNodes, roomModel) <- mapRIO fst . gets $ GameModel.vikingRoom . rsModels

  sunBBs <- Worker.readObservedIO rSunOut

  t <- fmap double2Float getMonotonicTime

  -- Battle

  let
    battleOpaqueNodes = fromIntegral . Model.irIndexCount $ Mesh.mOpaqueNodes battleMeta
    battleTransform = mempty
  battleInstance <- Buffer.createCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    [battleTransform]
  ResourceT.register $ Buffer.destroy context battleInstance

  -- let
  --   generatorOpaqueNodes = fromIntegral . Model.irIndexCount $ Mesh.mOpaqueNodes generatorMeta

  --   -- generatorTransform = mempty
  --   generatorTransform =
  --     Transform.rotateY $ -τ * t / 128

  --   generatorParts = do
  --     mn <- Storable.toList gNodes
  --     pure
  --       ( Mesh.getRange mn
  --       , generatorTransform
  --       )
  --   (generatorRanges, generatorInstanceAttrs) = List.unzip generatorParts

  -- (_transient, generatorInstances) <- Buffer.allocateCoherent
  --   context
  --   Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
  --   1
  --   (Storable.fromList generatorInstanceAttrs)

  -- SkyHome

  let
    homeOpaqueNodes = fromIntegral . Model.irIndexCount $ Mesh.mOpaqueNodes homeMeta

    homeTransform = mconcat
      [ Transform.rotateY $ τ * t / 128
      , Transform.translate
          (-250)
          (10 * sin (τ * t / 33))
          0
      ]

    homeParts = do
      mn <- Storable.toList hNodes
      pure
        ( Mesh.getRange mn
        , homeTransform
        )
    (homeRanges, homeInstanceAttrs) = List.unzip homeParts

  (_transient, homeInstances) <- Buffer.allocateCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    (Storable.fromList homeInstanceAttrs)

  -- Room

  let
    roomTransform = mconcat
      [ Transform.rotateY $ -τ * t / 128
      , Transform.translate
          250
          (10 * cos (τ * t / 33))
          0
      ]

  (_transient, roomInstances) <- Buffer.allocateCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    (Storable.singleton roomTransform)

  -- Bounding box wireframes

  bbInstances <- Buffer.createCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 $
    Storable.concat
      [ sunBBs

      -- XXX: not quite static
      , Storable.map
          (\Mesh.MaterialNode{mnNode} -> Mesh.nTransformBB mnNode <> battleTransform)
          (Storable.drop battleOpaqueNodes bNodes)

      , Storable.map
          (\Mesh.MaterialNode{mnNode} -> Mesh.nTransformBB mnNode <> homeTransform)
          (Storable.drop homeOpaqueNodes hNodes)

      , [ mconcat
            [ mTransformBB battleMeta
            , battleTransform
            ]
        , mconcat
            [ mTransformBB homeMeta
            , homeTransform
            ]
        , mconcat
            [ mTransformBB roomMeta
            , roomTransform
            ]
        ]
      ]
  _bbInstances <- ResourceT.register $ Buffer.destroy context bbInstances

  (uiDisplay, uiDrawOpaque, uiDrawBlended) <- RenderUI.prepareUI fPipelines rUI

  dear <- RenderUI.imguiDrawData

  let shadowRender = Render.spShadowPass fRenderpass
  let shadowLayout = Pipeline.pLayout $ Render.pShadowCast fPipelines
  ShadowPass.usePass shadowRender imageIndex cb do
    -- XXX: per-light/layer
    let
      viewport = ShadowPass.smRenderArea shadowRender
      scissor  = ShadowPass.smRenderArea shadowRender
    Swapchain.setDynamic cb viewport scissor
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS shadowLayout rSunDescs $
      Pipeline.bind cb (Render.pShadowCast fPipelines) do
        -- XXX: Cast shadows only from the opaque bits
        Draw.indexedPosRanges cb battleModel battleInstance [mOpaqueIndices battleMeta]

        -- TODO: well... textured models can have cut-outs that really should be considered w/discard()
        Draw.indexedPosRanges cb homeModel homeInstances $
          take homeOpaqueNodes homeRanges

        -- XXX: the rest are opaque
        Draw.indexedPosRanges cb roomModel roomInstances [mOpaqueIndices roomMeta]
        Draw.indexedPos cb thingsModel thingsInstances

  let sceneLayout = Pipeline.pLayout $ Render.pSkySun fPipelines
  ForwardPass.usePass (Render.spForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    -- XXX: Run opaque UI first to depth-test away costly scene fragments.
    when uiDisplay $
      withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS sceneLayout rSceneUiDescs do
        uiDrawOpaque cb

    -- XXX: proceed with the scene...
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS sceneLayout rSceneDescs do
      -- XXX: opaque first
      Pipeline.bind cb (Render.pLitColored fPipelines) do
        Draw.indexed cb thingsModel thingsInstances

      Pipeline.bind cb (Render.pLitMaterial fPipelines) do
        Draw.indexed cb roomModel roomInstances

        Draw.indexedRanges cb battleModel battleInstance
          [mOpaqueIndices battleMeta]

        Draw.indexedParts True cb homeModel homeInstances 0 $
          take homeOpaqueNodes homeRanges

      -- XXX: draw skybox after all opaque things
      Pipeline.bind cb (Render.pSkybox fPipelines) $
        Draw.triangle_ cb

      -- XXX: draw the blending parts now
      -- TODO: those are really should be depth-sorted wrt. camera position

      -- XXX: uses different vertex topology: edges instead of tris
      Pipeline.bind cb (Render.pWireframe fPipelines) $
        Draw.indexed cb bbWire bbInstances

      Pipeline.bind cb (Render.pLitMaterialBlend fPipelines) do
        -- XXX: skipping room, no blended nodes
        Draw.indexedRanges cb battleModel battleInstance [mBlendedIndices battleMeta]
        Draw.indexedParts True cb homeModel homeInstances homeOpaqueNodes homeRanges

    -- XXX: Draw blended UI parts
    when uiDisplay $
      withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS sceneLayout rSceneUiDescs $
        uiDrawBlended cb

    ImGui.draw dear cb

τ :: Float
τ = 2 * pi
