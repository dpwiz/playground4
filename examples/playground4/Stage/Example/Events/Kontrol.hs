{-# LANGUAGE CPP #-}

module Stage.Example.Events.Kontrol
  ( spawn
  ) where

import RIO

#ifndef KONTROL
-- Stub for a disabled event source.

import UnliftIO.Resource (MonadResource, ReleaseKey, register)

spawn :: MonadResource m => sink -> m ReleaseKey
spawn _sink = register $ pure ()

#else

import Control.Exception (AsyncException(ThreadKilled))
import RIO.State (gets)
import Sound.ControlHold.Client qualified as Client
import Sound.ControlHold.Client.NanoKontrol2 qualified as Nk
import UnliftIO.Concurrent (forkFinally, killThread)
import UnliftIO.Resource (ReleaseKey)
import UnliftIO.Resource qualified as Resource

import Engine.Events.Sink (Sink(..))
import Engine.Types (StageRIO)

import Stage.Example.Events.Types (Event)
import Stage.Example.Events.Types qualified as Event
import Stage.Example.Types (RunState(..))

spawn :: Sink Event RunState -> StageRIO RunState ReleaseKey
spawn sink = do
  kontrol <- withUnliftIO (Client.runDefault . handler sink) `forkFinally`
    \case
      Left exc ->
        case fromException exc of
          Just ThreadKilled ->
            logDebug "Controller thread killed"
          _others ->
            logError $ "Controller thread crashed: " <> displayShow exc
      Right () ->
        logWarn "Controller thread exited prematurely"
  Resource.register $ killThread kontrol

handler :: Sink Event RunState -> UnliftIO (StageRIO RunState) -> Client.Handler
handler (Sink signal) (UnliftIO ul) =
  Nk.mkHandler_ \event -> ul do
    -- logDebug $ "Kontrol event: " <> displayShow event
    case event of
      Nk.Group 0 group ->
        case group of
          Nk.Knob alpha ->
            signal $ Event.SetOrbitAzimuth alpha
          Nk.Slider alpha -> do
            signal $ Event.SetOrbitScale alpha
          _ignore ->
            pure ()

      Nk.Group 1 group ->
        case group of
          Nk.Knob alpha ->
            signal $ Event.SetOrbitAscent alpha
          Nk.Slider alpha ->
            signal $ Event.SetOrbitDistance alpha
          _ignore ->
            pure ()

      Nk.Group 2 group ->
        case group of
          Nk.Knob alpha ->
            signal $ Event.SetFogScatter alpha
          Nk.Slider alpha -> do
            signal $ Event.SetProjectionFov alpha
          _ignore ->
            pure ()

      Nk.Group 3 group ->
        case group of
          Nk.Knob alpha ->
            signal $ Event.SetEnvInclination alpha
          Nk.Slider alpha -> do
            signal $ Event.SetEnvAltitude alpha
          _ignore ->
            pure ()

      Nk.Group 4 group ->
        case group of
          Nk.Knob alpha -> do
            signal $ Event.SetTweak 2 alpha
          Nk.Slider alpha -> do
            signal $ Event.SetTweak 3 alpha
          _ignore ->
            pure ()

      Nk.Group 6 group ->
        case group of
          Nk.Knob alpha ->
            signal $ Event.SetSunAzimuth alpha
          Nk.Slider alpha ->
            signal $ Event.SetSunRadius alpha
          _ignore ->
            pure ()

      Nk.Group 7 group ->
        case group of
          Nk.Knob alpha -> do
            signal $ Event.SetSunInclination alpha
          Nk.Slider alpha -> do
            signal $ Event.SetSunDepthRange alpha
          _ignore ->
            pure ()

      _ignore ->
        pure ()

{- |
  Debouncing wrapper that triggers on press, but only if a button was released before.
-}
_onPress
  :: Bool
  -> (state -> TVar Bool)
  -> StageRIO state ()
  -> StageRIO state ()
_onPress pressed varGetter action = do
  armedVar <- gets varGetter
  if pressed then do
    armed <- readTVarIO armedVar
    when armed action
  else
    atomically $
      writeTVar armedVar True

{- |
  Debouncing wrapper that triggers on button release, but only if a button was pressed before.
-}
_onRelease
  :: Bool
  -> (state -> TVar Bool)
  -> StageRIO state ()
  -> StageRIO state ()
_onRelease pressed = _onPress (not pressed)

#endif
