module Stage.Example.Events.MouseButton
  ( clickHandler
  ) where

import RIO

import RIO.State (gets)

import Engine.Events.MouseButton (ClickHandler)
import Engine.Events.Sink (Sink(..))
import Engine.UI.Layout qualified as Layout
import Engine.Window.MouseButton (MouseButton(..), MouseButtonState(..))
import Engine.Worker qualified as Worker

import Stage.Example.Events.Types (Event)
import Stage.Example.Events.Types qualified as Event
import Stage.Example.Types (RunState(..))
import Stage.Example.UI qualified as UI

clickHandler :: ClickHandler Event RunState
clickHandler (Sink signal) cursorPos buttonEvent = do
  ui <- gets rsUI
  uiDisplay <- Worker.getOutputData (UI.display ui)
  when uiDisplay do
    Layout.whenInBoxP cursorPos (UI.debugBoxP ui) \cursorLocal ->
      case buttonEvent of
        (_mods, MouseButtonState'Pressed, MouseButton'1) -> do
          logInfo "Bugs? How unfortunate."
          signal Event.DoNothing
        _ ->
          logInfo $ "MouseButton event: " <> displayShow (cursorLocal, buttonEvent)

    Layout.whenInBoxP cursorPos (UI.davidBoxP ui) \_cursorLocal ->
      case buttonEvent of
        (_mods, MouseButtonState'Pressed, MouseButton'1) -> do
          logInfo "David says nothing, it's a statue."
          signal Event.DoNothing
        (_mods, MouseButtonState'Released, MouseButton'1) -> do
          logInfo "David still says nothing, indifferent as a stone."
          signal Event.DoNothing
        _ ->
          logDebug "David isn't interested."
