module Stage.Example.Events.Types
  ( Event(..)
  ) where

import RIO

import Vulkan.NamedType ((:::))

data Event
  = DoNothing

  | ToggleUI

  | RestartStage

  | SetOrbitAzimuth ("alpha" ::: Float)
  | SetOrbitScale ("alpha" ::: Float)
  | SetOrbitAscent ("alpha" ::: Float)
  | SetOrbitDistance ("alpha" ::: Float)

  | SetFogScatter ("alpha" ::: Float)

  | SetProjectionFov ("alpha" ::: Float)

  | SetEnvInclination ("alpha" ::: Float)
  | SetEnvAltitude ("alpha" ::: Float)

  | SetTweak ("ix" ::: Natural) ("alpha" ::: Float)

  | SetSunAzimuth ("alpha" ::: Float)
  | SetSunInclination ("alpha" ::: Float)
  | SetSunRadius ("alpha" ::: Float)
  | SetSunDepthRange ("alpha" ::: Float)
  deriving (Eq, Show, Generic)
