module Stage.Example.Events.Sink
  ( handleEvent
  ) where

import RIO

import RIO.State (gets)

import Engine.Camera qualified as Camera
import Engine.StageSwitch (trySwitchStage)
import Engine.Types (StackStage(..), NextStage(..), StageRIO)
import Engine.Worker qualified as Worker

import Stage.Example.Events.Types (Event(..))
import Stage.Example.Types (ExampleStage, RunState(..))
import Stage.Example.UI qualified as UI
import Stage.Example.World.Env qualified as Env
import Stage.Example.World.Scene qualified as Scene
import Stage.Example.World.Sun qualified as Sun

handleEvent :: (Int -> ExampleStage) -> Event -> StageRIO RunState ()
handleEvent mkSelf = \case
  DoNothing ->
    logInfo "Busy doing nothing."

  ToggleUI -> do
    ui <- gets rsUI
    Worker.pushInput (UI.display ui) not

  RestartStage -> do
    nextStage <- gets rsNextStage
    -- time <- gets rsTimeRateV
    -- world <- gets $ Sim.pWorld . rsSimP

    changed <- atomically do
      changed <- trySwitchStage nextStage . Replace $
        StackStage $ mkSelf 0

      -- when changed do
      --   Worker.updateInputSTM time $
      --     const Time.RatePaused

      pure changed

    logDebug $ "Scene self-replaced: " <> displayShow changed

  SetOrbitAzimuth alpha -> do
    viewProc <- gets rsViewP
    Worker.pushInput viewProc \input -> input
      { Camera.orbitAzimuth =
          (alpha - 0.5) * τ
      }

  SetOrbitScale alpha -> do
    viewProc <- gets rsViewP
    Worker.pushInput viewProc \input -> input
      { Camera.orbitScale =
          10 ** (4 * alpha) + 1/1024
      }

  SetOrbitAscent alpha -> do
    viewProc <- gets rsViewP
    Worker.pushInput viewProc \input -> input
      { Camera.orbitAscent =
          (alpha * 0.495 - 0.25) * τ
      }

  SetOrbitDistance alpha -> do
    viewProc <- gets rsViewP
    Worker.pushInput viewProc \input -> input
      { Camera.orbitDistance =
          alpha + 1/1024
      }

  SetFogScatter alpha -> do
    sceneVar <- gets rsSceneV
    Worker.pushInput sceneVar \input -> input
      { Scene.iFogScatter =
          2 ** (-18 * alpha)
      }

  SetProjectionFov alpha -> do
    fovProc <- gets rsProjectionP
    Worker.pushInput fovProc \input -> input
      { Camera.projectionFovRads =
          (alpha + 0.5) * τ / 4
      }

  SetEnvInclination alpha -> do
    env <- gets rsEnvP
    Worker.pushInput env \input -> input
      { Env.inclination = alpha
      }

  SetEnvAltitude alpha -> do
    env <- gets rsEnvP
    Worker.pushInput env \input -> input
      { Env.altitude = 2 ** (alpha * 14) - 0.9
      }

  SetTweak ix alpha -> do
    sceneVar <- gets rsSceneV
    Worker.updateInput sceneVar \input ->
      case ix of
        0 -> Just input
          { Scene.iTweak0 = alpha
          }
        1 -> Just input
          { Scene.iTweak1 = alpha
          }
        2 -> Just input
          { Scene.iTweak2 = alpha
          }
        3 -> Just input
          { Scene.iTweak3 = alpha
          }
        _ ->
          Nothing

  SetSunAzimuth alpha -> do
    sunProc <- gets rsSunP
    Worker.pushInput sunProc \input -> input
      { Sun.siAzimuth = alpha * τ
      }

  SetSunInclination alpha -> do
    sunProc <- gets rsSunP
    Worker.pushInput sunProc \input -> input
      { Sun.siInclination = alpha * τ / 4
      }

  SetSunRadius alpha -> do
    sunProc <- gets rsSunP
    Worker.pushInput sunProc \input -> input
      { Sun.siRadius = 2 ** (alpha * 16)
      }

  SetSunDepthRange alpha -> do
    sunProc <- gets rsSunP
    Worker.pushInput sunProc \input -> input
      { Sun.siDepthRange = 2 ** (alpha * 16)
      }

τ :: Float
τ = 2 * pi
