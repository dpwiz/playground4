module Stage.Example.UI
  ( UI(..)
  , spawn

  , Observer(..)
  , newObserver
  , observe
  ) where

import RIO hiding (display)

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as Resource
import Geomancy (vec2, vec4)
import RIO.Vector.Storable qualified as Storable
import Vulkan.NamedType ((:::))

import Engine.Camera qualified as Camera
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Render.Samplers qualified as Samplers
import Render.Unlit.Textured.Model qualified as UnlitTextured
import Render.Debug.Model qualified as Debug
import Resource.Buffer qualified as Buffer
import Resource.Combined.Textures qualified as CombinedTextures

import Global.Resource.Combined (CombinedCollection)
import Global.Resource.CubeMap (CubeMapCollection)
import Global.Resource.Font (FontCollection)
import Global.Resource.Font qualified as GameFont
import Global.Resource.Model qualified as GameModel
import Global.Resource.Texture qualified as Texture
import Stage.Example.World.Env qualified as Env

data UI = UI
  { display :: Worker.Var Bool

  , debugP    :: Worker.Merge Debug.StorableAttrs
  , debugBoxP :: Layout.BoxProcess

  , davidP    :: Worker.Merge StorableAttrs
  , davidBoxP :: Layout.BoxProcess

  , flareP    :: Worker.Merge StorableAttrs
  , statusTop :: Message.Process
  , statusBot :: Message.Process

  , fashionP :: Worker.Merge StorableAttrs
  }

type StorableAttrs =
  ( Storable.Vector UnlitTextured.TextureParams
  , Storable.Vector UnlitTextured.Transform
  )

-- TODO: extract
type Assets =
  ( "fonts"    ::: FontCollection
  , "models"   ::: GameModel.Collection
  , "textures" ::: CombinedCollection
  , "cubeMaps" ::: CubeMapCollection
  )

spawn
  :: Assets
  -> Layout.BoxProcess
  -> Camera.ViewProcess
  -> Worker.Var sceneInput
  -> Env.Process
  -> Engine.StageRIO env (Resource.ReleaseKey, UI)
spawn assets screenBoxP viewP _sceneV envP = do
  display <- Worker.newVar True

  -- Main 12-columns with 2 sidebars
  sections <- Layout.splitsRelStatic Layout.sharePadsH screenBoxP [2, 8, 2]
  sectionsKey <- Worker.registerCollection sections
  (leftP, centerP, rightP) <- case sections of
    [l, c, r] ->
      pure (l, c, r)
    _ ->
      error "assert: 3 sections"

  -- Left sidebar

  (debugBoxKey, debugBoxP) <- Worker.registered $
    Layout.fitPlaceAbs Layout.LeftTop 1.0 leftP

  (debugKey, debugP) <- Worker.registered $
    Worker.spawnMerge1
      ( \box ->
          Debug.storableAttrs1
            (Samplers.linear Samplers.indices)
            5
            [Layout.boxTransformAbs box]
      )
      debugBoxP

  (davidBoxKey, davidBoxP) <- Worker.registered $
    Layout.fitPlaceAbs Layout.LeftBottom 1.0 leftP

  (davidKey, davidP) <- Worker.registered $
    Worker.spawnMerge1
      ( \box ->
        UnlitTextured.storableAttrs1
          (Samplers.linear Samplers.indices)
          (fst . Texture.david $ CombinedTextures.textures combined)
          [Layout.boxTransformAbs box]
      )
      davidBoxP

  -- Main container
  (flareBoxKey, flareBoxP) <- Worker.registered $
    Layout.fitPlaceAbs Layout.Center 1.0 centerP

  (flareKey, flareP) <- Worker.registered $
    Worker.spawnMerge1
      ( \box ->
          UnlitTextured.storableAttrs1
              (Samplers.linear Samplers.indices)
              (fst . Texture.flare $ CombinedTextures.textures combined)
              [Layout.boxTransformAbs box]
      )
      flareBoxP

  padVar <- Worker.newVar $ vec4 8 16 8 16
  (paddedKey, padded) <- Worker.registered $
    Layout.padAbs centerP padVar

  (statusTopInputKey, statusTopKey, statusTop) <- Message.spawnFromR padded (Worker.getInput viewP) mkTopMessage
  (statusBotInputKey, statusBotKey, statusBot) <- Message.spawnFromR padded (Worker.getInput envP) mkBotMessage

  -- Right sidebar
  (fashionBoxKey, fashionBoxP) <- Worker.registered $
    Layout.fitPlaceAbs Layout.RightTop (vec2 1.0 1.5) rightP

  (fashionKey, fashionP) <- Worker.registered $
    Worker.spawnMerge1
      ( \box ->
          let
            UnlitTextured.InstanceAttrs{..} = UnlitTextured.instanceAttrs
              (Samplers.linear Samplers.indices)
              (fst . Texture.fashion $ CombinedTextures.textures combined)
              [Layout.boxTransformAbs box]
          in
            ( Storable.singleton textureParams
            , Storable.singleton transformMat4
            )
      )
      fashionBoxP

  -- Cleanup

  key <- Resource.register $ traverse_ Resource.release
    [ sectionsKey

    , debugBoxKey, debugKey

    , davidBoxKey, davidKey

    , paddedKey
    , flareBoxKey, flareKey
    , statusTopInputKey, statusTopKey
    , statusBotInputKey, statusBotKey

    , fashionBoxKey, fashionKey
    ]

  pure (key, UI{..})
  where
    mkTopMessage input@Camera.ViewOrbitalInput{} =
      small (vec4 0.5 1 0.25 1) Layout.RightTop $
        fromString (show input)

    mkBotMessage input@Env.Input{} =
      small (vec4 0.5 1 0.25 1) Layout.LeftBottom $
        fromString (show input)

    small color origin text = Message.Input
      { inputText         = text
      , inputOrigin       = origin
      , inputSize         = 16
      , inputColor        = color
      , inputFont         = GameFont.small rsFonts
      , inputFontId       = fst . GameFont.small $ CombinedTextures.fonts combined
      , inputOutline      = vec4 0 0 0 0.5
      , inputOutlineWidth = 4/16
      , inputSmoothing    = 1/16
      }

    (rsFonts, _rsModels, combined, _rsCubes) = assets

data Observer = Observer
  { debug :: Worker.ObserverIO (Debug.InstanceBuffers 'Buffer.Coherent 'Buffer.Coherent)

  , david :: Worker.ObserverIO (UnlitTextured.InstanceBuffers 'Buffer.Coherent 'Buffer.Coherent)

  , flare    :: Worker.ObserverIO (UnlitTextured.InstanceBuffers 'Buffer.Coherent 'Buffer.Coherent)
  , messages :: [Message.Observer]

  , fashion :: Worker.ObserverIO (UnlitTextured.InstanceBuffers 'Buffer.Coherent 'Buffer.Coherent)
  }

newObserver :: UI -> ResourceT (Engine.StageRIO st) Observer
newObserver UI{..} = do
  -- XXX: Generic?
  messages <- traverse (const $ Message.newObserver 256)
    [ statusTop
    , statusBot
    ]

  debug <- Debug.allocateInstancesCoherent_ 1 >>= Worker.newObserverIO

  let
    newBufferObserver1 =
      UnlitTextured.allocateInstancesCoherent_ 1 >>= Worker.newObserverIO

  david <- newBufferObserver1
  flare <- newBufferObserver1
  fashion <- newBufferObserver1

  pure Observer{..}

observe
  :: ( HasVulkan env
     )
  => UI
  -> Observer -> RIO env ()
observe UI{..} Observer{..} = do
  context <- ask

  Worker.observeIO_ debugP debug $
    Debug.updateCoherentResize_ context

  Worker.observeIO_ davidP david $
    UnlitTextured.updateCoherentResize_ context

  Worker.observeIO_ flareP flare $
    UnlitTextured.updateCoherentResize_ context
  traverse_ (uncurry Message.observe) $
    zip [statusTop, statusBot] messages

  Worker.observeIO_ fashionP fashion $
    UnlitTextured.updateCoherentResize_ context
