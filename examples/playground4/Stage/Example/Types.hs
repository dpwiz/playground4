module Stage.Example.Types where

import RIO

import Data.Tagged (Tagged)
import Geomancy (Transform, Vec2)
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.StageSwitch (StageSwitchVar)
import Engine.Worker (ObserverIO)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Sun (Sun)
import Resource.Buffer qualified as Buffer

import Global.Render qualified as Rendering
import Global.Resource.Combined (CombinedCollection)
import Global.Resource.CubeMap (CubeMapCollection)
import Global.Resource.Font (FontCollection)
import Global.Resource.Material (MaterialCollection)
import Global.Resource.Model qualified as GameModel
import Stage.Example.UI (UI)
import Stage.Example.UI qualified as UI
import Stage.Example.World.Env (Env)
import Stage.Example.World.Env qualified as Env
import Stage.Example.World.Scene qualified as Scene
import Stage.Example.World.Sun qualified as Sun

type ExampleStage = Rendering.Stage Resources RunState

type Frame = Rendering.Frame Resources

data Resources = Resources
  { rWorldThings :: ObserverIO (Buffer.Allocated 'Buffer.Coherent Transform)

  , rSunDescs :: Tagged '[Sun] (Vector Vk.DescriptorSet)
  , rSunData  :: Buffer.Allocated 'Buffer.Coherent Sun
  , rSunOut   :: ObserverIO (Storable.Vector Transform)

  , rSceneDescs :: Tagged '[Scene, Env] (Vector Vk.DescriptorSet)
  , rSceneData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , rScene      :: ObserverIO Scene

  , rSceneUiDescs :: Tagged '[Scene, Env] (Vector Vk.DescriptorSet)
  , rSceneUiData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , rSceneUi      :: ObserverIO Scene

  , rEnv        :: Env.Observer

  , rUI :: UI.Observer
  }

data RunState = RunState
  { rsNextStage   :: StageSwitchVar
  , rsProjectionP :: Camera.ProjectionProcess
  , rsViewP       :: Camera.ViewProcess

  , rsCursorPos :: Worker.Var Vec2
  , rsCursorP   :: Worker.Merge Vec2

  , rsSceneP :: Scene.Process
  , rsSceneV :: Scene.InputVar

  , rsSceneUiP :: Scene.Process

  , rsEnvP :: Env.Process
  , rsSunP :: Sun.Process
  -- , rsMoonP       :: Sun.Process
  -- , rsLightsP     :: Sun.LightsProcess

  , rsFonts     :: FontCollection
  , rsModels    :: GameModel.Collection
  , rsTextures  :: CombinedCollection
  , rsCubeMaps  :: CubeMapCollection
  , rsMaterials :: MaterialCollection

  , rsUI :: UI
  }
