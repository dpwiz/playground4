{-# LANGUAGE OverloadedLists #-}

module Stage.Example.Render.UI where

import RIO

import Control.Monad.Trans.Resource qualified as ResourceT
import DearImGui qualified
import Geomancy (vec4, withVec2, withVec4)
import Geomancy.Transform qualified as Transform
import GHC.Float (double2Float)
import RIO.State (gets)
import Vulkan.Core10 qualified as Vk

import Engine.Types (StageFrameRIO)
import Engine.Vulkan.DescSets (Bound, Compatible)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Worker qualified as Worker
import Render.Draw qualified as Draw
import Render.ImGui qualified as ImGui
import Render.DescSets.Set0 (Scene)
import Resource.Buffer qualified as Buffer

import Global.Render qualified as Render
import Global.Resource.Model qualified as GameModel
import Stage.Example.Types (Resources(..), RunState(..))
import Stage.Example.UI qualified as UI
import Stage.Example.World.Env qualified as Env
import Stage.Example.World.Sun (SunInput(..))

type DrawM = StageFrameRIO Render.ScreenPasses Render.Pipelines Resources RunState

type DrawData dsl m =
  ( Bool
  , Vk.CommandBuffer -> Bound dsl Void Void m ()
  , Vk.CommandBuffer -> Bound dsl Void Void m ()
  )

prepareUI
  :: ( Compatible '[Scene] dsl
     )
  => Render.Pipelines
  -> UI.Observer
  -> DrawM (DrawData dsl DrawM)
prepareUI fPipelines rUI = do
  ui <- mapRIO fst $ gets rsUI
  uiDisplay <- Worker.getOutputData (UI.display ui)

  if not uiDisplay then
    pure
      ( uiDisplay
      , const $ pure ()
      , const $ pure ()
      )
  else do
    context <- ask

    debug <- Worker.readObservedIO (UI.debug rUI)
    david <- Worker.readObservedIO (UI.david rUI)
    _flare <- Worker.readObservedIO (UI.flare rUI)
    uiMessages <- traverse Worker.readObservedIO (UI.messages rUI)
    fashion <- Worker.readObservedIO (UI.fashion rUI)

    quadUV <- mapRIO fst . gets $ GameModel.quadUV . rsModels
    _bbWire <- mapRIO fst . gets $ GameModel.bbWire . rsModels

    cur <- mapRIO fst (gets rsCursorP) >>= Worker.getOutputData
    t <- fmap double2Float getMonotonicTime
    bbInstancesUi <- Buffer.createCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 $
      withVec2 cur \x y ->
        [  mconcat
            [ Transform.scale3 (sin t * 32 + 64) (sin t * 32 + 64) 0.25
            , Transform.rotateZ (t / 2)
            , Transform.translate x y 0.5
            ]
        , mconcat
            [ Transform.scale3 (cos t * 24 + 48) (cos t * 24 + 48) 0.25
            , Transform.rotateZ (-t / 3)
            , Transform.translate x y 0.5
            ]
        ]
    _bbInstancesUi <- ResourceT.register $ Buffer.destroy context bbInstancesUi

    let
      uiDrawOpaque cb = do -- when uiDisplay do
        -- Vk.cmdSetScissor cb 0 [Layout.boxRectAbs leftBox]
        Pipeline.bind cb (Render.pUnlitTextured fPipelines) do
          Draw.indexed cb quadUV david
          Draw.indexed cb quadUV fashion

      uiDrawBlended cb = do
        Pipeline.bind cb (Render.pDebug fPipelines) $
          Draw.indexed cb quadUV debug

        Pipeline.bind cb (Render.pEvanwSdf fPipelines) $
          traverse_ (Draw.quads cb) uiMessages

        -- Pipeline.bind cb (Render.pWireframeNoDepth fPipelines) $
        --   Draw.indexed cb bbWire bbInstancesUi

    pure
      ( uiDisplay
      , uiDrawOpaque
      , uiDrawBlended
      )

imguiDrawData :: DrawM DearImGui.DrawData
imguiDrawData = fmap snd $ ImGui.mkDrawData do
  sunP <- mapRIO fst $ gets rsSunP
  envP <- mapRIO fst $ gets rsEnvP
  ui <- mapRIO fst $ gets rsUI
  -- let
  --   minSize = pure @IO (DearImGui.ImVec2 200 200)
  --   maxSize = pure @IO (DearImGui.ImVec2 500 500)
  -- DearImGui.setNextWindowSizeConstraints minSize maxSize
  bracket (DearImGui.begin "Success") (const DearImGui.end) \open -> do
    when open do
      DearImGui.newLine
      DearImGui.text "Warning: Avoiding success may incur some costs."
      DearImGui.newLine
      void $! DearImGui.checkbox "Avoid" $
        Worker.stateVar (UI.display ui)

  bracket (DearImGui.begin "Environment") (const DearImGui.end) \open ->
    when open do
      void $! DearImGui.sliderFloat "Orbit Azimuth, turn"
        (Worker.stateVarMap
          Env.azimuth
          (\new env -> env { Env.azimuth = new })
          envP
        )
        0
        1

      void $! DearImGui.sliderFloat "Orbit Inclination, turn"
        (Worker.stateVarMap
          Env.inclination
          (\new env -> env { Env.inclination = new })
          envP
        )
        0
        1

      void $! DearImGui.sliderFloat "Orbit Altitude, km"
        (Worker.stateVarMap
          Env.altitude
          (\new env -> env { Env.altitude = new })
          envP
        )
        0.0125
        (2**20)

      DearImGui.separator

      -- XXX: derived from planet-sun position
      void $! DearImGui.sliderFloat "Sun azimuth, turn"
        (Worker.stateVarMap
          ((/ τ) . siAzimuth)
          (\new si -> si { siAzimuth = new * τ })
          sunP
        )
        0
        1

      void $! DearImGui.sliderFloat "Sun inclination, turn"
        (Worker.stateVarMap
          ((/ τ) . siInclination)
          (\new si -> si { siInclination = new * τ })
          sunP
        )
        0
        1

      void $! DearImGui.colorButton
        "Local color"
        (Worker.stateVarMap
          (\SunInput{siColor} ->
              withVec4 siColor DearImGui.ImVec4
          )
          (\(DearImGui.ImVec4 r g b a) si -> si
              { siColor = vec4 r g b a
              }
          )
          sunP
        )

τ :: Float
τ = 2 * pi
