module Stage.Example.Setup
  ( stackStage
  ) where

import RIO

import Geomancy (vec2, vec3, vec4, pattern WithVec2)
import RIO.App (appEnv)
import RIO.State (gets)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Events.CursorPos qualified as CursorPos
import Engine.Events.MouseButton qualified as MouseButton
import Engine.StageSwitch (getNextStage, newStageSwitchVar)
import Engine.Types (StackStage(..), Stage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.Worker qualified as Worker
import Render.ImGui qualified as ImGui

import Global.Render qualified as Render
import Global.Resource.Combined (CombinedCollection)
import Global.Resource.CubeMap (CubeMapCollection)
import Global.Resource.Font (FontCollection)
import Global.Resource.Material (MaterialCollection)
import Global.Resource.Model qualified as GameModel
import Stage.Example.Events.Key qualified as Key
import Stage.Example.Events.Kontrol qualified as Kontrol
import Stage.Example.Events.MouseButton qualified as MouseButton
import Stage.Example.Events.Sink (handleEvent)
import Stage.Example.Render qualified as Render
import Stage.Example.Types (ExampleStage, RunState(..))
import Stage.Example.UI qualified as UI
import Stage.Example.World.Env qualified as Env
import Stage.Example.World.Scene qualified as Scene
import Stage.Example.World.Sun (SunInput(..), mkSun)

stackStage
  :: FontCollection
  -> GameModel.Collection
  -> CombinedCollection
  -> CubeMapCollection
  -> MaterialCollection
  -> Int
  -> StackStage
stackStage fonts models textures cubes materials =
  StackStage . exampleStage fonts models textures cubes materials

exampleStage
  :: FontCollection
  -> GameModel.Collection
  -> CombinedCollection
  -> CubeMapCollection
  -> MaterialCollection
  -> Int
  -> ExampleStage
exampleStage fonts models textures cubes materials moonCount = Stage
  { sTitle = "Example [" <> textDisplay moonCount <> "]"

  , sAllocateP  = Render.allocatePipelines True -- TODO: move to class
  , sInitialRS  = initialRS fonts models textures cubes materials moonCount
  , sInitialRR  = Render.initialData
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands
  , sGetNextStage   = getNextStage rsNextStage

  , sAfterLoop = afterLoop
  }
  where
    mkSelf = exampleStage fonts models textures cubes materials

    beforeLoop = do
      cursorWindow <- gets rsCursorPos
      cursorCentered <- gets rsCursorP

      (key, _sink) <- Events.spawn
        (handleEvent mkSelf)
        [ CursorPos.callback cursorWindow
        , MouseButton.callback cursorCentered MouseButton.clickHandler
        , Key.callback
        , Kontrol.spawn
        ]

      ImGui.beforeLoop True

      pure key

    afterLoop key = do
      ImGui.afterLoop
      Resource.release key

initialRS
  :: FontCollection
  -> GameModel.Collection
  -> CombinedCollection
  -> CubeMapCollection
  -> MaterialCollection
  -> Int
  -> StageRIO env (Resource.ReleaseKey, RunState)
initialRS rsFonts rsModels rsTextures rsCubeMaps rsMaterials moonCount = do
  logInfo $ "Setting up state for " <> display moonCount <> " moons"
  debugMessage <- toIO . logInfo $ "Releasing state for " <> display moonCount <> " moons"
  debugKey <- Resource.register debugMessage

  rsNextStage <- newStageSwitchVar

  rsProjectionP <- asks $ Engine.ghScreenP . appEnv
  rsCursorPos <- Worker.newVar 0
  (cursorKey, rsCursorP) <- Worker.registered $
    Worker.spawnMerge2
      (\Camera.ProjectionInput{projectionScreen} (WithVec2 windowX windowY) ->
        let
          Vk.Extent2D{width, height} = projectionScreen
        in
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      (Worker.getInput rsProjectionP)
      rsCursorPos

  let cameraTarget = vec3 0 0 0
  (viewKey, rsViewP) <- Worker.registered $
    Worker.spawnCell Camera.mkViewOrbital_ Camera.ViewOrbitalInput
      { orbitAzimuth  = τ/8
      , orbitAscent   = τ/12
      , orbitDistance = 0.5
      , orbitScale    = 1000
      , orbitTarget   = cameraTarget
      }

  (sunKey, rsSunP) <- Worker.registered $
    Worker.spawnCell mkSun SunInput
      { siInclination = τ/8
      , siAzimuth     = -τ/8

        -- TODO: get from direction
      , siColor = vec4 1 1 1 1 -- vec4 1 0.75 0.66 4.0

        -- TODO: get from view frustum and scene BB
      , siRadius     = 2 ** (0.6692913 * 16)
      , siDepthRange = 2 ** (0.7637795 * 16)
      , siSize       = 768
      , siTarget     = 0

      , siShadowIx   = 0
      }

  rsSceneV <- Worker.newVar Scene.initialInput
  (sceneKey, rsSceneP) <- Worker.registered $
    Worker.spawnMerge3 Scene.mkScene rsProjectionP rsViewP rsSceneV
  (sceneUiKey, rsSceneUiP) <- Worker.registered $
    Worker.spawnMerge3 Scene.mkSceneUi rsProjectionP rsViewP rsSceneV

  (envKey, rsEnvP) <- Worker.registered $
    Env.spawn Env.Input
      { azimuth     = 70/360
      , inclination = -0/360
      , altitude    = 0.5
      }

  (screenKey, rsScreenBoxP) <- Worker.registered Layout.trackScreen

  let assets = (rsFonts, rsModels, rsTextures, rsCubeMaps)
  (uiKey, rsUI) <- UI.spawn assets rsScreenBoxP rsViewP rsSceneV rsEnvP

  releaseKeys <- Resource.register $ traverse_ Resource.release
    [ cursorKey
    , debugKey
    , viewKey
    , sceneKey, sceneUiKey
    , sunKey
    , envKey
    , screenKey
    , uiKey
    ]

  pure (releaseKeys, RunState{..})

τ :: Float
τ = 2 * pi
