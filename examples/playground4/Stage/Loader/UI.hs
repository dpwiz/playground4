module Stage.Loader.UI
  ( UI(..)
  , spawn

  , Observer(..)
  , newObserver
  , observe
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as Resource
import Geomancy (vec4)
import Geomancy.Transform qualified as Transform
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (HasVulkan)
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Geometry.Flat qualified as Flat
import Render.Samplers qualified as Samplers
import Render.Unlit.Textured.Model qualified as UnlitTextured
import Resource.Buffer qualified as Buffer
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.Model qualified as Model

import Global.Resource.Combined (CombinedCollection)
import Global.Resource.Font (FontCollection)
import Global.Resource.Font qualified as GameFont
import Global.Resource.Texture qualified as Texture

data UI = UI
  { titleP    :: Message.Process
  , subtitleP :: Message.Process

  , progressInput :: Worker.Var Message.Input
  , progressP     :: Message.Process

  , backgroundP :: Worker.Merge StorableAttrs
  , spinnerP    :: Worker.Merge StorableAttrs

  , quadUV :: UnlitTextured.Model 'Buffer.Staged
  }

type StorableAttrs =
  ( Storable.Vector UnlitTextured.TextureParams
  , Storable.Vector UnlitTextured.Transform
  )

spawn
  :: FontCollection
  -> CombinedCollection
  -> Queues Vk.CommandPool
  -> Layout.BoxProcess
  -> Engine.StageRIO env (Resource.ReleaseKey, UI)
spawn fonts combined pools screenBoxP = do
  paddingVar <- Worker.newVar 16
  (screenPaddedKey, screenPaddedP) <- Worker.registered $
    Layout.padAbs screenBoxP paddingVar

  -- Messages

  sections <- Layout.splitsRelStatic Layout.sharePadsV screenPaddedP [2, 1, 1]
  sectionsKey <- Worker.registerCollection sections

  (titleBoxP, subtitleBoxP, progressBoxP) <- case sections of
    [titleBoxP, subtitleBoxP, progressBoxP] ->
      pure (titleBoxP, subtitleBoxP, progressBoxP)
    _ ->
      error "assert: shares in Traversable preserve structure"

  (titleKey, titleP) <- Worker.registered $
    Worker.newVar title >>= Message.spawn titleBoxP
  (subtitleKey, subtitleP) <- Worker.registered $
    Worker.newVar subtitle >>= Message.spawn subtitleBoxP

  progressInput <- Worker.newVar progress
  (progressKey, progressP) <- Worker.registered $
    Message.spawn progressBoxP progressInput

  -- Splash

  (backgroundBoxKey, backgroundBoxP) <- Worker.registered $
    Layout.fitPlaceAbs Layout.Center 1.0 screenBoxP

  (backgroundKey, backgroundP) <- Worker.registered $
    Worker.spawnMerge1
      ( \box ->
          UnlitTextured.storableAttrs1
              (Samplers.linear Samplers.indices)
              (fst . Texture.vulkan_planet $ CombinedTextures.textures combined)
              [Layout.boxTransformAbs box]
      )
      backgroundBoxP

  -- Spinner

  (spinnerTransformTimeKey, spinnerTransformTimeP) <- Worker.registered $
    Worker.spawnTimed_ 100 True do
      t <- getMonotonicTime
      pure $ Transform.rotateZ (realToFrac t)

  (spinnerTransformBoxKey, spinnerTransformBoxP) <- Worker.registered $
    Worker.spawnMerge1
      (Layout.boxTransformAbs . snd . Layout.boxFitScale 64)
      subtitleBoxP

  (spinnerKey, spinnerP) <- Worker.registered $
    Worker.spawnMerge2
      ( \place turn ->
          UnlitTextured.storableAttrs1
              (Samplers.linear Samplers.indices)
              (fst . Texture.cd $ CombinedTextures.textures combined)
              [turn, place]
      )
      spinnerTransformBoxP
      spinnerTransformTimeP

  -- Models

  context <- ask
  quadUV <- Model.createStagedL context pools (Flat.toVertices Flat.texturedQuad) Nothing
  quadUVKey <- Resource.register $ Model.destroyIndexed context quadUV

  -- Cleanup

  key <- Resource.register $ traverse_ Resource.release
    [ screenPaddedKey
    , sectionsKey
    , titleKey, subtitleKey, progressKey
    , backgroundBoxKey, backgroundKey
    , spinnerTransformBoxKey, spinnerTransformTimeKey, spinnerKey
    , quadUVKey
    ]

  pure (key, UI{..})
  where
    title = Message.Input
      { inputText         = "Playground 4"
      , inputOrigin       = Layout.CenterBottom
      , inputSize         = 64
      , inputColor        = vec4 1 0.5 0.25 1
      , inputFont         = GameFont.large fonts
      , inputFontId       = fst . GameFont.large $ CombinedTextures.fonts combined
      , inputOutline      = vec4 0 0 0 1
      , inputOutlineWidth = 4/16
      , inputSmoothing    = 1/16
      }

    subtitle = Message.Input
      { inputText         = "Loading..."
      , inputOrigin       = Layout.CenterTop
      , inputSize         = 32
      , inputColor        = vec4 0.5 1 0.25 1
      , inputFont         = GameFont.small fonts
      , inputFontId       = fst . GameFont.small $ CombinedTextures.fonts combined
      , inputOutline      = vec4 0 0 0 1
      , inputOutlineWidth = 4/16
      , inputSmoothing    = 1/16
      }

    progress = Message.Input
      { inputText         = ""
      , inputOrigin       = Layout.CenterBottom
      , inputSize         = 16
      , inputColor        = vec4 0.25 0.5 1 1
      , inputFont         = GameFont.small fonts
      , inputFontId       = fst . GameFont.small $ CombinedTextures.fonts combined
      , inputOutline      = 0
      , inputOutlineWidth = 0
      , inputSmoothing    = 1/16
      }

data Observer = Observer
  { messages   :: [Message.Observer]
  , background :: Worker.ObserverIO (UnlitTextured.InstanceBuffers 'Buffer.Coherent 'Buffer.Coherent)
  , spinner    :: Worker.ObserverIO (UnlitTextured.InstanceBuffers 'Buffer.Coherent 'Buffer.Coherent)
  }

newObserver :: UI -> ResourceT (Engine.StageRIO st) Observer
newObserver UI{..} = do
  -- XXX: Generic?
  messages <- traverse (const $ Message.newObserver 256)
    [ titleP
    , subtitleP
    , progressP
    ]

  let
    newBufferObserver1 =
      UnlitTextured.allocateInstancesCoherent_ 1 >>= Worker.newObserverIO

  background <- newBufferObserver1
  spinner <- newBufferObserver1

  pure Observer{..}

observe
  :: ( HasVulkan env
     )
  => UI
  -> Observer -> RIO env ()
observe UI{..} Observer{..} = do
  traverse_ (uncurry Message.observe) $
    zip [titleP, subtitleP, progressP] messages

  context <- ask

  Worker.observeIO_ backgroundP background $
    UnlitTextured.updateCoherentResize_ context

  Worker.observeIO_ spinnerP spinner $
    UnlitTextured.updateCoherentResize_ context
