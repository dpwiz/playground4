module Stage.Loader.Types
  ( LoaderStage
  , Frame
  , Resources(..)
  , RunState(..)
  ) where

import RIO

import Data.Tagged (Tagged)
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.StageSwitch (StageSwitchVar)
import Engine.Worker (ObserverIO)
import Render.DescSets.Set0 (Scene)
import Render.Unlit.Textured.Model qualified as TexturedUnlit
import Resource.Buffer qualified as Buffer

import Global.Render qualified as Rendering
import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap (CubeMapCollection)
import Global.Resource.Font (FontCollection)
import Stage.Example.World.Scene qualified as Scene -- FIXME
import Stage.Loader.UI (UI)
import Stage.Loader.UI qualified as UI

type LoaderStage = Rendering.Stage Resources RunState

type Frame = Rendering.Frame Resources

data Resources = Resources
  { rSceneUiDescs :: Tagged '[Scene] (Vector Vk.DescriptorSet)
  , rSceneUiData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , rSceneUi      :: ObserverIO Scene

  , rTextureIds :: Combined.Ids -- ^ XXX: can be observed

  , rUI :: UI.Observer
  }

data RunState = RunState
  { rsNextStage   :: StageSwitchVar

  , rsProjectionP :: Camera.ProjectionProcess
  , rsViewP       :: Camera.ViewProcess
  , rsSceneUiP    :: Scene.Process

  , rsQuadUV     :: TexturedUnlit.Model 'Buffer.Staged

  , rsFonts      :: FontCollection

  , rsTextureIds :: Combined.Ids
  , rsTextures   :: Combined.Textures
  , rsCubeMaps   :: CubeMapCollection

  , rsUI :: UI
  }
