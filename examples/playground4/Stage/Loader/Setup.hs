module Stage.Loader.Setup
  ( stackStage
  ) where

import RIO

import Data.List qualified as List
import Geomancy (vec2, vec3)
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import RIO.App (appEnv)
import RIO.FilePath (takeBaseName)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk
import Vulkan.Utils.Debug qualified as Debug

import Engine.Camera qualified as Camera
import Engine.StageSwitch (getNextStage, newStageSwitchVar, trySwitchStage)
import Engine.Types (StackStage(..), Stage(..), StageRIO, StageSetupRIO)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (Queues, getDevice)
import Engine.Worker qualified as Worker
import Geometry.Cube qualified as Cube
import Geometry.Flat qualified as Flat
import Geometry.Icosphere (icosphere)
import Render.Lit.Colored.Model qualified as LitColored
import Render.Lit.Material.Collect (SceneModel(..))
import Render.Lit.Material.Collect qualified as Collect
import Resource.Collection qualified as Collection
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.CommandBuffer (allocatePools)
import Resource.Font qualified as Font
import Resource.Image qualified as Image
import Resource.Mesh.Codec qualified as Mesh
import Resource.Model qualified as Model
import Resource.Texture qualified as Texture
import Resource.Texture.Ktx1 qualified as Ktx1

import Global.Render qualified as Render
import Global.Resource.CubeMap qualified as CubeMaps
import Global.Resource.Font qualified as GameFont
import Global.Resource.Model qualified as GameModel
import Global.Resource.Texture qualified as Textures
import Global.Resource.Texture.SkyHome qualified as SkyHome
import Stage.Example.Setup qualified as Example
import Stage.Example.World.Scene qualified as Scene
import Stage.Loader.Render qualified as Render
import Stage.Loader.Types (LoaderStage, RunState(..))
import Stage.Loader.UI qualified as UI

stackStage :: StackStage
stackStage = StackStage loaderStage

loaderStage :: LoaderStage
loaderStage = Stage
  { sTitle = "Loader"

  , sInitialRS  = initialRS
  , sAllocateP  = Render.allocatePipelines False -- TODO: move to class
  , sInitialRR  = Render.initialData
  , sBeforeLoop = pure ()

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sGetNextStage = getNextStage rsNextStage
  , sAfterLoop    = pure
  }

initialRS :: StageSetupRIO (Resource.ReleaseKey, RunState)
initialRS = do
  rsProjectionP <- asks $ Engine.ghScreenP . appEnv

  let cameraTarget = vec3 0 0 1
  (viewKey, rsViewP) <- Worker.registered $
    Worker.spawnCell (Camera.mkViewOrbital cameraTarget) Camera.ViewOrbitalInput
      -- XXX: ordinary perspective projection from (0,0,0) to (0,0,1)
      { orbitAzimuth  = 0
      , orbitAscent   = 0
      , orbitTarget   = 0
      , orbitDistance = 0
      , orbitScale    = 1
      }

  sceneInput <- Worker.newVar Scene.initialInput
  (sceneUiKey, rsSceneUiP) <- Worker.registered $
    Worker.spawnMerge3 Scene.mkSceneUi rsProjectionP rsViewP sceneInput

  rsNextStage <- newStageSwitchVar

  context <- ask
  (bqKey, pools) <- allocatePools context

  logInfo "Loading fonts"
  (_fontKey, fonts) <- Font.allocateCollection pools GameFont.configs

  rsQuadUV <- Model.createStagedL context pools (Flat.toVertices Flat.texturedQuad) Nothing
  _quadKey <- Resource.register $ Model.destroyIndexed context rsQuadUV

  (_textureKey, textures) <- Texture.allocateCollectionWith (Ktx1.createTexture pools) Textures.paths

  let
    rsFonts = fmap Font.container fonts
    combinedTextures = Collection.enumerate CombinedTextures.Collection
      { textures = textures
      , fonts    = fmap Font.texture fonts
      }
    rsTextureIds = fmap fst combinedTextures
    rsTextures   = fmap snd combinedTextures
  let names = List.zip3 (toList rsTextureIds) (toList Textures.paths) (toList rsTextures)
  for_ names \(ix, path, tx) -> do
    logDebug $ displayShow (ix, path)
    let image = Image.aiImage $ Texture.tAllocatedImage tx
    Debug.nameObject (getDevice context) image . fromString $ show ix <> ":" <> takeBaseName path

  -- XXX: move to loader thread
  -- XXX: requires its own pools?..
  (_cubeKey, cubeMaps) <- Texture.allocateCollectionWith (Ktx1.createTexture pools) CubeMaps.paths
  let rsCubeMaps = Collection.enumerate cubeMaps
  logDebug . displayShow $ fmap fst rsCubeMaps

  (screenKey, screenBoxP) <- Worker.registered Layout.trackScreen

  (uiKey, rsUI) <- UI.spawn rsFonts combinedTextures pools screenBoxP
  let
    updateProgress text =
      Worker.pushInput (UI.progressInput rsUI) \msg -> msg
        { Message.inputText = text
        }

  releaseKeys <- Resource.register $
    traverse_ Resource.release
      [ viewKey
      , sceneUiKey
      , bqKey
      , screenKey
      , uiKey
      ]

  loadingModels <- async $ loadModels pools updateProgress
  loadingCubes <- async $ pure rsCubeMaps

  {- XXX:
    Loaded resources are leaking into global scope deliberately.

    This allows to use @Replace@ 'StageAction' safely, without pulling
    resources from under replacement stages.
  -}
  switcher <- async do
    threadDelay 1e6
    (loadedModels, loadedCubes) <- waitBoth loadingModels loadingCubes
    logInfo "Loader signalled a stage change"

    updateProgress "Collecting materials..."

    let
      collectedMaterials = Collect.sceneMaterials
        loadedModels
        (CombinedTextures.textures rsTextureIds)
        [ SceneModel
            { smLabel            = "Viking room"
            , smGetModel         = GameModel.vikingRoom
            , smGetTextureOffset = Textures.viking_room
            }
        , SceneModel
            { smLabel            = "Sky home"
            , smGetModel         = GameModel.skyHome
            , smGetTextureOffset = SkyHome.bark_emissive . Textures.sky_home
            }
        , SceneModel
            { smLabel            = "Battle"
            , smGetModel         = GameModel.battle
            , smGetTextureOffset = const 0
            }
        ]

    updateProgress "Done!"
    void $! atomically . trySwitchStage rsNextStage . Engine.Replace $
      Example.stackStage rsFonts loadedModels combinedTextures loadedCubes collectedMaterials 0

  -- XXX: propagate exceptions from loader threads
  link switcher

  pure (releaseKeys, RunState{..})

loadModels
  :: Queues Vk.CommandPool
  -> (Text -> StageRIO env ())
  -> StageRIO env GameModel.Collection
loadModels pools updateProgress = do
  context <- ask

  updateProgress "Loading models"

  bbWire <- Model.createStagedL context pools Cube.bbWireColored Nothing
  _bbWireKey <- Resource.register $ Model.destroyIndexed context bbWire

  quadUV <- Model.createStagedL context pools (Flat.toVertices Flat.texturedQuad) Nothing
  _quadKey <- Resource.register $ Model.destroyIndexed context quadUV

  let
    v05 = vec3 0.5 0.5 0.5
    sphereAttrs pos = LitColored.VertexAttrs
      { vaBaseColor         = Vec4.fromVec3 (pos * v05 + v05) 1
      , vaEmissiveColor     = 0
      , vaMetallicRoughness = vec2 0.5 0.5
      , vaNormal            = Vec3.Packed pos
      }

  icosphere1 <- Model.createStagedL context pools (icosphere sphereAttrs 1) Nothing
  _icosphere1Key <- Resource.register $ Model.destroyIndexed context icosphere1

  icosphere4 <- Model.createStagedL context pools (icosphere sphereAttrs 4) Nothing
  _icosphere4Key <- Resource.register $ Model.destroyIndexed context icosphere4

  updateProgress "resources/meshes/viking_room.p4m"
  (_vrKey, vikingRoom) <- Mesh.loadIndexed pools "resources/meshes/viking_room.p4m"

  updateProgress "resources/meshes/sky_home.p4m"
  (_shKey, skyHome) <- Mesh.loadIndexed pools "resources/meshes/sky_home.p4m"

  updateProgress "resources/meshes/battle.p4m"
  (_bKey, battle) <- Mesh.loadIndexed pools "resources/meshes/battle.p4m"

  updateProgress "Done!"

  pure GameModel.Collection{..}
