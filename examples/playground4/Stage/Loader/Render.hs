{-# LANGUAGE OverloadedLists #-}

module Stage.Loader.Render where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as ResourceT
import RIO.State (gets)
import RIO.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

import Engine.Types (StageRIO, StageFrameRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (createSet0Ds)
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
import Resource.Buffer qualified as Buffer

import Global.Render qualified as Render
import Stage.Loader.Types (Resources(..), RunState(..))
import Stage.Loader.UI qualified as UI

initialData
  :: Queues Vk.CommandPool
  -> Render.RenderPasses
  -> Render.Pipelines
  -> ResourceT (StageRIO RunState) Resources
initialData _cmdPools _renderPasses pipelines = do
  combinedTextures <- gets rsTextures
  rTextureIds <- gets rsTextureIds -- XXX: can be observed instead

  combinedCubes <- gets $ fmap snd . rsCubeMaps -- XXX: should be observed if used in loader

  (rSceneUiDescs, rSceneUiData, rSceneUi) <- createSet0Ds
    (Render.getSceneLayout pipelines)
    combinedTextures
    combinedCubes
    Nothing
    mempty -- XXX: no shadows on loader
    Nothing

  rUI <- gets rsUI >>= UI.newObserver

  logReleaseThings <- toIO $ logDebug "Releasing Loader recycled resources"
  void $! ResourceT.register do
    logReleaseThings

  pure Resources{..}

updateBuffers
  :: RunState
  -> Resources
  -> StageFrameRIO Render.ScreenPasses Render.Pipelines Resources RunState ()
updateBuffers RunState{..} Resources{..} = do
  Worker.observeIO_ rsSceneUiP rSceneUi \_old new -> do
    _same <- Buffer.updateCoherent (Vector.singleton new) rSceneUiData
    pure new

  UI.observe rsUI rUI

recordCommands
  :: Vk.CommandBuffer
  -> Resources
  -> "imageIndex" ::: Word32
  -> StageFrameRIO Render.ScreenPasses Render.Pipelines Resources RunState ()
recordCommands cb Resources{..} imageIndex = do
  Engine.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

  -- Load UI
  ui <- mapRIO fst $ gets rsUI
  uiMessages <- traverse Worker.readObservedIO (UI.messages rUI)
  background <- Worker.readObservedIO (UI.background rUI)
  spinner <- Worker.readObservedIO (UI.spinner rUI)

  ForwardMsaa.usePass (Render.spForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    let dsl = Pipeline.pLayout $ Render.pUnlitTexturedBlend fPipelines
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS dsl rSceneUiDescs do
      -- Render UI
      Pipeline.bind cb (Render.pUnlitTexturedBlend fPipelines) do
        Draw.indexed cb (UI.quadUV ui) background
        Draw.indexed cb (UI.quadUV ui) spinner

      Pipeline.bind cb (Render.pEvanwSdf fPipelines) $
        traverse_ (Draw.quads cb) uiMessages

τ :: Floating a => a
τ = 2 * pi
