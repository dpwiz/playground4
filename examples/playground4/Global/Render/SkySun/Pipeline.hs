{-# LANGUAGE OverloadedLists #-}

module Global.Render.SkySun.Pipeline
  ( Pipeline
  , allocate
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Vulkan.Core10 qualified as Vk
import Vulkan.Zero (Zero(..))

import Engine.Vulkan.Pipeline (Config(..))
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (HasVulkan, HasRenderPass(..), DsBindings)
import Render.Code (compileVert, compileFrag, glsl)
import Render.Code.Lit (hgPhase, raySphereIntersection, structLight)
import Render.Code.Noise (hash33)
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0.Code (set0binding0, set0binding1, set0binding3, set0binding4)
import Stage.Example.World.Env (Env)
import Stage.Example.World.Env.Code (set1binding0)

type Pipeline = Pipeline.Pipeline '[Scene, Env] () ()

allocate
  :: ( HasVulkan env
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsBindings
  -> Tagged Env DsBindings
  -> renderpass
  -> ResourceT (RIO env) Pipeline
allocate multisample (Tagged set0) (Tagged set1) = do
  fmap snd . Pipeline.allocate
    Nothing
    multisample
    zero
      { cVertexCode         = Just vertCode
      , cFragmentCode       = Just fragCode
      , cDescLayouts        = Tagged @'[Scene, Env] [set0, set1]
      , cCull               = Vk.CULL_MODE_NONE
      }

vertCode :: ByteString
vertCode =
  $(compileVert [glsl|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    ${set0binding0}

    layout(location = 0) out vec3 fUVW;

    float farZ = 0.9999; // 1 - 1e-7;

    void main() {
      vec4 pos = vec4(0.0);
      switch(gl_VertexIndex) {
          case 0: pos = vec4(-1.0,  3.0, farZ, 1.0); break;
          case 1: pos = vec4(-1.0, -1.0, farZ, 1.0); break; // XXX: swapped with 2. culling?
          case 2: pos = vec4( 3.0, -1.0, farZ, 1.0); break;
      }

      vec3 unProjected = (scene.invProjection * pos).xyz;
      // unProjected *= -1;
      fUVW = mat3(scene.invView) * unProjected;

      gl_Position = pos;
    }
  |])

fragCode :: ByteString
fragCode =
  $(compileFrag [glsl|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable
    #extension GL_EXT_nonuniform_qualifier : enable

    ${set0binding0}
    ${set0binding1}
    ${set0binding3}

    ${set1binding0}

    #define M_MAX 1e9
    #define KEY_M (float(77)+0.5)/256.0

    const float M_TAU = 6.283185307;
    const float M_2TAU = 2.0 * M_TAU;

    ///////////////////////////////////////
    // bodies
    ///////////////////////////////////////
    float earthRadius = env.bodies[0];
    float atmoHeight  = env.bodies[1];

    float sunRadius     = env.bodies[2];
    float distanceToSun = env.bodies[3];

    float atmoRadius  = earthRadius + atmoHeight;

    ///////////////////////////////////////
    // atmosphere
    ///////////////////////////////////////
    vec3 betaR = env.rayleigh.rgb;
    vec3 betaM = env.mie.rgb;

    vec3 M_2TAUbetaR = M_2TAU * betaR;
    vec3 M_2TAUbetaM = M_2TAU * betaM;

    vec2 heightScales = vec2(
      env.rayleigh.a,
      env.mie.a
    );
    const float g = -0.76; // XXX: something-something hgPhase(nu, g)

    const float NUM_DENSITY_SAMPLES     = 8.0;
    const float NUM_VIEW_SAMPLES        = 8.0;
    const int   INT_NUM_DENSITY_SAMPLES = int(NUM_DENSITY_SAMPLES);
    const int   INT_NUM_VIEW_SAMPLES    = int(NUM_VIEW_SAMPLES);

    ${hash33}

    vec3 stars(in vec3 p, float res) {
      vec3 p2 = p * 0.15 * res;
      vec3 q = fract(p2) - 0.5;
      vec3 id = floor(p2);
      vec2 noise = hash33(id).xy;

      float cStart = 1.0 - smoothstep(0.0, 0.6, length(q));

      vec3 c = vec3(0);
      for (float i = 0.0; i < 4.0; i++) {
        float c2 = cStart * step(noise[0], 0.0005 + i * i * 0.001);

        c += c2 * (
          mix(
            vec3(1.0, 0.49, 0.1),
            vec3(0.75, 0.9, 1.0),
            noise[1]
          ) * 0.1 + 0.9
        );

        p *= 1.3;
      }

      return c * c * 0.8;
    }

    ${raySphereIntersection}
    ${hgPhase}

    vec2 atmosphericDensity(vec3 origin, vec3 exit) {
      float distance = length(exit - origin);
      vec3  direction = (exit - origin) / distance;
      float stepSize = distance / NUM_DENSITY_SAMPLES;

      vec2 density = vec2(0.0);
      vec3 stepPoint = origin;
      for(int i = 0; i < INT_NUM_DENSITY_SAMPLES; i++) {
          stepPoint += direction * 0.5 * stepSize;
          vec2 altitude = vec2(length(stepPoint) - earthRadius);
          density += exp(-altitude / heightScales);
      }

      return density * stepSize;
    }

    vec4 atmosphericScattering(vec3 rayOrigin, vec3 rayDirection, vec3 earthCenter, vec3 sunDirection) {
      vec2 atmoHits  = raySphereIntersection(rayOrigin, rayDirection, earthCenter, atmoRadius);
      vec2 groundHit = raySphereIntersection(rayOrigin, rayDirection, earthCenter, earthRadius);

      bool bNoPlanetIntersection = groundHit[0] < 0.0 && groundHit[1] < 0.0;

      float farPoint  = bNoPlanetIntersection ? atmoHits[1] : groundHit[0];
      float nearPoint = atmoHits[0] > 0.0 ? atmoHits[0] : 0.0;

      float stepLength = (farPoint - nearPoint) / NUM_VIEW_SAMPLES;

      vec3 rayleightCollected = vec3(0.0);
      vec3 mieCollected = vec3(0.0);
      vec2 densityCollected = vec2(0.0);

      vec3 stepPoint = rayOrigin + rayDirection * nearPoint;
      for(int i = 0; i < INT_NUM_VIEW_SAMPLES; i++) {
        stepPoint += rayDirection * 0.5 * stepLength;

        vec2 densityToOrigin = atmosphericDensity(stepPoint, rayOrigin);

        float stepAtmoExit = raySphereIntersection(stepPoint, sunDirection, earthCenter, atmoRadius)[1];
        vec2 densities = atmosphericDensity(stepPoint, stepPoint + sunDirection * stepAtmoExit) + densityToOrigin;

        float altitude = length(stepPoint) - earthRadius;
        vec2 expRM = exp(-vec2(altitude) / heightScales);

        rayleightCollected += expRM.x * exp( -M_2TAUbetaR * densities.x );
        mieCollected       += expRM.y * exp( -M_2TAUbetaM * densities.y );
        densityCollected   += densityToOrigin;
      }

      // XXX: only .r component used for extinction
      float extinction = exp(
        - (
            M_2TAUbetaR.r * densityCollected[0] +
            M_2TAUbetaM.r * densityCollected[1]
          )
      );

      float nu = dot(sunDirection, -rayDirection);
      vec3 inscatter =
        ( stepLength * betaR * hgPhase(nu, 0.0) * rayleightCollected
        + stepLength * betaM * hgPhase(nu, g)   * mieCollected
        ) * 10.0;

      float cloudHeight = env.fancy.w;
      float clouds = 0;
      float cloudsLit = 0;
      int cloudTex = int(env.fancy.z);
      if (cloudTex > -1) {
        vec2 cloudHits = raySphereIntersection(rayOrigin, rayDirection, earthCenter, earthRadius + cloudHeight);
        if (cloudHits[0] > 0.0 && scene.envCubeId > -1) {
          vec3 atmoPos = rayOrigin + rayDirection * cloudHits[0] - earthCenter;
          vec3 cloudsUVW = normalize(atmoPos);

          clouds = texture(
            samplerCube(
              cubes[nonuniformEXT(cloudTex)],
              samplers[2] // XXX: linear/mip/repeat
            ),
            cloudsUVW
          ).r;

          cloudsLit = max(dot(sunDirection, cloudsUVW), 0);
        }
      }
      float occlusion = 1.0 - clouds;

      vec3 surface = vec3(0);
      if (scene.envCubeId > -1) {
        vec3 groundPos = rayOrigin + rayDirection * groundHit[0] - earthCenter;
        vec3 surfaceUVW = normalize(groundPos);

        vec3 diffuseFac = vec3(max(dot(sunDirection, surfaceUVW), 0));
        vec3 diffuse = texture(
          samplerCube(
            cubes[2], // XXX: will require nonuniformEXT when dynamic
            samplers[2] // XXX: linear/mip/repeat
          ),
          surfaceUVW
        ).rgb;

        vec3 halfwayDir = normalize(sunDirection - rayDirection);
        vec3 specular = vec3(1.0); // diffuse;
        float specularFac = pow(max(dot(surfaceUVW, halfwayDir), 0.0), 64);

        surface = diffuse * diffuseFac * occlusion + specular * specularFac * diffuseFac * occlusion;
      }

      float stardim = length(rayOrigin - earthCenter) - earthRadius;
      vec3 starlight = stars(rayDirection, 5000) * clamp(stardim / atmoHeight, 0.0, 1.0) * occlusion;

      return vec4(
        inscatter
          + vec3(clouds * cloudsLit)
          + (bNoPlanetIntersection ? starlight : surface),
        extinction * float(bNoPlanetIntersection)
      );
    }

    const uint MAX_LIGHTS = 255;
    ${structLight}
    ${set0binding4}

    layout(location = 0) in vec3 fUVW;

    layout(location = 0) out vec4 oColor;

    void main() {
      vec3 viewDirection = normalize(fUVW);

      vec3 sunDirection = lights[0].direction.xyz;

      // BUG: some parts of code depend on this being 0
      vec3 earthCenter = vec3(0);

      vec3 position = env.location.xyz * (earthRadius + env.location.a);

      vec4 light = atmosphericScattering(position, viewDirection, earthCenter, sunDirection);
      oColor.rgb = light.rgb;

      vec3 sunPos = sunDirection * distanceToSun;
      float t0 = raySphereIntersection(position, viewDirection, sunPos, sunRadius)[0];
      if(t0 > 0.0) {
        oColor.rgb += vec3(light.w);
      }
    }
  |])
