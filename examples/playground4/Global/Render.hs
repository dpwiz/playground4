module Global.Render
  ( Stage
  , Frame
  , RenderPasses
  , ScreenPasses(..)
  , Pipelines(..)
  , allocatePipelines
  , getSceneLayout
  , getSunLayout
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import RIO.Vector.Partial as Vector (headM)
import Vulkan.Core10 qualified as Vk

import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (HasSwapchain(getMultisample), RenderPass(..))
import Render.Debug.Pipeline qualified as Debug
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Scene
import Render.DescSets.Sun (Sun)
import Render.DescSets.Sun qualified as Sun
import Render.Font.EvanwSdf.Pipeline qualified as EvanwSdf
import Render.ForwardMsaa (ForwardMsaa)
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui
import Render.Lit.Colored.Pipeline qualified as LitColored
import Render.Lit.Material.Pipeline qualified as LitMaterial
import Render.Lit.Textured.Pipeline qualified as LitTextured
import Render.Samplers qualified as Samplers
import Render.ShadowMap.Pipeline qualified as ShadowPipe
import Render.ShadowMap.RenderPass (ShadowMap)
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Render.Skybox.Pipeline qualified as Skybox
import Render.Unlit.Colored.Pipeline qualified as UnlitColored
import Render.Unlit.Textured.Pipeline qualified as UnlitTextured

import Global.Render.SkySun.DescSets qualified as SkySun
import Global.Render.SkySun.Pipeline qualified as SkySun
import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap qualified as CubeMap


type Stage = Engine.Stage RenderPasses Pipelines
type Frame = Engine.Frame RenderPasses Pipelines

type RenderPasses = ScreenPasses

data ScreenPasses = ScreenPasses
  { spForwardMsaa :: ForwardMsaa
  , spShadowPass  :: ShadowMap
  }

-- TODO: BufferPasses, like compute & shadow-mapping

instance RenderPass ScreenPasses where
  allocateRenderpass_ context = do
    spForwardMsaa <- ForwardMsaa.allocateMsaa context

    -- TODO: get from config
    let
      size      = 4096 -- XXX: 2048 is kinda minimum, even with PCF enabled
      numLayers = 2    -- XXX: no-cascade sun + moon
    spShadowPass <- ShadowPass.allocate context size numLayers

    pure ScreenPasses{..}

  updateRenderpass context ScreenPasses{..} = ScreenPasses
    <$> ForwardMsaa.updateMsaa context spForwardMsaa
    <*> pure spShadowPass -- XXX: not a screen pass

  refcountRenderpass ScreenPasses{..} = do
    refcountRenderpass spForwardMsaa
    refcountRenderpass spShadowPass

data Pipelines = Pipelines
  { pEvanwSdf :: EvanwSdf.Pipeline
  , pSkybox   :: Skybox.Pipeline
  , pSkySun   :: SkySun.Pipeline
  , pDebug    :: Debug.Pipeline

  , pLitColored       :: LitColored.Pipeline
  , pLitColoredBlend  :: LitColored.Pipeline
  , pLitMaterial      :: LitMaterial.Pipeline
  , pLitMaterialBlend :: LitMaterial.Pipeline
  , pLitTextured      :: LitTextured.Pipeline
  , pLitTexturedBlend :: LitTextured.Pipeline

  , pUnlitColored        :: UnlitColored.Pipeline
  , pUnlitColoredNoDepth :: UnlitColored.Pipeline
  , pUnlitTextured       :: UnlitTextured.Pipeline
  , pUnlitTexturedBlend  :: UnlitTextured.Pipeline
  , pWireframe           :: UnlitColored.Pipeline
  , pWireframeNoDepth    :: UnlitColored.Pipeline

  , pShadowCast :: ShadowPipe.Pipeline
  }

allocatePipelines
  :: HasSwapchain swapchain
  => Bool
  -> swapchain
  -> ScreenPasses
  -> ResourceT (StageRIO st) Pipelines
allocatePipelines useImgui swapchain ScreenPasses{..} = do
  (_, samplers) <- Samplers.allocate swapchain
  let msaa = getMultisample swapchain
  let sceneBinds = Scene.set0 samplers (Left Combined.numTextures) (Left CubeMap.numCubes) 1
  let envBinds = SkySun.set1

  pDebug    <- Debug.allocate msaa sceneBinds spForwardMsaa
  pEvanwSdf <- EvanwSdf.allocate msaa sceneBinds spForwardMsaa
  pSkybox   <- Skybox.allocate msaa sceneBinds spForwardMsaa
  pSkySun <- SkySun.allocate msaa sceneBinds envBinds spForwardMsaa

  pLitColored       <- LitColored.allocate msaa sceneBinds spForwardMsaa
  pLitColoredBlend  <- LitColored.allocateBlend msaa sceneBinds spForwardMsaa
  pLitMaterial      <- LitMaterial.allocate msaa sceneBinds spForwardMsaa
  pLitMaterialBlend <- LitMaterial.allocateBlend msaa sceneBinds spForwardMsaa
  pLitTextured      <- LitTextured.allocate msaa sceneBinds spForwardMsaa
  pLitTexturedBlend <- LitTextured.allocateBlend msaa sceneBinds spForwardMsaa

  pUnlitColored        <- UnlitColored.allocate True msaa sceneBinds spForwardMsaa
  pUnlitColoredNoDepth <- UnlitColored.allocate False msaa sceneBinds spForwardMsaa
  pUnlitTextured       <- UnlitTextured.allocate msaa sceneBinds spForwardMsaa
  pUnlitTexturedBlend  <- UnlitTextured.allocateBlend msaa sceneBinds spForwardMsaa
  pWireframe           <- UnlitColored.allocateWireframe True msaa sceneBinds spForwardMsaa
  pWireframeNoDepth    <- UnlitColored.allocateWireframe False msaa sceneBinds spForwardMsaa

  let sunBinds = Sun.set0
  pShadowCast <- ShadowPipe.allocate sunBinds spShadowPass ShadowPipe.defaults

  when useImgui $
    void $! ImGui.allocate swapchain spForwardMsaa 0

  pure Pipelines{..}

getSceneLayout :: Pipelines -> Tagged '[Scene] Vk.DescriptorSetLayout
getSceneLayout Pipelines{pLitColored} =
  case Vector.headM (unTagged $ Pipeline.pDescLayouts pLitColored) of
    Nothing ->
      error "pLitColored has at least set0 in layout"
    Just set0layout ->
      Tagged set0layout

getSunLayout :: Pipelines -> Tagged '[Sun] Vk.DescriptorSetLayout
getSunLayout Pipelines{pShadowCast} =
  case Vector.headM (unTagged $ Pipeline.pDescLayouts pShadowCast) of
    Nothing ->
      error "pShadowCast has at least set0 in layout"
    Just set0layout ->
      Tagged set0layout
