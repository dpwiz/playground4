module Global.Resource.Combined where

import RIO

import Resource.Collection (enumerate, size)
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.Texture (Texture, Flat)

import Global.Resource.Font qualified as Font
import Global.Resource.Texture qualified as Texture

type TextureCollectionF a =
  CombinedTextures.Collection
    Texture.Collection
    Font.Collection
    a

type CombinedCollection = TextureCollectionF (Int32, Texture Flat)
type Ids                = TextureCollectionF Int32
type Textures           = TextureCollectionF (Texture Flat)

paths :: TextureCollectionF FilePath
paths = CombinedTextures.Collection
  { textures = Texture.paths
  , fonts    = fmap Font.configTexture Font.configs
  }

indices :: TextureCollectionF Int32
indices = fmap fst $ enumerate paths

numTextures :: Num a => a
numTextures = size paths
