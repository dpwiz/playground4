{-# LANGUAGE DeriveAnyClass #-}

module Global.Resource.Texture.SkyHome where

import RIO

import Data.Distributive (Distributive(..))
import Data.Distributive.Generic (genericCollect)
import Data.Functor.Rep (Co(..), Representable)
import GHC.Generics (Generic1)

import Resource.Static as Static
import Resource.Texture (Texture, Flat)

{- |
  Textures from the sky_home.gltf scene

  Names could be anything, but the order must match source "images" array.
-}
data Collection a = Collection
  { bark_emissive               :: a
  , bark_basecolor              :: a
  , material_413_basecolor      :: a
  , door__barrels_basecolor     :: a
  , roof_basecolor              :: a
  , material_650_basecolor      :: a
  , wooden_skel_no_op_basecolor :: a
  , wall_basecolor              :: a
  , op_branches_basecolor       :: a
  , planks_basecolor            :: a
  , grass_basecolor             :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Co Collection)
  deriving anyclass (Representable)

instance Distributive Collection where
  collect = genericCollect

type TextureCollection = Collection (Int32, Texture Flat)

Static.filePatterns Static.Files "resources/textures/sky_home"

paths :: Collection FilePath
paths = Collection
  BARK_EMISSIVE_KTX_ZST
  BARK_BASECOLOR_KTX_ZST
  MATERIAL_413_BASECOLOR_KTX_ZST
  DOOR__BARRELS_BASECOLOR_KTX_ZST
  ROOF_BASECOLOR_KTX_ZST
  MATERIAL_650_BASECOLOR_KTX_ZST
  WOODEN_SKEL_NO_OP_BASECOLOR_KTX_ZST
  WALL_BASECOLOR_KTX_ZST
  OP_BRANCHES_BASECOLOR_KTX_ZST
  PLANKS_BASECOLOR_KTX_ZST
  GRASS_BASECOLOR_KTX_ZST
