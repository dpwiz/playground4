{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.CubeMap
  ( Collection(..)
  , CubeMapCollection
  , Ids
  , Textures
  , paths
  , indices
  , numCubes
  ) where

import RIO

import Data.Distributive (Distributive(..))
import Data.Distributive.Generic (genericCollect)
import Data.Functor.Rep (Co(..), Representable)
import GHC.Generics (Generic1)

import Resource.Collection (enumerate, size)
import Resource.Static as Static
import Resource.Texture (Texture, CubeMap)

data Collection a = Collection
  { black    :: a
  , test     :: a
  , planet   :: a
  , clouds   :: a
  , milkyway :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Co Collection)
  deriving anyclass (Representable)

instance Distributive Collection where
  collect = genericCollect

type CubeMapCollection = Collection (Int32, Texture CubeMap)
type Ids               = Collection Int32
type Textures          = Collection (Texture CubeMap)

Static.filePatterns Static.Files "resources/cubemaps"

paths :: Collection FilePath
paths = Collection
  { black    = BLACK_KTX_ZST
  , test     = TEST_KTX_ZST
  , planet   = PLANET_KTX_ZST
  , clouds   = CLOUDS_KTX_ZST
  , milkyway = MILKYWAY_KTX_ZST
  }

indices :: Collection Int32
indices = fmap fst $ enumerate paths

numCubes :: Num a => a
numCubes = size paths
