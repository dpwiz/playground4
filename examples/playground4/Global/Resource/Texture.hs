{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Texture
  ( Collection(..)
  , TextureCollection
  , paths
  ) where

import RIO

import Data.Distributive (Distributive(..))
import Data.Distributive.Generic (genericCollect)
import Data.Functor.Rep (Co(..), Representable)
import GHC.Generics (Generic1)

import Resource.Static as Static
import Resource.Texture (Texture, Flat)

import Global.Resource.Texture.SkyHome qualified as SkyHome

data Collection a = Collection
  { black         :: a
  , flat          :: a
  , ibl_brdf      :: a
  , uvgrid        :: a

  , brickwall        :: a
  , brickwall_normal :: a

  , cd            :: a
  , david         :: a
  , fashion       :: a
  , flare         :: a
  , stars         :: a

  , vulkan_planet :: a
  , viking_room   :: a
  , sky_home      :: SkyHome.Collection a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Co Collection)
  deriving anyclass (Representable)

instance Distributive Collection where
  collect = genericCollect

type TextureCollection = Collection (Int32, Texture Flat)

Static.filePatterns Static.Files "resources/textures/base"

paths :: Collection FilePath
paths = Collection
  { black         = BLACK_KTX_ZST
  , flat          = FLAT_KTX_ZST
  , ibl_brdf      = IBL_BRDF_LUT_KTX_ZST
  , uvgrid        = UVGRID_KTX_ZST

  , brickwall        = BRICKWALL_KTX_ZST
  , brickwall_normal = BRICKWALL_NORMAL_KTX_ZST

  , cd            = CD_KTX_ZST
  , david         = DAVID_KTX_ZST
  , fashion       = FASHION_KTX_ZST
  , flare         = FLARE_KTX_ZST
  , stars         = STARS_KTX_ZST

  , vulkan_planet = VULKAN_PLANET_KTX_ZST

  , viking_room   = VIKING_ROOM_KTX_ZST
  , sky_home      = SkyHome.paths
  }
